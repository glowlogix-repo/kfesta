<?php
function product_manager_form(){
	if (isset($_POST["vendor_product_type"])) { 
    
    	$user_id 		= (int) $_POST['vendor_product_id'];
		$product_ids 	= (int) $_POST['vendor_product_type'];

    	$vendor_products = get_user_meta($user_id, '_vendor_product_ids', true);

		if ( ! is_array($vendor_products) ) {
		    $vendor_products = array();

		     // Push a new value onto the array
		    $vendor_products[] = $product_ids;
		    // Write the user meta record with the new value in it
		    update_user_meta($user_id, '_vendor_product_ids', $vendor_products);

		} elseif (! in_array($product_ids, $vendor_products)) {
    		
    		array_push($vendor_products, $product_ids);
    		/*
    		Added product id in meta.
    		*/
    		update_user_meta( $user_id, '_vendor_product_ids', $vendor_products);
    	} 
    }
}
add_action('init', 'product_manager_form');


function custom_processing($order_id) {

    /*
start_working_on_cake, cake_is_finished, cake’s_photo, cake_is_delivered
    */
    $order_id = 2396;
    $order = wc_get_order( $order_id );
    $items = $order->get_items(); 


    foreach ( $order->get_items() as $item ) {

        // Compatibility for woocommerce 3+
        $product_id = version_compare( WC_VERSION, '3.0', '<' ) ? $item['product_id'] : $item->get_product_id(); 

        $product_order_config = array();
        
        if( have_rows('configurable_product', $product_id) ): 
            while( have_rows('configurable_product', $product_id) ): the_row(); 
                $step_name = get_sub_field('step_name', $product_id);
                $step_type = get_sub_field('step_type', $product_id); 

                $product_order_config[$product_id][$step_name] = 0;  
                
            endwhile; 
        endif; 
        update_post_meta($order_id, 'configurable_order_product', $product_order_config);
    }

/*a:1:{i:2356;a:3:{s:21:"start_working_on_cake";i:0;...*/ 
}
//add_action('admin_init', 'custom_processing');
add_action('woocommerce_order_status_processing', 'custom_processing');