<?php
/*
Plugin Name: Novaro People
Plugin URI: http://www.novarostudio.com/
Description: Novaro People is a wordpress plugin for display some people in your team or organization.
Version: 1.0
Author: novarostudio
Author URI: http://www.novarostudio.com
License: GPL
*/

/*  Copyright 2015  Novaro Studio

    Novaro People is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);

class Novaro_People{
	
	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;
	
	function __construct(){
		// Register the shortcode to the function ep_shortcode()
		add_shortcode( 'people', array($this, 'novaro_people') );
		
		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');
		
		//Register the Portfolio Menu
		add_action('init', array($this, 'novaro_pf_post_type'));
		add_action('init', array($this, 'novaro_pf_action_init'));
		add_action('after_setup_theme', array($this, 'novaro_pf_setup'));
		
		//Customize the Portfolio List in the wp-admin
		add_filter('manage_edit-peoplepost_columns', array($this, 'novaro_pf_add_list_columns'));
		add_action('manage_peoplepost_posts_custom_column', array($this, 'novaro_pf_manage_column'));
		add_action( 'restrict_manage_posts', array($this, 'novaro_pf_add_taxonomy_filter') );
		
		$this->version		= $this->novaro_plugin_version();
		$this->postslug		= $this->novaro_postslug();
		$this->taxonomslug	= $this->novaro_taxonomslug();
		$this->posttype		= $this->novaro_posttype();
		$this->posttaxonomy	= $this->novaro_taxonomy();
	}
	
	//Get the version of portfolio
	function novaro_plugin_version(){
		$this->version = "1.0";
		
		return $this->version;
	}
	
	function novaro_lang(){
		$thelang = 'novaro';
		return $thelang;
	}
	
	function novaro_shortname(){
		$theshortname = 'novaro';
		return $theshortname;
	}
	
	function novaro_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}
	
	function novaro_posttype(){
		$this->posttype = 'peoplepost';
		return $this->posttype;
	}
	
	function novaro_taxonomy(){
		$this->posttaxonomy = 'peoplecat';
		return $this->posttaxonomy;
	}
	
	function novaro_postslug(){
		$this->postslug = 'people';
		return $this->postslug;
	}
	
	function novaro_taxonomslug(){
		$this->taxonomslug = 'peoplecat';
		return $this->taxonomslug;
	}
	
	function novaro_pf_md5hash($str = ''){
		return md5($str);
	}

	//Get the image size for every column
	function novaro_pf_setsize(){
	
		//set image size for every column in here.
		$this->imagesizes = array(
			array(
				"num"		=> 'default',
				"namesize"	=> 'people-image',
				"width" 	=> 700,
				"height" 	=> 700
			)
			
		);
		return $this->imagesizes;
	}
	
	function novaro_pf_setup(){
		add_theme_support( 'post-thumbnails' );
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			add_image_size( $imgsize["namesize"], $imgsize["width"], $imgsize["height"], true ); // Portfolio Thumbnail
		}
	}
	
	function novaro_pf_getthumbinfo($col){
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			if($col==$imgsize["num"]){
				return $imgsize;
			}
		}
		return false;
	}
	
	function novaro_people($atts, $content = null) {
		extract(shortcode_atts(array(
			'id' 	=> '',
			'class'	=> '',
			'col' => '3',
			'cat' => '',
			'showposts' => 3,
			'showtitle' => 'yes',
			'showinfo' => 'yes',
			'showthumb' => 'yes'
		), $atts));
		
		$nvr_initial = $this->novaro_initial();
		$nvr_shortname = $this->novaro_shortname();
		
		$catname = get_term_by('slug', $cat, $this->novaro_taxonomy());
		$showtitle = ($showtitle=='yes')? true : false;
		$showinfo = ($showinfo=='yes')? true : false;
		$showthumb = ($showthumb=='yes')? true : false;
		$showposts = (is_numeric($showposts))? $showposts : 5;
		
		if($col!='3' && $col!='4'){
			$col = '3';
		}
		
		if($col=='3'){
			$col = 3;
		}elseif($col=='4'){
			$col = 4;
		}else{
			$col = 3;
		}
		
		$imgsize = 'people-image';
		
		$qryargs = array(
			'post_type' => $this->novaro_posttype(),
			'showposts' => $showposts
		);
		if($catname!=false){
			$qryargs['tax_query'] = array(
				array(
					'taxonomy' => $this->novaro_taxonomy(),
					'field' => 'slug',
					'terms' => $catname->slug
				)
			);
		}
		
		$nvr_peopleqry = new WP_Query( $qryargs );
		
		$nvr_output = "";
		if( $nvr_peopleqry->have_posts() ){
			$nvr_output .= '<div class="nvr-people '.esc_attr( $class ).'">';
			$nvr_output .= '<ul class="row">';
			$i = 1;
			while ( $nvr_peopleqry->have_posts() ) : $nvr_peopleqry->the_post(); 
				
				if($col==3){
					$liclass = 'four columns';
				}elseif($col==4){
					$liclass = 'three columns';
				}else{
					$liclass = '';
				}
				
				$peopleid 		= get_the_ID();
				$custom 		= get_post_custom( $peopleid );
				$peopleinfo 	= (isset($custom['_'.$nvr_initial.'_people_info'][0]))? $custom['_'.$nvr_initial.'_people_info'][0] : "";
				$peoplethumb 	= (isset($custom['_'.$nvr_initial.'_people_thumb'][0]))? $custom['_'.$nvr_initial.'_people_thumb'][0] : "";
				$peoplepinterest= (isset($custom['_'.$nvr_initial.'_people_pinterest'][0]))? $custom['_'.$nvr_initial.'_people_pinterest'][0] : "";
				$peoplefacebook	= (isset($custom['_'.$nvr_initial.'_people_facebook'][0]))? $custom['_'.$nvr_initial.'_people_facebook'][0] : "";
				$peopletwitter 	= (isset($custom['_'.$nvr_initial.'_people_twitter'][0]))? $custom['_'.$nvr_initial.'_people_twitter'][0] : "";
				
				if($i%$col==1){
					$liclass .= ' alpha';
				}elseif($i%$col==0 && $col>1){
					$liclass .= ' last';
				}
				
				$nvr_output .= '<li class="'.esc_attr( $liclass ).'">';
					$nvr_output .='<div class="peoplecontainer">';
						if($showthumb){
							$nvr_output .='<div class="imgcontainer">';
								if($peoplethumb){
									$nvr_output .= '<img src="'.esc_url( $peoplethumb ).'" alt="'.esc_attr( get_the_title( $peopleid ) ).'" title="'. esc_attr( get_the_title( $peopleid ) ) .'" class="scale-with-grid" />';
								}elseif( has_post_thumbnail( $peopleid ) ){
									$nvr_output .= get_the_post_thumbnail( $peopleid, $imgsize, array('class' => 'scale-with-grid'));
								}else{
									$nvr_output .= '<img src="'.esc_url( plugin_dir_url( __FILE__ ).'images/testi-user.png' ).'" alt="'.esc_attr( get_the_title( $peopleid ) ).'" title="'. esc_attr( get_the_title( $peopleid ) ) .'" class="scale-with-grid" />';
								}
								$nvr_output .= '<div class="peoplecontent">';
									$nvr_output .= '<h5 class="peopleabout">'.__('About Me', $this->novaro_lang()).'</h5>';
									$nvr_output .= get_the_content();
								$nvr_output .= '</div>';
								$nvr_output .= '<div class="clearfix"></div>';
							$nvr_output .='</div>';
							
							$bqclass="";
						}else{
							$bqclass="nomargin";
						}
				
						$nvr_output .= '<div class="peopletitle '.esc_attr( $bqclass ).'">';
							if($showtitle || $showinfo){
								if($showtitle){
									$nvr_output .= '<h5 class="fontbold marginoff">'.get_the_title( $peopleid ).'</h5>';
								}
								if($showinfo){
									$nvr_output .= '<div class="peopleinfo">'.$peopleinfo.'</div>';
								}
							}
							$nvr_output .= '<div class="clearfix"></div>';
						$nvr_output .= '</div>';
						$nvr_output .= '<div class="peoplesocial">';
							if($peoplefacebook){
								$nvr_output .= '<a href="'.esc_url( $peoplefacebook ).'" target="_blank" class="fa fa-facebook"></a>';
							}
							if($peopletwitter){
								$nvr_output .= '<a href="'.esc_url( $peopletwitter ).'" target="_blank" class="fa fa-twitter"></a>';
							}
							if($peoplepinterest){
								$nvr_output .= '<a href="'.esc_url( $peoplepinterest ).'" target="_blank" class="fa fa-pinterest"></a>';
							}
							$nvr_output .= '<div class="clearfix"></div>';
						$nvr_output .= '</div>';
						
						$nvr_output .= '<div class="clearfix"></div>';
					$nvr_output .= '</div>';
				$nvr_output .= '</li>';
				
				$i++;
			endwhile;
				$nvr_output .= '<li class="clearfix"></li>';
			$nvr_output .= '</ul>';
			$nvr_output .= '<div class="clearfix"></div>';
			$nvr_output .= "</div>";
		}else{
			$nvr_output .= '<!-- no people post -->';
		}
		wp_reset_postdata();
		
		return do_shortcode($nvr_output);
	}
	
	/* Make a Portfolio Post Type */
	function novaro_pf_post_type() {
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		$postslug = $this->novaro_postslug();
		$taxonomslug = $this->novaro_taxonomslug();
		
		register_post_type( $posttype,
					array( 
					'label' => __('People', $this->novaro_lang() ), 
					'public' => true, 
					'show_ui' => true,
					'show_in_nav_menus' => true,
					'rewrite' => array( 'slug' => $postslug, 'with_front' => false ),
					'hierarchical' => true,
					'menu_position' => 5,
					'has_archive' => true,
					'supports' => array(
										 'title',
										 'editor',
										 'thumbnail',
										 'excerpt',
										 'custom-fields',
										 'revisions')
						) 
					);
		register_taxonomy($taxonom, $posttype, array(
			'hierarchical' => true,
			'label' =>  __('People Categories', $this->novaro_lang()),
			'query_var' => true,
			'rewrite' => array( 'slug' => $taxonomslug, 'with_front' => false ),
			'show_ui' => true,
			'singular_name' => 'Category'
			));
	}
	
	function novaro_pf_add_list_columns($portfolio_columns){
		
		$thetaxonomy = $this->novaro_taxonomy();
		$new_columns = array();
		$new_columns['cb'] = '<input type="checkbox" />';
		
		$new_columns['title'] = __('People Name', $this->novaro_lang());
		$new_columns['images'] = __('Images', $this->novaro_lang());
		$new_columns['author'] = __('Author', $this->novaro_lang());
		
		$new_columns[$thetaxonomy] = __('Categories', $this->novaro_lang());
		
		$new_columns['date'] = __('Date', $this->novaro_lang());
		
		return $new_columns;
	}
	
	function novaro_pf_manage_column($column_name){
		global $post;
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		
		$id = $post->ID;
		$title = $post->post_title;
		switch($column_name){
			case 'images':
				$thumbnailid = get_post_thumbnail_id($id);
				$imagesrc = wp_get_attachment_image_src($thumbnailid, 'thumbnail');
				if($imagesrc){
					echo '<img src="'.$imagesrc[0].'" width="50" alt="'.$title.'" />';
				}else{
					_e('No Featured Image', $this->novaro_lang());
				}
				break;
			
			case $taxonom:
				$postterms = get_the_terms($id, $taxonom);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonom.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$termtext = implode(", ",$termlists);
						echo $termtext;
					}
				}
				
				break;
		}
	}
	
	/* Filter Custom Post Type Categories */
	function novaro_pf_add_taxonomy_filter() {
		global $typenow;
		$posttype = $this->novaro_posttype();
		$taxonomy = $this->novaro_taxonomy();
		if( $typenow==$posttype){
			$filters = array($taxonomy);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo '<select name="'. esc_attr( $tax_slug ).'" id="'. esc_attr( $tax_slug ) .'" class="postform">';
				echo "<option value=''>".__('View All',$this->novaro_lang())." "."$tax_name</option>";
				foreach ($terms as $term) { 
					$selectedstr = '';
					if(isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug){
						$selectedstr = ' selected="selected"';
					}
					echo '<option value='. $term->slug. $selectedstr . '>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
	
	function novaro_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages
		
		$version = $this->novaro_plugin_version();
		
		//Register and use this plugin main CSS
		wp_register_style('nvr_skeleton-css', plugin_dir_url( __FILE__ ).'css/1140.css', 'nvr_normalize-css', '', 'screen, all');
		wp_enqueue_style('nvr_skeleton-css');
		
		wp_register_style('nvr_font-awesome-css', plugin_dir_url( __FILE__ ).'css/font-awesome.min.css', 'nvr_normalize-css', '', 'screen, all');
		wp_enqueue_style('nvr_font-awesome-css');
		
		wp_register_style('nvr_custom-people-css', plugin_dir_url( __FILE__ ).'css/nvrpeople.css', '', '', 'screen, all');
		wp_enqueue_style('nvr_custom-people-css');
	}
	
	// The excerpt based on character
	function novaro_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}
	
}

$thepeople = new Novaro_People();
?>