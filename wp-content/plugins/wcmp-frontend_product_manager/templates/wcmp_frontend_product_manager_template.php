<?php
/**
 * The template for displaying demo plugin content.
 *
 * Override this template by copying it to yourtheme/wcmp-frontend-product-manager/wcmp-frontend-product-manager_template.php
 *
 * @author 		dualcube
 * @package 	wcmp-frontend-product-manager/Templates
 * @version     0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;

echo $demo_value;
?>