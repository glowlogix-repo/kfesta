<?php
class Frontend_Product_Manager_Shortcode {

	public function __construct() {

	}

	/**
	 * Output the Frontend Product Manager shortcode.
	 *
	 * @access public
	 * @param array $atts
	 * @return void
	 */
	static public function output( $attr ) {
		global $WCMp, $WCMp_Frontend_Product_Manager, $wc_product_attributes;
		
		if( !is_user_logged_in() ) {
			_e('You do not have enough permission to access this page. Please logged in first.', 'wcmp_frontend_product_manager');
    	return;
		}
		
		$WCMp_Frontend_Product_Manager->nocache();
		
		$current_vendor_id = apply_filters( 'wcmp_current_loggedin_vendor_id', get_current_user_id() );
		
		// If vendor does not have product submission cap then show message
    if( is_user_logged_in() && is_user_wcmp_vendor( $current_vendor_id ) && !current_user_can('edit_products') ) {
    	_e('You do not have enough permission to submit a new product. Please contact site administrator.', 'wcmp_frontend_product_manager');
    	return;
    }
		
		$product_id = 0;
		$product = array();
		$product_type = '';
		$is_virtual = '';
		$title = '';
		$sku = '';
		$excerpt = '';
		$description = '';
		$regular_price = '';
		$sale_price = '';
		$sale_date_from = '';
		$sale_date_upto = '';
		$product_url = '';
		$button_text = '';
		$visibility = 'visible';
		$is_downloadable = '';
		$downloadable_files = array();
		$download_limit = '';
		$download_expiry = '';
		$featured_img = '';
		$gallery_img_ids = array();
		$gallery_img_urls = array();
		$categories = array();
		$product_tags = '';
		$manage_stock = '';
		$stock_qty = 0;
		$backorders = '';
		$stock_status = ''; 
		$sold_individually = '';
		$weight = '';
		$length = '';
		$width = '';
		$height = '';
		$shipping_class = '';
		$tax_status = '';
		$tax_class = '';
		$attributes = array();
		$default_attributes = '';
		$attributes_select_type = array();
		$variations = array();
		
		$upsell_ids = array();
		$crosssell_ids = array();
		$children = array();
		
		$enable_reviews = '';
		$menu_order = '';
		$purchase_note = '';
		
		// Yoast SEO Support
		$yoast_wpseo_focuskw_text_input = '';
		$yoast_wpseo_metadesc = '';
		
		// WooCommerce Custom Product Tabs Lite Support
		$product_tabs = array();
		
		// WooCommerce Product Fees Support
		$product_fee_name = '';
		$product_fee_amount = '';
		$product_fee_multiplier = 'no';
		
		// WooCommerce Bulk Discount Support
		$_bulkdiscount_enabled = 'no';
		$_bulkdiscount_text_info = '';
		$_bulkdiscounts = array();
		
		
		if(isset($_REQUEST['pro_id'])) {
			$product = wc_get_product( $_REQUEST['pro_id'] );
			
			// Fetching Product Data
			if($product && !empty($product)) {
				$product_id = $_REQUEST['pro_id'];
				
				$vendor_data = get_wcmp_product_vendors( $product_id );
				if( !current_user_can( 'administrator' ) && $vendor_data && ( $vendor_data->id != $current_vendor_id ) ) {
					_e('You do not have enough permission to access this product.', 'wcmp_frontend_product_manager');
					return;
				}
				
				$product_type = $product->get_type();
				$title = $product->get_title();
				$sku = $product->get_sku();
				$excerpt = $product->get_short_description();
				$description = $product->get_description();
				$regular_price = $product->get_regular_price();
				$sale_price = $product->get_sale_price();
				
				$sale_date_from = ( $date = get_post_meta( $product_id, '_sale_price_dates_from', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';
				$sale_date_upto = ( $date = get_post_meta( $product_id, '_sale_price_dates_to', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';
				
				// External product option
				$product_url = get_post_meta( $product_id, '_product_url', true);
				$button_text = get_post_meta( $product_id, '_button_text', true);
				
				// Product Visibility
				$visibility = get_post_meta( $product_id, '_visibility', true);
				
				// Virtual
				$is_virtual = ( get_post_meta( $product_id, '_virtual', true) == 'yes' ) ? 'enable' : '';
				
				// Download ptions
				$is_downloadable = ( get_post_meta( $product_id, '_downloadable', true) == 'yes' ) ? 'enable' : '';
				if($is_downloadable == 'enable') {
					$downloadable_files = get_post_meta( $product_id, '_downloadable_files', true);
					if(!$downloadable_files) $downloadable_files = array();
					$download_limit = get_post_meta( $product_id, '_download_limit', true);
					$download_expiry = get_post_meta( $product_id, '_download_expiry', true);
				}
				
				// Product Images
				$featured_img = ($product->get_image_id()) ? $product->get_image_id() : '';
				if($featured_img) $featured_img = wp_get_attachment_url($featured_img);
				$gallery_img_ids = $product->get_gallery_image_ids();
				if(!empty($gallery_img_ids)) {
					foreach($gallery_img_ids as $gallery_img_id) {
						$gallery_img_urls[]['image'] = wp_get_attachment_url($gallery_img_id);
					}
				}
				
				// Product Categories
				$pcategories = get_the_terms( $product_id, 'product_cat' );
				if( !empty($pcategories) ) {
					foreach($pcategories as $pkey => $pcategory) {
						$categories[] = $pcategory->term_id;
					}
				} else {
					$categories = array();
				}
				
				// Product Tags
				$product_tag_list = wp_get_post_terms($product_id, 'product_tag', array("fields" => "names"));
				$product_tags = implode(',', $product_tag_list);
				
				// Product Stock options
				$manage_stock = $product->managing_stock() ? 'enable' : '';
				$stock_qty = $product->get_stock_quantity();
				$backorders = $product->get_backorders();
				$stock_status = $product->get_stock_status();  
				$sold_individually = $product->is_sold_individually() ? 'enable' : '';
				
				// Product Shipping Data
				$weight = $product->get_weight();
				$length = $product->get_length();
				$width = $product->get_width();
				$height = $product->get_height();
				$shipping_class = $product->get_shipping_class_id();
				
				// Product Tax Data
				$tax_status = $product->get_tax_status();
				$tax_class = $product->get_tax_class();
				
				// Product Attributes
				$pro_attributes = get_post_meta( $product_id, '_product_attributes', true );
				if(!empty($pro_attributes)) {
					$acnt = 0;
					foreach($pro_attributes as $pro_attribute) {
						
						if ( $pro_attribute['is_taxonomy'] ) {
							$att_taxonomy = $pro_attribute['name'];

							if ( ! taxonomy_exists( $att_taxonomy ) ) {
								continue;
							}
							
							$attribute_taxonomy = $wc_product_attributes[ $att_taxonomy ];
					
							$attributes[$acnt]['term_name'] = $att_taxonomy;
							$attributes[$acnt]['name'] = wc_attribute_label( $att_taxonomy );
							$attributes[$acnt]['attribute_taxonomy'] = $attribute_taxonomy;
							$attributes[$acnt]['tax_name'] = $att_taxonomy;
							$attributes[$acnt]['is_taxonomy'] = 1;
							
							if ( 'select' === $attribute_taxonomy->attribute_type ) {
								$args = array(
												'orderby'    => 'name',
												'hide_empty' => 0
											);
								$all_terms = get_terms( $att_taxonomy, apply_filters( 'wc_product_attribute_terms', $args ) );
								$attributes_option = array();
								if ( $all_terms ) {
									foreach ( $all_terms as $term ) {
										$attributes_option[$term->term_id] = esc_attr( apply_filters( 'woocommerce_product_attribute_term_name', $term->name, $term ) );
									}
								}
								$attributes[$acnt]['attribute_type'] = 'select';
								$attributes[$acnt]['option_values'] = $attributes_option;
								$attributes[$acnt]['value'] = wp_get_post_terms( $product_id, $att_taxonomy, array( 'fields' => 'ids' ) );
							} else {
								$attributes[$acnt]['attribute_type'] = 'text';
								$attributes[$acnt]['value'] = esc_attr( implode( ' ' . WC_DELIMITER . ' ', wp_get_post_terms( $product_id, $att_taxonomy, array( 'fields' => 'names' ) ) ) );
							}
						} else {
							$attributes[$acnt]['term_name'] = apply_filters( 'woocommerce_attribute_label', $pro_attribute['name'], $pro_attribute['name'], $product );
							$attributes[$acnt]['name'] = apply_filters( 'woocommerce_attribute_label', $pro_attribute['name'], $pro_attribute['name'], $product );
							$attributes[$acnt]['value'] = $pro_attribute['value'];
							$attributes[$acnt]['tax_name'] = '';
							$attributes[$acnt]['is_taxonomy'] = 0;
							$attributes[$acnt]['attribute_type'] = 'text';
						}
						
						$attributes[$acnt]['is_visible'] = $pro_attribute['is_visible'] ? 'enable' : '';
						$attributes[$acnt]['is_variation'] = $pro_attribute['is_variation'] ? 'enable' : '';
						
						if( 'select' === $attributes[$acnt]['attribute_type'] ) {
							$attributes_select_type[$acnt] = $attributes[$acnt];
							unset($attributes[$acnt]);
						}
						$acnt++;
					}
				}
				
				// Product Default Attributes
				$default_attributes = json_encode( (array) get_post_meta( $product_id, '_default_attributes', true ) );
				
				// Variable Product Variations
				$variation_ids = $product->get_children();
				if(!empty($variation_ids)) {
					foreach($variation_ids as $variation_id_key => $variation_id) {
						$variation_data = new WC_Product_Variation($variation_id);
						
						$variations[$variation_id_key]['id'] = $variation_id;
						$variations[$variation_id_key]['enable'] = $variation_data->is_purchasable() ? 'enable' : '';
						$variations[$variation_id_key]['sku'] = get_post_meta($variation_id, '_sku', true);
						
						// Variation Image
						$variation_img = $variation_data->get_image_id();
						if($variation_img) $variation_img = wp_get_attachment_url($variation_img);
						else $variation_img = '';
						$variations[$variation_id_key]['image'] = $variation_img;
						
						// Variation Price
						$variations[$variation_id_key]['regular_price'] = get_post_meta($variation_id, '_regular_price', true);
						$variations[$variation_id_key]['sale_price'] = get_post_meta($variation_id, '_sale_price', true);
						
						// Variation Stock Data
						$variations[$variation_id_key]['manage_stock'] = $variation_data->managing_stock() ? 'enable' : '';
						$variations[$variation_id_key]['stock_status'] = $variation_data->get_stock_status();
						$variations[$variation_id_key]['stock_qty'] = $variation_data->get_stock_quantity();
						$variations[$variation_id_key]['backorders'] = $variation_data->get_backorders();
						
						// Variation Virtual Data
						$variations[$variation_id_key]['is_virtual'] = ( 'yes' == get_post_meta($variation_id, '_virtual', true) ) ? 'enable' : '';
						
						// Variation Downloadable Data
						$variations[$variation_id_key]['is_downloadable'] = ( 'yes' == get_post_meta($variation_id, '_downloadable', true) ) ? 'enable' : '';
						$variations[$variation_id_key]['downloadable_files'] = get_post_meta($variation_id, '_downloadable_files', true);
						$variations[$variation_id_key]['download_limit'] = get_post_meta($variation_id, '_download_limit', true);
						$variations[$variation_id_key]['download_expiry'] = get_post_meta($variation_id, '_download_expiry', true);
						if(!empty($variations[$variation_id_key]['downloadable_files'])) {
							foreach($variations[$variation_id_key]['downloadable_files'] as $variations_downloadable_files) {
								$variations[$variation_id_key]['downloadable_file'] = $variations_downloadable_files['file'];
								$variations[$variation_id_key]['downloadable_file_name'] = $variations_downloadable_files['name'];
							}
						}
						
						// Variation Shipping Data
						$variations[$variation_id_key]['weight'] = $variation_data->get_weight();
						$variations[$variation_id_key]['length'] = $variation_data->get_length();
						$variations[$variation_id_key]['width'] = $variation_data->get_width();
						$variations[$variation_id_key]['height'] = $variation_data->get_height();
						$variations[$variation_id_key]['shipping_class'] = $variation_data->get_shipping_class_id();
						
						// Variation Tax
						$variations[$variation_id_key]['tax_class'] = $variation_data->get_tax_class();
						
						// Variation Attributes
						$variations[$variation_id_key]['attributes'] = json_encode( $variation_data->get_variation_attributes() );
						
						// Description
						$variations[$variation_id_key]['description'] = get_post_meta($variation_id, '_variation_description', true);
						
						$variations = apply_filters( 'wcmp_fpm_variation_edit_data', $variations, $variation_id, $variation_id_key );
					}
				}
				
				$upsell_ids = get_post_meta( $product_id, '_upsell_ids', true ) ? get_post_meta( $product_id, '_upsell_ids', true ) : array();
				$crosssell_ids = get_post_meta( $product_id, '_crosssell_ids', true ) ? get_post_meta( $product_id, '_crosssell_ids', true ) : array();
				$children = get_post_meta( $product_id, '_children', true ) ? get_post_meta( $product_id, '_children', true ) : array();
				
				// Product Advance Options
				$product_post = get_post( $product_id );
				$enable_reviews = ( $product_post->comment_status == 'open' ) ? 'enable' : '';
				$menu_order = $product_post->menu_order;
				$purchase_note = get_post_meta( $product_id, '_purchase_note', true );
				
				// Yoast SEO Support
				if(WCMp_Frontend_Product_Manager_Dependencies::fpm_yoast_plugin_active_check()) {
					$yoast_wpseo_focuskw_text_input = get_post_meta( $product_id, '_yoast_wpseo_focuskw_text_input', true );
					$yoast_wpseo_metadesc = get_post_meta( $product_id, '_yoast_wpseo_metadesc', true );
				}
				
				// WooCommerce Custom Product Tabs Lite Support
				if(WCMp_Frontend_Product_Manager_Dependencies::fpm_wc_tabs_lite_plugin_active_check()) {
					$product_tabs = (array) get_post_meta( $product_id, 'frs_woo_product_tabs', true );
				}
				
				// WooCommerce Product Fees Support
				if(WCMp_Frontend_Product_Manager_Dependencies::fpm_wc_product_fees_plugin_active_check()) {
					$product_fee_name = get_post_meta( $product_id, 'product-fee-name', true );
					$product_fee_amount = get_post_meta( $product_id, 'product-fee-amount', true );
					$product_fee_multiplier = get_post_meta( $product_id, 'product-fee-multiplier', true );
				}
				
				// WooCommerce Bulk Discount Support
				if(WCMp_Frontend_Product_Manager_Dependencies::fpm_wc_bulk_discount_plugin_active_check()) {
					$_bulkdiscount_enabled = get_post_meta( $product_id, '_bulkdiscount_enabled', true );
					$_bulkdiscount_text_info = get_post_meta( $product_id, '_bulkdiscount_text_info', true );
					$_bulkdiscounts = (array) get_post_meta( $product_id, '_bulkdiscounts', true );
				}
			}
		}
		
		$is_vendor = false;
		$current_user_id = $current_vendor_id;
		if( is_user_wcmp_vendor( $current_user_id ) ) $is_vendor = true;
		
		// Shipping Class List
		$product_shipping_class = get_terms( 'product_shipping_class', array('hide_empty' => 0));
		$variation_shipping_option_array = array('-1' => __('Same as parent', 'wcmp_frontend_product_manager'));
		$shipping_option_array = array('_no_shipping_class' => __('No shipping class', 'wcmp_frontend_product_manager'));
		foreach($product_shipping_class as $product_shipping) {
			if( $is_vendor ) {
				$vendor_id = get_woocommerce_term_meta( $product_shipping->term_id, 'vendor_id', true );
				if(!$vendor_id)	{
					//$variation_shipping_option_array[$product_shipping->term_id] = $product_shipping->name;
					//$shipping_option_array[$product_shipping->term_id] = $product_shipping->name;
				} else {
					if($vendor_id == $current_user_id) {
						$variation_shipping_option_array[$product_shipping->term_id] = $product_shipping->name;
						$shipping_option_array[$product_shipping->term_id] = $product_shipping->name;
					}
				}
			} else {
				$variation_shipping_option_array[$product_shipping->term_id] = $product_shipping->name;
				$shipping_option_array[$product_shipping->term_id] = $product_shipping->name;
			}
		}
		
		// Tax Class List
		$tax_classes         = WC_Tax::get_tax_classes();
		$classes_options     = array();
		$variation_tax_classes_options['parent'] = __( 'Same as parent', 'wcmp_frontend_product_manager' );
		$variation_tax_classes_options[''] = __( 'Standard', 'wcmp_frontend_product_manager' );
		$tax_classes_options[''] = __( 'Standard', 'wcmp_frontend_product_manager' );

		if ( ! empty( $tax_classes ) ) {

			foreach ( $tax_classes as $class ) {
				$tax_classes_options[ sanitize_title( $class ) ] = esc_html( $class );
				$variation_tax_classes_options[ sanitize_title( $class ) ] = esc_html( $class );
			}
		}
		
		$args = array(
			'posts_per_page'   => -1,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'product',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'	   => $current_vendor_id,
			'post_status'      => array('publish'),
			'suppress_filters' => true 
		);
		$products_array = get_posts( $args );
		
		$product_categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0&parent=0' );
		$product_categories   = apply_filters( 'wcmp_frontend_product_cat_filter', $product_categories );
		global $wc_product_attributes;
	
		// Array of defined attribute taxonomies
		$attribute_taxonomies = wc_get_attribute_taxonomies();
		?>
		<form id="product_manager_form" class="woocommerce" method="POST">
			<div class="frontend_product_manager_product_types">
				<p class="product_type pro_title"><strong>Products</strong></p><label class="screen-reader-text" for="product_type">Products</label>
					<select id="product_type" name="vendor_product_type" class="regular-select">
					<?php
						$args = array( 'post_type' => 'product' ,'posts_per_page' => -1);
						$products = get_posts( $args );
						foreach( $products as $product ) {
						  echo '<option value="'.$product->ID.'">'.$product->post_title.'</option>';
						}
					?>
					</select>
			</div>
			<input type="hidden" name="vendor_product_id" value="<?php echo get_current_user_id();?>">
			<div id="product_manager_submit">
				<input type="submit" name="submit" value="<?php _e('Submit', 'wcmp_frontend_product_manager'); ?>" id="pruduct_manager_submit_button" />
			</div>
		</form>
		<?php
		
		do_action('wcmp-frontend-product-manager_template');

	}
}
