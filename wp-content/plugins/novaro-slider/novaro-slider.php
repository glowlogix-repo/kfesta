<?php
/*
Plugin Name: Novaro Slider
Plugin URI: http://www.novarostudio.com/
Description: Novaro Slider is a wordpress plugin for display a slider in the novaro studio theme.
Version: 1.0
Author: novarostudio
Author URI: http://www.novarostudio.com
License: GPL
*/

/*  Copyright 2015  Novaro Studio

    Novaro Slider is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);

class Novaro_Slider{
	
	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;
	
	function __construct(){
		
		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');
		
		//Register the Portfolio Menu
		add_action('init', array($this, 'novaro_pf_post_type'));
		add_action('init', array($this, 'novaro_pf_action_init'));
		add_action('after_setup_theme', array($this, 'novaro_pf_setup'));
		
		//Customize the Portfolio List in the wp-admin
		add_filter('manage_edit-slider-view_columns', array($this, 'novaro_pf_add_list_columns'));
		add_action('manage_slider-view_posts_custom_column', array($this, 'novaro_pf_manage_column'));
		//add_action( 'restrict_manage_posts', array($this, 'novaro_pf_add_taxonomy_filter') );
		
		$this->version		= $this->novaro_plugin_version();
		$this->postslug		= $this->novaro_postslug();
		$this->taxonomslug	= $this->novaro_taxonomslug();
		$this->posttype		= $this->novaro_posttype();
		$this->posttaxonomy	= $this->novaro_taxonomy();
	}
	
	//Get the version of portfolio
	function novaro_plugin_version(){
		$this->version = "1.0";
		
		return $this->version;
	}
	
	function novaro_lang(){
		$thelang = 'novaro';
		return $thelang;
	}
	
	function novaro_shortname(){
		$theshortname = 'novaro';
		return $theshortname;
	}
	
	function novaro_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}
	
	function novaro_posttype(){
		$this->posttype = 'slider-view';
		return $this->posttype;
	}
	
	function novaro_taxonomy(){
		$this->posttaxonomy = 'slidercat';
		return $this->posttaxonomy;
	}
	
	function novaro_postslug(){
		$this->postslug = 'slider-view';
		return $this->postslug;
	}
	
	function novaro_taxonomslug(){
		$this->taxonomslug = 'slidercat';
		return $this->taxonomslug;
	}
	
	function novaro_pf_md5hash($str = ''){
		return md5($str);
	}

	//Get the image size for every column
	function novaro_pf_setsize(){
	
		//set image size for every column in here.
		$this->imagesizes = array(
			
			
		);
		return $this->imagesizes;
	}
	
	function novaro_pf_setup(){
		add_theme_support( 'post-thumbnails' );
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			add_image_size( $imgsize["namesize"], $imgsize["width"], $imgsize["height"], true ); // Portfolio Thumbnail
		}
	}
	
	function novaro_pf_getthumbinfo($col){
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			if($col==$imgsize["num"]){
				return $imgsize;
			}
		}
		return false;
	}
	
	/* Make a Portfolio Post Type */
	function novaro_pf_post_type() {
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		$postslug = $this->novaro_postslug();
		$taxonomslug = $this->novaro_taxonomslug();
		
		register_post_type( $posttype,
					array( 
					'label' => __('Slider', $this->novaro_lang() ), 
					'public' => true, 
					'show_ui' => true,
					'show_in_nav_menus' => true,
					'rewrite' => array( 'slug' => $postslug, 'with_front' => false ),
					'hierarchical' => true,
					'menu_position' => 5,
					'has_archive' => true,
					'supports' => array(
										 'title',
										 'editor',
										 'thumbnail',
										 'excerpt',
										 'custom-fields',
										 'revisions')
						) 
					);
		register_taxonomy($taxonom, $posttype, array(
			'hierarchical' => true,
			'label' =>  __('Slider Categories', $this->novaro_lang()),
			'query_var' => true,
			'rewrite' => array( 'slug' => $taxonomslug, 'with_front' => false ),
			'show_ui' => true,
			'singular_name' => 'Category'
			));
	}
	
	function novaro_pf_add_list_columns($portfolio_columns){
		
		$thetaxonomy = $this->novaro_taxonomy();
		$new_columns = array();
		$new_columns['cb'] = '<input type="checkbox" />';
		
		$new_columns['title'] = __('Slider Title', $this->novaro_lang());
		$new_columns['images'] = __('Images', $this->novaro_lang());
		$new_columns['author'] = __('Author', $this->novaro_lang());
		
		$new_columns[$thetaxonomy] = __('Categories', $this->novaro_lang());
		
		$new_columns['date'] = __('Date', $this->novaro_lang());
		
		return $new_columns;
	}
	
	function novaro_pf_manage_column($column_name){
		global $post;
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		
		$id = $post->ID;
		$title = $post->post_title;
		switch($column_name){
			case 'images':
				$thumbnailid = get_post_thumbnail_id($id);
				$imagesrc = wp_get_attachment_image_src($thumbnailid, 'thumbnail');
				if($imagesrc){
					echo '<img src="'.$imagesrc[0].'" width="50" alt="'.$title.'" />';
				}else{
					_e('No Featured Image', $this->novaro_lang());
				}
				break;
			
			case $taxonom:
				$postterms = get_the_terms($id, $taxonom);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonom.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$termtext = implode(", ",$termlists);
						echo $termtext;
					}
				}
				
				break;
		}
	}
	
	/* Filter Custom Post Type Categories */
	function novaro_pf_add_taxonomy_filter() {
		global $typenow;
		$posttype = $this->novaro_posttype();
		$taxonomy = $this->novaro_taxonomy();
		if( $typenow==$posttype){
			$filters = array($taxonomy);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo '<select name="'. esc_attr( $tax_slug ).'" id="'. esc_attr( $tax_slug ) .'" class="postform">';
				echo "<option value=''>".__('View All',$this->novaro_lang())." "."$tax_name</option>";
				foreach ($terms as $term) { 
					$selectedstr = '';
					if(isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug){
						$selectedstr = ' selected="selected"';
					}
					echo '<option value='. $term->slug. $selectedstr . '>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
	
	function novaro_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages
		
		$version = $this->novaro_plugin_version();
	}
	
	// The excerpt based on character
	function novaro_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}
	
}

$theslider = new Novaro_Slider();
?>