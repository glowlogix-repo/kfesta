<?php

define('WCMP_FRONTEND_PRODUCT_MANAGER_PLUGIN_TOKEN', 'wcmp-frontend-product-manager');

define('WCMP_FRONTEND_PRODUCT_MANAGER_TEXT_DOMAIN', 'wcmp_frontend_product_manager');

define('WCMP_FRONTEND_PRODUCT_MANAGER_PLUGIN_VERSION', '2.1.5');

define('WCMP_FRONTEND_PRODUCT_MANAGER_PLUGIN_SERVER_URL', 'https://wc-marketplace.com');

?>