﻿=== WooCommerce Address Validation & Google Address Autocomplete Plugin ===
Contributors: nandanamahanta,varun874,xadapter, niwf
Donate link: 
Tags: address validation, address verification, google address autocomplete, postcode check, zipcode check.
Requires at least: 3.0.1
Tested up to: 4.8
Stable tag: 1.1.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Simple and easy to use address validation & google address autocomplete plugin. Uses EasyPost, UPS & Google APIs.

== Description ==
WooCommerce Address Validation & Google Address Autocomplete Plugin helps to improve customer service by assisting shoppers when they place orders. The plugin will help you to  decrease the number of delivery delays due to an incorrect address entry.

This plugin aids in validating & verifying the addresses of the customers before placing order to maximize your store's throughput. Integrated with <a rel="nofollow" href="https://www.easypost.com/">EasyPost</a> Address Verification API(Free) & <a rel="nofollow" href="https://www.ups.com/">UPS</a> Address Validation API(Premium). 
Also, plugin comes with Address Auto-Correction & autocompletion using super efficient <a rel="nofollow" href="https://developers.google.com/maps/documentation/javascript/places-autocomplete">Google Address Autocomplete API</a>.

Features:
<ul>
<li>Address autocomplete using Google Address Autocomplete API.</li>
<li>Automatic Address validation using EasyPost Address Verification APIs</li>
<li>Option to toggle manual and automatic address validation.</li>
<li>Customisable Popup window for user prompted manual address validation.</li>
</ul>

Prerequisite: 
Get an <a rel="nofollow" href="https://www.easypost.com/docs/api.html">EasyPost API key</a> to start using. Easy to use address validation plugin in the market.
Get <a rel="nofollow" href="https://developers.google.com/places/web-service/autocomplete">Google autocomplete API Key</a> to activate address autocomplete feature.

= About Google Address Autocomplete Feature =

The Google Places API Web Service and Place Autocomplete share a usage quota as described in the <a href="https://developers.google.com/places/web-service/usage" target="_blank">Usage Limits documentation</a> for Google Places API Web Service. These usage limits can be lifted by placing a request to google as explained in their article. The daily usage is calculated as the sum of client-side and server-side requests combined.

<a href="https://developers.google.com/maps/documentation/javascript/reference#Autocomplete" target="_blank">Google Address Autocomplete</a> feature adds a text input field to your woocommerce checkout page, and monitors that field for character entries. As the user enters text, google autocomplete returns place predictions in the form of a dropdown pick list. See more details <a href="https://developers.google.com/maps/documentation/javascript/places-autocomplete#add_autocomplete" target="_blank">here</a>.

= About EasyPost Address Verification =

With EasyPost Address Verification, aggregate and leverage as much data as possible to create one large international network that improves upon itself. EasyPost have complete coverage of the United States and ever growing coverage internationally. EasyPost operate in every country, with our granularity reaching down to the unit level in many countries.

Simply put, nobody has the reach or reliability of EasyPost Address Verification API. Improve deliverability and increase customer confidence by running your addresses through EasyPost.

= About UPS Address Validation (Premium) =

Add Web-based United States address validation capabilities to your WooCommerce Web Store. The UPS Address Validation Application Programming Interface (API) can provide direct automated integration between your website or enterprise system and UPS. As with all APIs, your customers will enjoy the depth of UPS services and capabilities, while your business becomes more efficient with improved processes.

The UPS Address Validation API checks a customer's address and provides suggested alternatives if an error is discovered. After integrating it with your WooCommerce Web  Store, this API can help you reduce operating costs and improve customer service.

The UPS Address Validation API requires XML/Web Services programmers to code and integrate. 

Available for use in over 40 countries, the UPS Address Validation API is a valuable addition to any business shipping packages to the United States. For example, if your U.S. customer enters an incorrect postal code while placing their online order, they are provided with suggested alternatives. 

<blockquote>

= Premium Version Features =
<ul>
 <li>Address validation using UPS Address validation API.</li>
 <li>Timely compatibility updates and bug fixes.</li>
 <li>Premium Support: Faster and time bound response for support requests.</li>
</ul>

For complete list of features and details, Please visit <a rel="nofollow" href="https://www.xadapter.com/product/address-validation-auto-complete-plugin-woocommerce/">Address Validation and Auto Complete Plugin for WooCommerce</a>.

</blockquote>

== Frequently Asked Questions ==

= How granular is your address validation? =

It depends on where the address is. For certain countries, we can get as granular as the housing unit level.

= Does it work for my country? =

You can find the list of supported countries over <a href="https://www.easypost.com/docs/address-verification-by-country.html">here</a>.

= Are the addresses categorized under residential and commercial? = 

Yes. We do we provide RDI (Residential Delivery Indicator) to aid in categorizing addresses as residential and commercial. 

= Will this woocommerce plugin do zip code check / postcode check as part of Address Validation? = 
Yes. Our plugin will check and validate zip code / postcode as part of Address Validation.

= About XAdapter.com =

XAdapter creates quality WordPress/WooCommerce plugins that are easy to use and customize. We are proud to have thousands of customers actively using our plugins across the globe.

== Installation ==

1. Upload the plugin folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the Plugins menu in WordPress.
3. That's it ! you can now configure the plugin.


== Screenshots ==

1. Address Suggestion Portal

2. Address Autocomplete option on checkout page

3. Settings Page

== Changelog == 

= 1.1.7 =
* Fixed PHP warning.

= 1.1.6 =
* compatible with Xadapter easypost plugin.

= 1.1.5 =
* Fixed Issue with Guest Checkout

= 1.1.4 =
* Fixed Compatibility issue with PHP 7.0

= 1.1.3 =
* Fixed Compatibility issue with older version of woocommerce (For below 3.0)

= 1.1.2 =
* Added option to enforce checkout on validated address only

= 1.1.1 =
* Updated Screenshots

= 1.1.0 =
* Added Background Address Validation & custom CSS

= 1.0.3 =
* Marketing Content Changed

= 1.0.2 =
* Minor Content Changed

1.0.1
Minor Content Changed

1.0.0
Initial Version

== Upgrade Notice ==

= 1.1.7 =
* Fixed PHP warning.

= 1.1.6 =
* compatible with Xadapter easypost plugin.

= 1.1.5 =
* Fixed Issue with Guest Checkout

= 1.1.4 =
* Fixed Compatibility issue with PHP 7.0

= 1.1.3 =
* Fixed Compatibility issue with older version of woocommerce (For below 3.0)

= 1.1.2 =
* Added option to enforce checkout on validated address only

= 1.1.1 =
* Updated Screenshots

= 1.1.0 =
* Added Background Address Validation & custom CSS

= 1.0.3 =
* Marketing Content Changed

= 1.0.2 =
* Minor Content Changed

1.0.1
Minor Content Changed
