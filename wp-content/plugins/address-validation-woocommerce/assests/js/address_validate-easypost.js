jQuery( function( $ ) {


		$("#xa_myModal").css("display","none");

	var result='';
	//for closing the suggestion popup
	$("span.xa-closebtn").on("click",function(){
		$( '#xa_myModal' ).css("display","none");
		$("#place_order").removeProp("disabled");
	});
	//for the recheck address button
	$("body").on("click","button.recheck",function(){
		$( '#xa_myModal' ).css("display","none");
		$("#place_order").removeProp("disabled");
	});
	//for using the original address from popup
	$("button.use_original").on("click",function(){
		$( '#xa_myModal' ).css("display","none");
		$("#place_order").removeProp("disabled");
		var form=$( "form.checkout, form#order_review, form#add_payment_method" ) ;
		form.submit();
	});
	//for using the validated address suggestion from popup
	$("body").on("click","button.use_validated",function(){
		$( '#xa_myModal' ).css("display","none");
		$("#place_order").removeProp("disabled");
		if($('#ship-to-different-address-checkbox').is(':checked')){
			$("#shipping_address_1").val(result.street1);
			$("#shipping_address_2").val(result.street2);
			$("#shipping_city").val(result.city);
			$("#shipping_postcode").val(result.zip.split("-")[0]);
			$('#shipping_country').val(result.country).trigger("change");
			$('#shipping_state').val(result.state).trigger("change");
		}
		else{
			$("#billing_address_1").val(result.street1);
			$("#billing_address_2").val(result.street2);
			$("#billing_city").val(result.city);
			$("#billing_postcode").val(result.zip.split("-")[0]);
			$('#billing_country').val(result.country).trigger("change");
			$('#billing_state').val(result.state).trigger("change");	
		}
		var form=$( "form.checkout, form#order_review, form#add_payment_method" ) ;
		form.submit();
	});
	//to hook the place order button from checkpout page
	$("body").on("click","#place_order",function(){
		var street1,street2,city,state,zip,country='';
		$("#address_rdi").val('');
		if($('#ship-to-different-address-checkbox').is(':checked'))
		{
			if( $("#shipping_address_1").val()=='' || 
			$("#shipping_city").val()=='' || 
			$("#shipping_state").val()=='' || 
			$("#shipping_postcode").val()=='' || 
			$("#shipping_country").val()==''){	
				//return true;
			}
			street1 = $("#shipping_address_1").val();
			street2 = $("#shipping_address_2").val();
			city = $("#shipping_city").val();
			state = $("#shipping_state").val();
			zip = $("#shipping_postcode").val();
			country = $("#shipping_country").val();
		}
		else
		{
			if( $("#billing_address_1").val()=='' || 
			$("#billing_city").val()=='' || 
			$("#billing_state").val()=='' || 
			$("#billing_postcode").val()=='' || 
			$("#billing_country").val()==''){	
				//return true;
			}
			street1 = $("#billing_address_1").val();
			street2 = $("#billing_address_2").val();
			city = $("#billing_city").val();
			state = $("#billing_state").val();
			zip = $("#billing_postcode").val();
			country = $("#billing_country").val();
		}
		$("#place_order").prop("disabled","disabled");		
		
		//ajax call to send data to php
		$.ajax({
                 type: 'post',
                 url: wc_checkout_params.ajax_url,
                 data: 
				{
                     action: 'wf_address_validation',
					 street1_post:street1,
					 street2_post:street2,
					 city_post:city,
					 state_post:state,
					 zip_post:zip,
					 country_post:country
                 },
				 //get response back from php
                 success: function(response) {
					result = $.parseJSON(response);
					// if validation fails Proceed with user given data
					if (result.status == 'failure') {
							$("#place_order").removeProp("disabled");
							var form=$( "form.checkout, form#order_review, form#add_payment_method" ) ;
							form.submit();
							return true;
					}
					//check if the owner has any map restrictions
					if(result.map == 'undefined')
					{
						var form=$( "form.checkout, form#order_review, form#add_payment_method" ) ;
						form.submit();
						return true;
					}

					$('#original').empty();
					$('#validated').empty();
					$('#right_title').empty();
					$('#right_button').empty();
					if(result.street1!=street1){
						$('#original').empty();
						$('#original').append('<span style="background-color:yellow !important;">'+street1+'</span>');
					}
					else{
						$('#original').empty();
						$('#original').append(street1);
					}	
					$('#original').append(',<br>');
					if(street2 != '')
					{
						if(result.street2.toLowerCase()!=street2.toLowerCase()){
							$('#original').append('<span style="background-color:yellow !important;">'+street2+'</span>');
						}
						else
						$('#original').append(street2);
						$('#original').append(',<br>');
					}
					if(result.city && result.city.toLowerCase()!=city.toLowerCase()){
						$('#original').append('<span style="background-color:yellow !important;">'+city+'</span>');
					}
					else
					$('#original').append(city);
					$('#original').append(', ');
					if(result.state.toLowerCase()!=state.toLowerCase()){
						$('#original').append('<span style="background-color:yellow !important;">'+state+'</span>');
					}
					else	
					$('#original').append(state);
					$('#original').append(', ');
					if(result.country.toLowerCase()!=country.toLowerCase()){
						$('#original').append('<span style="background-color:yellow !important;">'+country+'</span>');
					}
					else						
					$('#original').append(country);
					$('#original').append(' - ');
					if(result.zip.split("-")[0]!=zip){
						$('#original').append('<span style="background-color:yellow !important;">'+zip+'</span>');
					}
					else
					$('#original').append(zip);
					if(result.status=='success')
					{
						$('#right_title').append("<center><bold>Validation Successful</bold></center>");
						$('#right_button').append("<center><button class='xa-btn xa-white xa-round-large xa-border use_validated'>Place Order with Suggested Address</button></center>");
						if(result.rdi == true){
							$("#address_rdi").val('Residential');
						}
						else{
							$("#address_rdi").val('Commercial');
						}
						$('#validated').append(result.street1);
						$('#validated').append(',<br>');
						if(result.street2 != '')
						{
							$('#validated').append(result.street2);
							$('#validated').append(',<br>');
						}
						$('#validated').append(result.city);
						$('#validated').append(', ');
						$('#validated').append(result.state);	
						$('#validated').append(', ');
						$('#validated').append(result.country);
						$('#validated').append(' - ');
						$('#validated').append(result.zip.split("-")[0]);
						$( '#xa_myModal' ).css("display","block");
						
						//..Checkout on the validated data received from Easypost server without giving option to select the users
						if(wf_address_autocomplete_validation_enable_address_popup_obj.enable == 'no')
						{
						    $("#place_order").removeProp("disabled");
						    if($('#ship-to-different-address-checkbox').is(':checked')){
							    $("#shipping_address_1").val(result.street1);
							    $("#shipping_address_2").val(result.street2);
							    $("#shipping_city").val(result.city);
							    $("#shipping_postcode").val(result.zip.split("-")[0]);
							    $('#shipping_country').val(result.country).trigger("change");
							    $('#shipping_state').val(result.state).trigger("change");
						    }
						    else{
							    $("#billing_address_1").val(result.street1);
							    $("#billing_address_2").val(result.street2);
							    $("#billing_city").val(result.city);
							    $("#billing_postcode").val(result.zip.split("-")[0]);
							    $('#billing_country').val(result.country).trigger("change");
							    $('#billing_state').val(result.state).trigger("change");	
						    }
						    var form=$( "form.checkout, form#order_review, form#add_payment_method" ) ;
						    form.submit();
						}
						
					}
					else	//for invalid addresses
					{
						$('#right_title').append("<center><bold>Address Validation Failed</bold></center>");
						$('#right_button').append("<center><button class='xa-btn xa-white xa-round-large xa-border recheck'>Recheck Address</button></center>");
						$( '#xa_myModal' ).css("display","block");	
					}		
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(textStatus, errorThrown);
                 }
             });
		return false;
		});
});