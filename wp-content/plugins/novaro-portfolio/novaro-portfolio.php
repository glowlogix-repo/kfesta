<?php
/*
Plugin Name: Novaro Portfolio
Plugin URI: http://www.novarostudio.com/
Description: Novaro Portfolio is a wordpress plugin for display personal portfolio.
Version: 1.0.1
Author: novarostudio
Author URI: http://www.novarostudio.com
License: GPL
*/

/*  Copyright 2015  Novaro Studio

    Novaro Portfolio is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);

class Novaro_Portfolio{
	
	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;
	
	function __construct(){
		// Register the shortcode to the function ep_shortcode()
		add_shortcode( 'portfolio_filter', array($this, "novaro_portfoliofilter"));
		add_shortcode( 'portfolio_carousel', array($this, "novaro_portfoliocarousel"));
		
		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');
		
		//Register the Portfolio Menu
		add_action('init', array($this, 'novaro_pf_post_type'));
		add_action('init', array($this, 'novaro_pf_action_init'));
		add_action('after_setup_theme', array($this, 'novaro_pf_setup'));
		
		//Customize the Portfolio List in the wp-admin
		add_filter('manage_edit-portofolio_columns', array($this, 'novaro_pf_add_list_columns'));
		add_action('manage_portofolio_posts_custom_column', array($this, 'novaro_pf_manage_column'));
		add_action( 'restrict_manage_posts', array($this, 'novaro_pf_add_taxonomy_filter') );
		
		$this->version		= $this->novaro_plugin_version();
		$this->postslug		= $this->novaro_postslug();
		$this->taxonomslug	= $this->novaro_taxonomslug();
		$this->posttype		= $this->novaro_posttype();
		$this->posttaxonomy	= $this->novaro_taxonomy();
	}
	
	//Get the version of portfolio
	function novaro_plugin_version(){
		$this->version = "1.0";
		
		return $this->version;
	}
	
	function novaro_lang(){
		$thelang = 'novaro';
		return $thelang;
	}
	
	function novaro_shortname(){
		$theshortname = 'novaro';
		return $theshortname;
	}
	
	function novaro_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}
	
	function novaro_posttype(){
		$this->posttype = 'portofolio';
		return $this->posttype;
	}
	
	function novaro_taxonomy(){
		$this->posttaxonomy = 'portfoliocat';
		return $this->posttaxonomy;
	}
	
	function novaro_postslug(){
		$this->postslug = 'portofolio';
		return $this->postslug;
	}
	
	function novaro_taxonomslug(){
		$this->taxonomslug = 'portfoliocat';
		return $this->taxonomslug;
	}
	
	function novaro_pf_md5hash($str = ''){
		return md5($str);
	}

	//Get the image size for every column
	function novaro_pf_setsize(){
	
		//set image size for every column in here.
		$this->imagesizes = array(
			array(
				"num"		=> 'default',
				"namesize"	=> 'portfolio-image',
				"width" 	=> 700,
				"height" 	=> 424,
                "crop"      => true
			),
			array(
				"num"		=> 'square',
				"namesize"	=> 'portfolio-image-square',
				"width" 	=> 700,
				"height" 	=> 700,
                "crop"      => true
			),
			array(
				"num"		=> 'portrait',
				"namesize"	=> 'portfolio-image-portrait',
				"width" 	=> 700,
				"height" 	=> 9999,
                "crop"      => false
			),
			array(
				"num"		=> 'landscape',
				"namesize"	=> 'portfolio-image-landscape',
				"width" 	=> 700,
				"height" 	=> 424,
                "crop"      => true
			)
			
		);
		return $this->imagesizes;
	}
	
	function novaro_pf_setup(){
		add_theme_support( 'post-thumbnails' );
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			add_image_size( $imgsize["namesize"], $imgsize["width"], $imgsize["height"], $imgsize["crop"] ); // Portfolio Thumbnail
		}
	}
	
	function novaro_pf_getthumbinfo($col){
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			if($col==$imgsize["num"]){
				return $imgsize;
			}
		}
		return false;
	}
	
	function novaro_pf_get_image($nvr_imgsize, $nvr_postid=""){
	
		global $post;
		$nvr_initial = $this->novaro_initial();
		$nvr_shortname = $this->novaro_shortname();
		
		if($nvr_postid==""){
			$nvr_postid = get_the_ID();
		}
	
		$nvr_custom = get_post_custom( $nvr_postid );
		$nvr_cf_thumb = (isset($nvr_custom["custom_thumb"][0]))? $nvr_custom["custom_thumb"][0] : "";
		$nvr_cf_externallink = (isset($nvr_custom["external_link"][0]))? $nvr_custom["external_link"][0] : "";
		$nvr_cf_imagegallery	= (isset($nvr_custom[$nvr_initial."_imagesgallery"][0]))? $nvr_custom[$nvr_initial."_imagesgallery"][0] : "";
		
		if(isset($nvr_custom["lightbox_img"])){
			$nvr_checklightbox = $nvr_custom["lightbox_img"] ; 
			$nvr_cf_lightbox = array();
			for($i=0;$i<count($nvr_checklightbox);$i++){
				if($nvr_checklightbox[$i]){
					$nvr_cf_lightbox[] = $nvr_checklightbox[$i];
				}
			}
			if(!count($nvr_cf_lightbox)){
				$nvr_cf_lightbox = "";
			}
		}else{
			$nvr_cf_lightbox = "";
		}
		
		if($nvr_cf_imagegallery!=''){
			$nvr_attachments = $nvr_cf_imagegallery;
			$nvr_attachmentids = explode(",",$nvr_attachments);
			$nvr_qryposts = array(
				'include' => $nvr_attachmentids,
				'post_status' => 'any',
				'post_type' => 'attachment'
			);
			
			$nvr_attachments = get_posts( $nvr_qryposts );
		}else{
			$nvr_qrychildren = array(
				'post_parent' => $nvr_postid ,
				'post_status' => null,
				'post_type' => 'attachment',
				'order_by' => 'menu_order',
				'order' => 'ASC',
				'post_mime_type' => 'image'
			);
		
			$nvr_attachments = get_children( $nvr_qrychildren );
		}
		
		$nvr_cf_thumb2 = array();
		$nvr_cf_full2 = "";
		$z = 1;
		foreach ( $nvr_attachments as $nvr_att_id => $nvr_attachment ) {
			$nvr_att_id = $nvr_attachment->ID;
			$nvr_getimage = wp_get_attachment_image_src($nvr_att_id, $nvr_imgsize, true);
			$nvr_portfolioimage = $nvr_getimage[0];
			$nvr_alttext = get_post_meta( $nvr_attachment->ID , '_wp_attachment_image_alt', true);
			$nvr_image_title = $nvr_attachment->post_title;
			$nvr_caption = $nvr_attachment->post_excerpt;
			$nvr_description = $nvr_attachment->post_content;
			$nvr_cf_thumb2[] ='<img src="'.esc_url( $nvr_portfolioimage ).'" alt="'.esc_attr( $nvr_alttext ).'" title="'. esc_attr( $nvr_image_title ) .'" class="scale-with-grid" />';
			
			$nvr_getfullimage = wp_get_attachment_image_src($nvr_att_id, 'full', true);
			$nvr_fullimage = $nvr_getfullimage[0];
			
			if($z==1){
				$nvr_fullimageurl = $nvr_fullimage;
				$nvr_fullimagetitle = $nvr_image_title;
				$nvr_fullimagealt = $nvr_alttext;
			}elseif($nvr_att_id == get_post_thumbnail_id( $nvr_postid ) ){
				$nvr_cf_full2 ='<a data-rel="prettyPhoto['.esc_attr( $post->post_name ).']" href="'.esc_url( $nvr_fullimageurl ).'" title="'. esc_attr( $nvr_fullimagetitle ) .'" class="hidden"></a>'.$nvr_cf_full2;
				$nvr_fullimageurl = $nvr_fullimage;
				$nvr_fullimagetitle = $nvr_image_title;
				$nvr_fullimagealt = $nvr_alttext;
			}else{
				$nvr_cf_full2 .='<a data-rel="prettyPhoto['.esc_attr( $post->post_name ).']" href="'.esc_url( $nvr_fullimage ).'" title="'. esc_attr( $nvr_image_title ) .'" class="hidden"></a>';
			}
			$z++;
		}
		
		if($nvr_cf_thumb!=""){
			$nvr_cf_thumb = '<img src="' . esc_url( $nvr_cf_thumb ) . '" alt="'. esc_attr( get_the_title($nvr_postid) ) .'"  class="scale-with-grid" />';
		}elseif( has_post_thumbnail( $nvr_postid ) ){
			$nvr_cf_thumb = get_the_post_thumbnail($nvr_postid, $nvr_imgsize, array('class' => 'scale-with-grid'));
		}elseif( isset( $nvr_cf_thumb2[0] ) ){
			$nvr_cf_thumb = $nvr_cf_thumb2[0];
		}else{
			$nvr_cf_thumb = '<span class="nvr-noimage"></span>';
		}
		
		
		if($nvr_cf_externallink!=""){
			$nvr_golink = $nvr_cf_externallink;
			$nvr_rollover = "gotolink";
			$nvr_atext = __('More',$this->novaro_lang());
			$nvr_cf_full2 = '';
		}else{
			$nvr_golink = get_permalink();
			$nvr_rollover = "gotopost";
			$nvr_atext = __('More',$this->novaro_lang());
		}
		
		$nvr_bigimageurl = $nvr_bigimagetitle = $nvr_rel = '';
		if( is_array($nvr_cf_lightbox) ){
			$nvr_bigimageurl = $nvr_cf_lightbox[0];
			$nvr_bigimagetitle = get_the_title();
			$nvr_rel = ' data-rel="prettyPhoto['. esc_attr( $post->post_name ).']"';
			$nvr_cf_lightboxoutput = '';
			for($i=1;$i<count($nvr_cf_lightbox);$i++){
				$nvr_cf_lightboxoutput .='<a data-rel="prettyPhoto['.esc_attr( $post->post_name ).']" href="'.esc_url( $nvr_cf_lightbox[$i] ).'" title="'. esc_attr( get_the_title($nvr_postid) ) .'" class="hidden"></a>';
			}
			$nvr_cf_full2 = $nvr_cf_lightboxoutput;
		}else{
			if( isset($nvr_fullimageurl)){
				$nvr_bigimageurl = $nvr_fullimageurl; 
				$nvr_bigimagetitle = $nvr_fullimagetitle;
				$nvr_rel = ' data-rel="prettyPhoto['.esc_attr( $post->post_name ).']"';
			}
		}
		
		$nvr_return = array(
			'nvr_bigimageurl' 	=> $nvr_bigimageurl,
			'nvr_bigimagetitle'	=> $nvr_bigimagetitle,
			'nvr_rel'			=> $nvr_rel,
			'nvr_cf_full2'		=> $nvr_cf_full2,
			'nvr_golink'		=> $nvr_golink,
			'nvr_rollover'		=> $nvr_rollover,
			'nvr_atext'			=> $nvr_atext,
			'nvr_cf_thumb'		=> $nvr_cf_thumb
		);
		return $nvr_return;
	}

	function novaro_pf_get_box( $nvr_imgsize, $nvr_postid="",$nvr_class="", $nvr_limitchar = 250 ){
	
		$nvr_output = "";
		global $post;
		
		if($nvr_postid==""){
			$nvr_postid = get_the_ID();
		}
		$nvr_taxonomy_slug = $this->novaro_taxonomy();
		
		$nvr_get_image = $this->novaro_pf_get_image($nvr_imgsize, $nvr_postid );
		extract($nvr_get_image);
		
		$nvr_output  .='<li class="'.esc_attr( $nvr_class ).'">';
			$nvr_output  .='<div class="nvr-pf-box">';
				$nvr_output  .='<div class="nvr-pf-img">';
					
					$nvr_output .='<a class="image '.esc_attr( $nvr_rollover ).'" href="'.esc_url( $nvr_golink ).'" title="'.esc_attr( get_the_title($nvr_postid) ).'"></a>';
					if($nvr_bigimageurl!=''){
						$nvr_output .='<a class="image zoom" href="'. esc_url( $nvr_bigimageurl ) .'" '.$nvr_rel.' title="'.esc_attr( $nvr_bigimagetitle ).'"></a>';
					}
					
					$nvr_output  .=$nvr_cf_thumb;
					$nvr_output  .=$nvr_cf_full2;
				$nvr_output  .='</div>';
		
				$nvr_excerpt = $this->novaro_pf_limit_char( get_the_excerpt(), $nvr_limitchar );
				$nvr_output  .='<div class="nvr-pf-text">';
				
					$nvr_output  .='<h2 class="nvr-pf-title"><a href="'.esc_url( get_permalink($nvr_postid) ).'" title="'.esc_attr( get_the_title($nvr_postid) ).'">'.get_the_title($nvr_postid).'</a></h2>';
					 // get the terms related to post
					$nvr_terms = get_the_terms( $nvr_postid, $nvr_taxonomy_slug );
					$nvr_termarr = array();
					if ( !empty( $nvr_terms ) ) {
					  foreach ( $nvr_terms as $nvr_term ) {
						$nvr_termarr[] = '<a href="'. esc_url( get_term_link( $nvr_term->slug, $nvr_taxonomy_slug ) ).'">'. $nvr_term->name ."</a>";
					  }
					  
					  $nvr_output .= '<div class="nvr-pf-cat">'.implode(", ", $nvr_termarr).'</div>';
					}
					$nvr_output .= '<div class="nvr-pf-separator"></div>';
					$nvr_output .= '<div class="nvr-pf-content">'.$nvr_excerpt.'</div>';
					
				$nvr_output  .='</div>';
				$nvr_output  .='<div class="nvr-pf-clear"></div>';
			$nvr_output  .='</div>';
		$nvr_output  .='</li>';
		
		return $nvr_output; 
	}
	
	//make the shortcode
	function novaro_portfoliofilter($atts, $content = null){
		extract(shortcode_atts(array(
					"title" => '',
					"cat" => '',
					'type' => 'grid',
					"col" => 4,
					'nospace' => '',
					"showposts" => '-1'
		), $atts));
		
		$nvr_initial = $this->novaro_initial();
		
		$cats = $cat;
		$showpost = $showposts;
		$orderby = "date";
		$ordersort = "DESC";
		$categories = explode(",",$cats);
	
		if($type!= 'grid' && $type!='classic' && $type!='masonry'){
			$type = 'grid';
		}
		
		if($col<3 || $col>5){
			$col = 4;
		}
		$type.='-'.$col;
		$arrtype = explode("-",$type);
		$ptype = $arrtype[0];
		if($nospace=="yes"){
			$pspace = 'nospace';
		}else{
			$pspace = 'space';
		}
		$column = intval($arrtype[1]);
		$freelayout = false;
		
		$approvedcats = array();
		foreach($categories as $category){
			$catname = get_term_by('slug',$category, $this->novaro_taxonomy() );
			if($catname!=false){
				$approvedcats[] = $catname;
			}
		}
		
		$catslugs = array();
		$nvr_outputfilter = '';
		if(count($approvedcats)>1){
			$nvr_outputfilter .= '<ul id="filters" class="option-set clearfix " data-option-key="filter">';
				$nvr_outputfilter .= '<li class="alpha selected"><a href="#filter" data-option-value="*">'. __('All Categories', $this->novaro_lang() ).'</a></li>';
				$filtersli = '';
				$numli = 1;
				foreach($approvedcats as $approvedcat){
					if($numli==1){
						$liclass = 'omega';
					}else{
						$liclass = '';
					}
					$filtersli = '<li class="'.esc_attr( $liclass ).'"><a href="#filter" data-option-value=".'. esc_attr( $approvedcat->slug ).'">'.$approvedcat->name.'</a></li>'.$filtersli;
					$catslugs[] = $approvedcat->slug;
					$numli++;
				}
				$nvr_outputfilter .= $filtersli;
			$nvr_outputfilter .= '</ul>';
			$hasfilter = true;
		}elseif(count($approvedcats)==1){
			$catslugs[] = $approvedcats[0]->slug;
			$hasfilter = false;
		}else{
			$hasfilter = false;
		}
	
		$idnum = 0;
	
		if($column!= 3 && $column!= 4 && $column!= 5 ){
			$column = 4;
		}
		$pfcontainercls = "nvr-pf-col-".$column;
		$pfcontainercls .= " ".$ptype;
		$pfcontainercls .= " ".$pspace;
		$imgsize = "portfolio-image";
		
		if($showpost==""){$showpost="-1";}
		
		$nvr_argquery = array(
			'post_type' => $this->novaro_posttype(),
			'orderby' => $orderby,
			'order' => $ordersort
		);
		$nvr_argquery['showposts'] = $showpost;
		
		if(count($catslugs)>0){
			$nvr_argquery['tax_query'] = array(
				array(
					'taxonomy' => $this->novaro_taxonomy(),
					'field' => 'slug',
					'terms' => $catslugs
				)
			);
		}
		
		$nvr_pfqry = new WP_Query($nvr_argquery);
		
		$nvr_output = '<div class="portfolio_filter">';
			$nvr_output .= $nvr_outputfilter;
			$nvr_output .= '<div class="nvr-pf-container row">';
				$nvr_output .= '<ul id="nvr-pf-filter" class="'. esc_attr( $pfcontainercls ) .'">';
				
				if( $nvr_pfqry->have_posts() ){
					while ( $nvr_pfqry->have_posts() ) : $nvr_pfqry->the_post(); 
							
							$idnum++;
							if(!$freelayout){
								if($column=="5"){
									$classpf = 'one_fifth columns ';
								}elseif($column=="4"){
									$classpf = 'three columns ';
								}else{
									$classpf = 'four columns ';
								}
							}else{
								$classpf = 'free columns ';
							}
							
							if(($idnum%$column) == 1){ $classpf .= "first ";}
							if(($idnum%$column) == 0){$classpf .= "last ";}
							
							$custompf = get_post_custom( get_the_ID() );
							
							$pimgsize = '';
							if($ptype=='masonry'){
								$pimgsize = (isset($custompf["_".$nvr_initial."_pimgsize"][0]))? $custompf["_".$nvr_initial."_pimgsize"][0] : "";
								
								if($pimgsize=='square'){
									$imgsize = 'portfolio-image-square';
								}elseif($pimgsize=='portrait'){
									$imgsize = 'portfolio-image-portrait';
								}elseif($pimgsize=='landscape'){
									$imgsize = 'portfolio-image-landscape';
								}
								$classpf .= $pimgsize.' ';
							}elseif($ptype=='grid'){
								$imgsize = 'portfolio-image-square';
								$pimgsize='square';
							}
							$classpf .= 'imgsize-'.$pimgsize.' ';
							
							$thepfterms = get_the_terms( get_the_ID(), $this->novaro_taxonomy() );
							
							$literms = "";
							if ( $thepfterms && ! is_wp_error( $thepfterms ) ){
				
								$approvedterms = array();
								foreach ( $thepfterms as $term ) {
									$approvedterms[] = $term->slug;
								}			
								$literms = implode( " ", $approvedterms );
							}
							
							$nvr_output .= $this->novaro_pf_get_box( $imgsize, get_the_ID(), $classpf.' element '.$literms );
								
							$classpf=""; 
								
					endwhile; // End the loop. Whew.
				}
	
				$nvr_output .= '<li class="pf-clear"></li>';
				$nvr_output .= '</ul>';
				$nvr_output .= '<div class="clearfix"></div>';
			$nvr_output .= '</div><!-- end #nvr-portfolio -->';
		$nvr_output .= '</div>';
		
		wp_reset_postdata();
		
		return $nvr_output;
	}
	
	function novaro_portfoliocarousel($atts, $content = null) {
		extract(shortcode_atts(array(
					"title" => '',
					"cat" => '',
					'nospace' => 'no',
					"showposts" => '-1',
					"full" => 'no',
		), $atts));
			
			$nvr_pcclass = '';
			
			$nvr_outputtitle = '';
			if($title!=''){
				$nvr_outputtitle = '<div class="titlecontainer"><h3><span>'. $title .'</span></h3></div>';
				$nvr_pcclass .= ' hastitle';
			}
			
			if($nospace=="yes"){
				$nvr_pcclass .= ' nospace';
			}
			
			$nvr_output  ='<div class="pcarousel '.esc_attr( $nvr_pcclass ).'">';
				$nvr_output .= $nvr_outputtitle;
				
			$i=1;
			$nvr_argquery = array(
				'post_type' => $this->novaro_posttype(),
				'showposts' => $showposts
			);
			if($cat){
				$nvr_argquery['tax_query'] = array(
					array(
						'taxonomy' => $this->novaro_taxonomy(),
						'field' => 'slug',
						'terms' => $cat
					)
				);
			}
			
			$nvr_pfqry = new WP_Query($nvr_argquery);
			$nvr_pcclass = '';
			if($full=="yes"){
				$nvr_pcclass .= ' nvr-fullwidthwrap';
			}
			$nvr_output  .='<div class="row">';
				$nvr_output  .='<div class="flexslider-carousel '. esc_attr( $nvr_pcclass ).'">';
					$nvr_output  .='<ul class="slides">';
					
					$nvr_havepost = false;
					if($nvr_pfqry->have_posts()){
						while ($nvr_pfqry->have_posts()) : $nvr_pfqry->the_post();
							$nvr_havepost = true;
							$imgsize	= 'portfolio-image-square';
							$pimgsize 	='square';
							$classpf	= 'imgsize-'.$pimgsize.' ';
							$nvr_output .= $this->novaro_pf_get_box( $imgsize, get_the_ID(), $classpf );
						
							$i++; $addclass=""; 
						
						endwhile; 
					}
					wp_reset_postdata();
					 
					$nvr_output .='</ul>';
				$nvr_output  .='</div>';
			 $nvr_output .='</div>';
			 
			 $nvr_output .='</div>';
			 if($nvr_havepost){
			 	return do_shortcode($nvr_output);
			}else{
				return false;
			}
	}
	
	/* Make a Portfolio Post Type */
	function novaro_pf_post_type() {
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		$postslug = $this->novaro_postslug();
		$taxonomslug = $this->novaro_taxonomslug();
		
		register_post_type( $posttype,
					array( 
					'label' => __('Portfolio', $this->novaro_lang() ), 
					'public' => true, 
					'show_ui' => true,
					'show_in_nav_menus' => true,
					'rewrite' => array( 'slug' => $postslug, 'with_front' => false ),
					'hierarchical' => true,
					'menu_position' => 5,
					'has_archive' => true,
					'supports' => array(
										 'title',
										 'editor',
										 'thumbnail',
										 'excerpt',
										 'custom-fields',
										 'revisions')
						) 
					);
		register_taxonomy($taxonom, $posttype, array(
			'hierarchical' => true,
			'label' =>  __('Portfolio Categories', $this->novaro_lang()),
			'query_var' => true,
			'rewrite' => array( 'slug' => $taxonomslug, 'with_front' => false ),
			'show_ui' => true,
			'singular_name' => 'Category'
			));
	}
	
	function novaro_pf_add_list_columns($portfolio_columns){
		
		$thetaxonomy = $this->novaro_taxonomy();
		$new_columns = array();
		$new_columns['cb'] = '<input type="checkbox" />';
		
		$new_columns['title'] = __('Portfolio Title', $this->novaro_lang());
		$new_columns['images'] = __('Images', $this->novaro_lang());
		$new_columns['author'] = __('Author', $this->novaro_lang());
		
		$new_columns[$thetaxonomy] = __('Categories', $this->novaro_lang());
		
		$new_columns['date'] = __('Date', $this->novaro_lang());
		
		return $new_columns;
	}
	
	function novaro_pf_manage_column($column_name){
		global $post;
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		
		$id = $post->ID;
		$title = $post->post_title;
		switch($column_name){
			case 'images':
				$thumbnailid = get_post_thumbnail_id($id);
				$imagesrc = wp_get_attachment_image_src($thumbnailid, 'thumbnail');
				if($imagesrc){
					echo '<img src="'.$imagesrc[0].'" width="50" alt="'.$title.'" />';
				}else{
					_e('No Featured Image', $this->novaro_lang());
				}
				break;
			
			case $taxonom:
				$postterms = get_the_terms($id, $taxonom);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonom.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$termtext = implode(", ",$termlists);
						echo $termtext;
					}
				}
				
				break;
		}
	}
	
	/* Filter Custom Post Type Categories */
	function novaro_pf_add_taxonomy_filter() {
		global $typenow;
		$posttype = $this->novaro_posttype();
		$taxonomy = $this->novaro_taxonomy();
		if( $typenow==$posttype){
			$filters = array($taxonomy);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo '<select name="'. esc_attr( $tax_slug ).'" id="'. esc_attr( $tax_slug ) .'" class="postform">';
				echo '<option value="">'.__('View All',$this->novaro_lang()).' '. $tax_name .'</option>';
				foreach ($terms as $term) { 
					$selectedstr = '';
					if(isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug){
						$selectedstr = ' selected="selected"';
					}
					echo '<option value='. $term->slug. $selectedstr . '>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
	
	function novaro_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages
		
		$version = $this->novaro_plugin_version();
		
		wp_register_script('nvr_isotope', plugin_dir_url( __FILE__ ).'js/jquery.isotope.min.js', array('jquery'), '1.0', true);
		wp_enqueue_script('nvr_isotope');
		
		wp_register_script('nvr_prettyPhoto', plugin_dir_url( __FILE__ ).'js/jquery.prettyPhoto.js', array('jquery'), '3.0', true);
		wp_enqueue_script('nvr_prettyPhoto');
		
		wp_register_script('nvr_infinite-scroll', plugin_dir_url( __FILE__ ).'js/jquery.infinitescroll.js', array('jquery'), '2.0b2', true);
		wp_enqueue_script('nvr_infinite-scroll');
		
		wp_register_script('nvr_ImagesLoaded', plugin_dir_url( __FILE__ ).'js/imagesloaded.pkgd.min.js', array('jquery'), '3.0.4', true);
		wp_enqueue_script('nvr_ImagesLoaded');
		
		wp_register_script('nvr_flexslider', plugin_dir_url( __FILE__ ).'js/jquery.flexslider-min.js', array('jquery'), '1.8', true);
		wp_enqueue_script('nvr_flexslider');
		
		wp_register_script('nvr_customPortfolio', plugin_dir_url( __FILE__ ).'js/nvrportfolio.js', array('jquery'), '1.8', true);
		wp_enqueue_script('nvr_customPortfolio');
		
		$nvr_localvar = array( 
			'pluginurl'					=> plugin_dir_url( __FILE__ ),
			'pfloadmore'				=> __('Loading More Portfolio', $this->novaro_lang() ),
			'loadfinish'				=> __('All Items are Loaded', $this->novaro_lang() )
		);
		wp_localize_script( 'nvr_customPortfolio', 'nvrpflocal_var', $nvr_localvar );
		
		//Register and use this plugin main CSS
		wp_register_style('nvr_skeleton-css', plugin_dir_url( __FILE__ ).'css/1140.css', 'nvr_normalize-css', '', 'screen, all');
		wp_enqueue_style('nvr_skeleton-css');
		
		wp_register_style('nvr_font-awesome-css', plugin_dir_url( __FILE__ ).'css/font-awesome.min.css', 'nvr_normalize-css', '', 'screen, all');
		wp_enqueue_style('nvr_font-awesome-css');
		
		wp_register_style('nvr_prettyPhoto-css', plugin_dir_url( __FILE__ ).'css/prettyPhoto.css', '', '', 'screen, all');
		wp_enqueue_style('nvr_prettyPhoto-css');
		
		wp_register_style('nvr_flexslider-css', plugin_dir_url( __FILE__ ).'css/flexslider.css', '', '', 'screen, all');
		wp_enqueue_style('nvr_flexslider-css');
		
		wp_register_style('nvr_custom-portfolio-css', plugin_dir_url( __FILE__ ).'css/nvrportfolio.css', '', '', 'screen, all');
		wp_enqueue_style('nvr_custom-portfolio-css');
	}
	
	// The excerpt based on character
	function novaro_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}
	
}

$theportfolio = new Novaro_Portfolio();
?>