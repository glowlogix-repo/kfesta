<?php
	/* -----------------------------------------------------------------
		Social Network
	----------------------------------------------------------------- */
	function nvr_socialnetwork($atts, $content) {
		extract(shortcode_atts(array(
					"usetext" => '',
					"vertical" => '',
		), $atts));
		
		if(!function_exists('nvr_socialicon')){return false;}
		
		if($usetext=='yes'){
			$usetext = true;
		}else{
			$usetext = false;
		}
		
		if($vertical=='yes'){
			$vertical = true;
		}else{
			$vertical = false;
		}
		
		return nvr_socialicon($usetext, $vertical);
	}

?>