<?php
/*
Plugin Name: Novaro Essential Shortcodes
Plugin URI: http://www.novarostudio.com/
Description: Novaro Essential Shortcodes is a wordpress plugin that contain all of novarostudio theme shortcodes.
Version: 1.0.2
Author: novarostudio
Author URI: http://www.novarostudio.com
License: GPL
*/

/*  Copyright 2015  Novaro Studio

    Novaro Testimonial is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);

class Novaro_Shortcode{
	
	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;
	
	function __construct(){
		
		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');
		add_action('init', array($this, 'novaro_pf_action_init'));
		add_action('vc_before_init', array( $this, 'novaro_vc_map'));
		
		$this->novaro_includes();
		
		add_shortcode( 'bannerimg', 'nvr_bannerimg' );
		
		add_shortcode( 'bordercustom', 'nvr_borderseparator' );
		
		add_shortcode('row', 'nvr_row');
		add_shortcode('one_half', 'nvr_one_half');
		add_shortcode('one_third', 'nvr_one_third');
		add_shortcode('one_fourth', 'nvr_one_fourth');
		add_shortcode('one_fifth', 'nvr_one_fifth');
		add_shortcode('one_sixth', 'nvr_one_sixth');
		add_shortcode('two_third', 'nvr_two_third');
		add_shortcode('two_fourth', 'nvr_two_fourth');
		add_shortcode('two_fifth', 'nvr_two_fifth');
		add_shortcode('two_sixth', 'nvr_two_sixth');
		add_shortcode('three_fourth', 'nvr_three_fourth');
		add_shortcode('three_fifth', 'nvr_three_fifth');
		add_shortcode('three_sixth', 'nvr_three_sixth');
		add_shortcode('four_fifth', 'nvr_four_fifth');
		add_shortcode('four_sixth', 'nvr_four_sixth');
		add_shortcode('five_sixth', 'nvr_five_sixth');
		
		add_shortcode('content_title', 'nvr_content_title');
		
		add_shortcode( 'counters', 'nvr_counters' );
		
		add_shortcode( 'dropcap', 'nvr_dropcap' );
		
		add_shortcode( 'featuredgallery', 'nvr_featuredgallery' );
		
		add_shortcode( 'featuredslider', 'nvr_featuredslider' );
		
		add_shortcode( 'heading', 'nvr_heading' );
		
		add_shortcode( 'highlight', 'nvr_highlight' );
		
		add_shortcode( 'hostingtable', 'nvr_hostingtable' );
		
		add_shortcode( 'iconcontainer', 'nvr_iconcontainer' );
		
		add_shortcode('loginform','nvr_login_form');
		
		add_shortcode( 'meter', 'nvr_meter' );
		
		add_shortcode('pre', 'nvr_pre');
		add_shortcode('code', 'nvr_code');
		
		add_shortcode( 'product_carousel', 'nvr_productcarousel' );
		add_shortcode( 'product_filter', 'nvr_productfilter' );
		
		add_shortcode( 'pullquote', 'nvr_pullquote' );
		add_shortcode( 'blockquote', 'nvr_blockquote' );
		
		add_shortcode( 'recent_posts', 'nvr_recentposts' );
		
		add_shortcode('registerform','nvr_register_form');
		
		add_shortcode('separator', 'nvr_separator');
		add_shortcode('spacer', 'nvr_spacer');
		add_shortcode('clearfix', 'nvr_clearfixfloat');
		
		add_shortcode( 'sliders', 'nvr_sliders' );
		add_shortcode( 'slide', 'nvr_slide' );
		
		add_shortcode('tabs', 'nvr_tab');
		
		add_shortcode('toggles', 'nvr_toggles');
		add_shortcode('toggle', 'nvr_toggle');
		
		add_shortcode('socialnetwork','nvr_socialnetwork');
		
		add_shortcode("nvr_grid_img","nvr_grid_image");
		add_shortcode("nvr_grid_info","nvr_grid_information");
		add_shortcode("nvr_grid_product","nvr_grid_product");
		
		if ( ! defined( 'WPB_VC_VERSION' ) ) {
			add_filter( 'the_content', 'nvr_pre_shortcode', 7 );
		}
		$this->version		= $this->novaro_plugin_version();
	}
	
	//Get the version of portfolio
	function novaro_plugin_version(){
		$this->version = "1.0";
		
		return $this->version;
	}
	
	function novaro_lang(){
		$thelang = 'novaro';
		return $thelang;
	}
	
	function novaro_shortname(){
		$theshortname = 'novaro';
		return $theshortname;
	}
	
	function novaro_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}
	
	function novaro_pf_md5hash($str = ''){
		return md5($str);
	}
	
	function novaro_includes(){
		
		$pluginpath = plugin_dir_path( __FILE__ );
		
		require_once( $pluginpath.'novaro-functions.php' );
		require_once( $pluginpath.'shortcodes/bannerimg.php' );
		require_once( $pluginpath.'shortcodes/borderseparator.php' );
		require_once( $pluginpath.'shortcodes/columns.php' );
		require_once( $pluginpath.'shortcodes/content-title.php' );
		require_once( $pluginpath.'shortcodes/counters.php' );
		require_once( $pluginpath.'shortcodes/dropcap.php' );
		require_once( $pluginpath.'shortcodes/featuredgallery.php' );
		require_once( $pluginpath.'shortcodes/featuredslider.php' );
		require_once( $pluginpath.'shortcodes/heading.php' );
		require_once( $pluginpath.'shortcodes/highlight.php' );
		require_once( $pluginpath.'shortcodes/hostingtable.php' );
		require_once( $pluginpath.'shortcodes/iconcontainer.php' );
		require_once( $pluginpath.'shortcodes/loginform.php' );
		require_once( $pluginpath.'shortcodes/meter.php' );
		require_once( $pluginpath.'shortcodes/pre.php' );
		require_once( $pluginpath.'shortcodes/products.php' );
		require_once( $pluginpath.'shortcodes/quote.php' );
		require_once( $pluginpath.'shortcodes/recentposts.php' );
		require_once( $pluginpath.'shortcodes/registerform.php' );
		require_once( $pluginpath.'shortcodes/separator.php' );
		require_once( $pluginpath.'shortcodes/sliders.php' );
		require_once( $pluginpath.'shortcodes/socialnetwork.php' );
		require_once( $pluginpath.'shortcodes/tabs.php' );
		require_once( $pluginpath.'shortcodes/toggles.php' );
		require_once( $pluginpath.'shortcodes/gridimg.php' );
		require_once( $pluginpath.'shortcodes/gridinfo.php' );
		require_once( $pluginpath.'shortcodes/gridproduct.php' );
	}
	
	function novaro_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages
		
		$version = $this->novaro_plugin_version();
		if(!is_admin()){
			wp_register_script('nvr_appear', plugin_dir_url( __FILE__ ) .'js/appear.js', array('jquery'), '1.0', true);
			wp_enqueue_script('nvr_appear');
			
			wp_register_script('nvr_isotope', plugin_dir_url( __FILE__ ).'js/jquery.isotope.min.js', array('jquery'), '1.0', true);
			wp_enqueue_script('nvr_isotope');
			
			wp_register_script('nvr_prettyPhoto', plugin_dir_url( __FILE__ ).'js/jquery.prettyPhoto.js', array('jquery'), '3.0', true);
			wp_enqueue_script('nvr_prettyPhoto');
			
			wp_register_script('nvr_infinite-scroll', plugin_dir_url( __FILE__ ).'js/jquery.infinitescroll.js', array('jquery'), '2.0b2', true);
			wp_enqueue_script('nvr_infinite-scroll');
			
			wp_register_script('nvr_ImagesLoaded', plugin_dir_url( __FILE__ ).'js/imagesloaded.pkgd.min.js', array('jquery'), '3.0.4', true);
			wp_enqueue_script('nvr_ImagesLoaded');
			
			wp_register_script('nvr_PerfectScrollbar', plugin_dir_url( __FILE__ ) .'js/perfect-scrollbar.with-mousewheel.min.js', array('jquery'), '0.4.9', true);
			wp_enqueue_script('nvr_PerfectScrollbar');
			
			wp_register_script('nvr_flexslider', plugin_dir_url( __FILE__ ).'js/jquery.flexslider-min.js', array('jquery'), '1.8', true);
			wp_enqueue_script('nvr_flexslider');
			
			wp_register_script('nvr_CountTo', plugin_dir_url( __FILE__ ) .'js/jquery.countTo.js', array('jquery'), '1.0', true);
			wp_enqueue_script('nvr_CountTo');
			
			wp_register_script('nvr_customshortcodes', plugin_dir_url( __FILE__ ).'js/nvrshortcodes.js', array('jquery'), '1.8', true);
			wp_enqueue_script('nvr_customshortcodes');
			
			
			$nvr_localvar = array( 
				'pluginurl'					=> plugin_dir_url( __FILE__ )
			);
			wp_localize_script( 'nvr_customshortcodes', 'nvrshortcodeslocal_var', $nvr_localvar );
			
			//Register and use this plugin main CSS
			wp_register_style('nvr_skeleton-css', plugin_dir_url( __FILE__ ).'css/1140.css', 'nvr_normalize-css', '', 'screen, all');
			wp_enqueue_style('nvr_skeleton-css');
			
			wp_register_style('nvr_flexslider-css', plugin_dir_url( __FILE__ ).'css/flexslider.css', '', '', 'screen, all');
			wp_enqueue_style('nvr_flexslider-css');
			
			wp_register_style('nvr_prettyPhoto-css', plugin_dir_url( __FILE__ ).'css/prettyPhoto.css', '', '', 'screen, all');
			wp_enqueue_style('nvr_prettyPhoto-css');
			
			wp_register_style('nvr_PerfectScrollbar-css', plugin_dir_url( __FILE__ ) . 'css/perfect-scrollbar.min.css', 'nvr_skeleton-css', '', 'screen, all');
			wp_enqueue_style('nvr_PerfectScrollbar-css');
			
			wp_register_style('nvr_custom-shortcodes-css', plugin_dir_url( __FILE__ ).'css/nvrshortcodes.css', '', '', 'screen, all');
			wp_enqueue_style('nvr_custom-shortcodes-css');
		}
	}
	
	// The excerpt based on character
	function novaro_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}
	
	function novaro_vc_map(){
		vc_map( 
			array(
				"name" => __( "Grid Image Content", 'novarostudio'),
				"base" => "nvr_grid_img",
				"class" => "",
				"category" => __( "Novaro", 'novarostudio'),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Title", 'novarostudio' ),
						"param_name" => "title",
						"value" => "",
						"description" => __( "Title for your grid image.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Subtitle", 'novarostudio' ),
						"param_name" => "subtitle",
						"value" => "",
						"description" => __( "Subtitle for your grid image.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Text on Link", 'novarostudio' ),
						"param_name" => "linktext",
						"value" => "",
						"description" => __( "link text for your grid image.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Link URL", 'novarostudio' ),
						"param_name" => "link",
						"value" => '',
						"description" => __( "link for your grid image.", 'novarostudio' )
					),
					array(
						'type' => 'colorpicker',
						'heading' => __( 'Background Color', 'novarostudio' ),
						'param_name' => 'bgcolor',
						"value" => '',
						"description" => __( "Background color for your grid.", 'novarostudio' )
					),
					array(
						'type' => 'attach_image',
						'heading' => __( 'Background Image', 'novarostudio' ),
						'param_name' => 'bgimage',
						"value" => '',
						"description" => __( "Choose the background image for your grid.", 'novarostudio' )
					)
				)
			)
	  	);
		
		vc_map( 
			array(
				"name" => __( "Grid Info Content", 'novarostudio'),
				"base" => "nvr_grid_info",
				"class" => "",
				"category" => __( "Novaro", 'novarostudio'),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Title", 'novarostudio' ),
						"param_name" => "title",
						"value" => "",
						"description" => __( "Title for your grid image.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Subtitle", 'novarostudio' ),
						"param_name" => "subtitle",
						"value" => "",
						"description" => __( "Subtitle for your grid image.", 'novarostudio' )
					),
					array(
						'type' => 'attach_image',
						'heading' => __( 'Image', 'novarostudio' ),
						'param_name' => 'image',
						"value" => '',
						"description" => __( "Choose the image for your grid.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Link URL", 'novarostudio' ),
						"param_name" => "link",
						"value" => '',
						"description" => __( "Link for your grid image.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Link Text", 'novarostudio' ),
						"param_name" => "linktext",
						"value" => '',
						"description" => __( "Text for your link button on your grid info.", 'novarostudio' )
					),
					array(
						'type' => 'colorpicker',
						'heading' => __( 'Background Color', 'novarostudio' ),
						'param_name' => 'bgcolor',
						"value" => '#fbfbfb',
						"description" => __( "Background color for your grid.", 'novarostudio' )
					)
				)
			)
	  	);
		
		vc_map( 
			array(
				"name" => __( "Grid Product", 'novarostudio'),
				"base" => "nvr_grid_product",
				"class" => "",
				"category" => __( "Novaro", 'novarostudio'),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Product ID or Slug", 'novarostudio' ),
						"param_name" => "productid",
						"value" => "",
						"description" => __( "Input your product id or product slug.", 'novarostudio' )
					)
				)
			)
	  	);
		
		vc_map( 
			array(
				"name" => __( "Banner Image", 'novarostudio'),
				"base" => "bannerimg",
				"class" => "",
				"category" => __( "Novaro", 'novarostudio'),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Image Source URL", 'novarostudio' ),
						"param_name" => "src",
						"value" => "",
						"description" => __( "Input the URL of your image.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Banner Link", 'novarostudio' ),
						"param_name" => "link",
						"value" => "",
						"description" => __( "Input the link of your banner.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Margin", 'novarostudio' ),
						"param_name" => "margin",
						"value" => "",
						"description" => __( "Input the margin of your banner.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Padding", 'novarostudio' ),
						"param_name" => "padding",
						"value" => "",
						"description" => __( "Input the padding of your banner.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Class", 'novarostudio' ),
						"param_name" => "class",
						"value" => "",
						"description" => __( "Input the custom class of your banner.", 'novarostudio' )
					),
					array(
						"type" => "textarea_html",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Content", "novarostudio" ),
						"param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
						"value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "novarostudio" ),
						"description" => __( "Enter your content.", "novarostudio" )
					)
				)
			)
	  	);
		
		vc_map( 
			array(
				"name" => __( "Product Filter", 'novarostudio'),
				"base" => "product_filter",
				"class" => "",
				"category" => __( "Novaro", 'novarostudio'),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Custom ID", 'novarostudio' ),
						"param_name" => "id",
						"value" => "",
						"description" => __( "Input your custom ID. (optional)", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Custom Class", 'novarostudio' ),
						"param_name" => "class",
						"value" => "",
						"description" => __( "Input your custom class. (optional)", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Title", 'novarostudio' ),
						"param_name" => "title",
						"value" => "",
						"description" => __( "Input your title. (optional)", 'novarostudio' )
					),
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Columns", 'novarostudio' ),
						"param_name" => "col",
						"value" => array(
							__('Two Columns', 'novarostudio') => "2" ,
							__('Three Columns', 'novarostudio') => "3",
							__('Four Columns', 'novarostudio') => "4",
						),
						"std" => "4",
						"description" => __( "Select the columns.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Product Category Slug", 'novarostudio' ),
						"param_name" => "cat",
						"value" => "",
						"description" => __( "Input the product category slugs. Please use comma-separated to use more than one category slug", 'novarostudio' )
					),
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Type", 'novarostudio' ),
						"param_name" => "type",
						"value" => array(
							__('Featured', 'novarostudio') => "featured",
							__('Best Selling', 'novarostudio') => "best-selling",
							__('Top Rated', 'novarostudio') => "top-rated",
							__('Latest', 'novarostudio') => "latest",
							__('On Sale', 'novarostudio') => "onsale"
						),
						"description" => __( "Select the type of query.", 'novarostudio' )
					),
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Remove Filter?", 'novarostudio' ),
						"param_name" => "nofilter",
						"value" => array(
							__('No', 'novarostudio') => "no",
							__('Yes', 'novarostudio') => "yes"
						),
						"description" => __( "", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Show Posts", 'novarostudio' ),
						"param_name" => "showposts",
						"value" => '',
						"description" => __( "Input the number of product that you want to display.", 'novarostudio' )
					)
				)
			)
	  	);
		
		vc_map( 
			array(
				"name" => __( "Product Carousel", 'novarostudio'),
				"base" => "product_carousel",
				"class" => "",
				"category" => __( "Novaro", 'novarostudio'),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Custom Class", 'novarostudio' ),
						"param_name" => "class",
						"value" => "",
						"description" => __( "Input your custom class. (optional)", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Title", 'novarostudio' ),
						"param_name" => "title",
						"value" => "",
						"description" => __( "Input your title. (optional)", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Product Category Slug", 'novarostudio' ),
						"param_name" => "cat",
						"value" => "",
						"description" => __( "Input the product category slugs. Please use comma-separated to use more than one category slug", 'novarostudio' )
					),
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Type", 'novarostudio' ),
						"param_name" => "type",
						"value" => array(
							__('Featured', 'novarostudio') => "featured",
							__('Best Selling', 'novarostudio') => "best-selling",
							__('Top Rated', 'novarostudio') => "top-rated",
							__('Latest', 'novarostudio') => "latest",
							__('On Sale', 'novarostudio') => "onsale"
						),
						"description" => __( "Select the type of query.", 'novarostudio' )
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Show Posts", 'novarostudio' ),
						"param_name" => "showposts",
						"value" => '-1',
						"description" => __( "Input the number of product that you want to display.", 'novarostudio' )
					)
				)
			)
	  	);
	}
	
}

$theshortcode = new Novaro_Shortcode();
?>