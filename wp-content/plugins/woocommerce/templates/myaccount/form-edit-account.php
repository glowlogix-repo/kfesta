<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_edit_account_form' ); ?>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	<?php
	$current_user = wp_get_current_user();
	$radius =get_user_meta($user->ID, 'vendor_radius', true);
	$days=   get_user_meta($user->ID, 'vendor_working_days', true);
	?>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
	</p>


	<fieldset>
		<legend><?php _e( 'Password change', 'woocommerce' ); ?></legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php _e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php _e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php _e( 'Confirm new password', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" />
		</p>
	</fieldset>
	<div class="clear"></div>
	<?php if($current_user->roles[0]!='customer'){ ?>
	<div class="wcmp-regi-12">
    <label>Select Working Days</label>

    <div class="weekDays-selector">

		  <input type="checkbox" id="weekday-mon" class="weekday" value="1" <?php if(in_array(1, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-mon">Mon</label>
		  <input type="checkbox" id="weekday-tue" class="weekday" value="2" <?php if(in_array(2, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-tue">Tue</label>
		  <input type="checkbox" id="weekday-wed" class="weekday" value="3"<?php if(in_array(3, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-wed">Wed</label>
		  <input type="checkbox" id="weekday-thu" class="weekday" value="4"<?php if(in_array(4, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-thu">Thu</label>
		  <input type="checkbox" id="weekday-fri" class="weekday" value="5" <?php if(in_array(5, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-fri">Fri</label>
		  <input type="checkbox" id="weekday-sat" class="weekday" value="6" <?php if(in_array(6, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-sat">Sat</label>
		  <input type="checkbox" id="weekday-sun" class="weekday" value="7" <?php if(in_array(7, $days)) echo( 'checked'); ?> name="days[]"/>
		  <label for="weekday-sun">Sun</label>
    </div>
</div>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
	    <label>Radius</label>
	    <input type="text" class="woocommerce-Input woocommerce-Input--password input-text" required
	    name="radius" placeholder="Enter Radius in Kilometers" value="<?php echo $radius;?>">
	</p>
	<?php } ?>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<input type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
