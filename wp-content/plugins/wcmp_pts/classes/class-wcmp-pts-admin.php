<?php

/**
 * WCMp Product Types plugin core
 *
 * WCMp Product Types Admin
 *
 * @author 		WC Marketplace
 * @package 	wcmp-pts/classes
 * @version   1.0.0
 */
 
class WCMp_Product_Types_Admin {
	
	public function __construct() {
		add_filter( "settings_capabilities_product_tab_options", array( &$this, 'wcmp_pts_capabilities_options' ) );
		add_filter( "settings_capabilities_product_tab_new_input", array( &$this, 'wcmp_pts_capabilities_options_save' ), 10, 2 );
		
		
		add_filter( "settings_vendor_general_tab_options", array( &$this,"add_wcmp_pts_endpoint_option" ) );
		add_filter( "settings_vendor_general_tab_new_input", array( &$this, "wcmp_pts_endpoint_options_save" ), 10, 2 );
		
	}
	
	/**
	 * WCMp Product Types Capailities
	 */
	function wcmp_pts_capabilities_options( $settings_tab_options ) {
		global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
		
		// Booking
		if( wcmp_pts_is_booking() ) {
                        
			$settings_tab_options['sections']['products_capability']['fields']['manage_bookings'] = array('title' => __('Manage Bookings', 'wcmp_pts'), 'type' => 'checkbox', 'id' => 'manage_bookings', 'label_for' => 'manage_bookings', 'name' => 'manage_bookings', 'desc' => __('Allow vendors to manager Bookable Product.', 'wcmp_pts'), 'value' => 'Enable');
		}
		
		// Subsctiptions
		if( wcmp_pts_is_subscription() ) {
			$settings_tab_options['sections']['products_capability']['fields']['manage_subscriptions'] = array('title' => __('Manage Subscriptions', 'wcmp_pts'), 'type' => 'checkbox', 'id' => 'manage_subscriptions', 'label_for' => 'manage_subscriptions', 'name' => 'manage_subscriptions', 'desc' => __('Allow vendors to manager Subscription Product.', 'wcmp_pts'), 'value' => 'Enable');
		}
		
		// Auction
		if( wcmp_pts_is_wcsauction() || wcmp_pts_is_yithauction() ) {
			$settings_tab_options['sections']['products_capability']['fields']['manage_auctions'] = array('title' => __('Manage Auctions', 'wcmp_pts'), 'type' => 'checkbox', 'id' => 'manage_auctions', 'label_for' => 'manage_auctions', 'name' => 'manage_auctions', 'desc' => __('Allow vendors to manager Auction Product.', 'wcmp_pts'), 'value' => 'Enable');
		}
		
		return $settings_tab_options;
	}
	
	/**
	 * WCMp Product Types Capailities Save
	 */
	function wcmp_pts_capabilities_options_save( $new_input, $input ) {
		global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
		
		$vendor_role = get_role( 'dc_vendor' );
		if ( isset($input['manage_bookings']) ) {
			$new_input['manage_bookings'] = sanitize_text_field($input['manage_bookings']);
			$vendor_role->add_cap( 'manage_bookings' );
		} else {
			$vendor_role->remove_cap( 'manage_bookings' );
		}
		
		if ( isset($input['manage_subscriptions']) ) {
			$new_input['manage_subscriptions'] = sanitize_text_field($input['manage_subscriptions']);
			$vendor_role->add_cap( 'manage_subscriptions' );
		} else {
			$vendor_role->remove_cap( 'manage_subscriptions' );
		}
		
		if ( isset($input['manage_auctions']) ) {
			$new_input['manage_auctions'] = sanitize_text_field($input['manage_auctions']);
			$vendor_role->add_cap( 'manage_auctions' );
		} else {
			$vendor_role->remove_cap( 'manage_auctions' );
		}
		
		return $new_input;
	}
	
	public function add_wcmp_pts_endpoint_option( $tab_options ) {
		global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
		
		if( wcmp_pts_is_booking() ) {
			// Vendor Dashbard Bookings
			$tab_options['sections']['wcmp_vendor_general_settings_endpoint_ssection']['fields']['wcb_wcmp_pts_bookings_endpoint'] = array('title' => __('Bookings', 'wcmp_pts'), 'type' => 'text', 'id' => 'wcb_wcmp_pts_bookings_endpoint', 'label_for' => 'wcb_wcmp_pts_bookings_endpoint', 'name' => 'wcb_wcmp_pts_bookings_endpoint', 'hints' => __('Set Endpoint for Vendor Dashboard Bookings page', 'wcmp_pts'), 'placeholder' => 'bookings');
			
			// Vendor Dashbard Booking Details
			$tab_options['sections']['wcmp_vendor_general_settings_endpoint_ssection']['fields']['wcb_wcmp_pts_booking_details_endpoint'] = array('title' => __('Booking Details', 'wcmp_pts'), 'type' => 'text', 'id' => 'wcb_wcmp_pts_booking_details_endpoint', 'label_for' => 'wcb_wcmp_pts_booking_details_endpoint', 'name' => 'wcb_wcmp_pts_booking_details_endpoint', 'hints' => __('Set Endpoint for Vendor Dashboard Booking details page', 'wcmp_pts'), 'placeholder' => 'booking-details');
		}
		
		if( wcmp_pts_is_wcsauction() || wcmp_pts_is_yithauction() ) {
			// Vendor Dashbard Auctions
			$tab_options['sections']['wcmp_vendor_general_settings_endpoint_ssection']['fields']['wcmp_pts_auctions_endpoint'] = array('title' => __('Auctions', 'wcmp_pts'), 'type' => 'text', 'id' => 'wcmp_pts_auctions_endpoint', 'label_for' => 'wcmp_pts_auctions_endpoint', 'name' => 'wcmp_pts_auctions_endpoint', 'hints' => __('Set Endpoint for Vendor Dashboard Auctions page', 'wcmp_pts'), 'placeholder' => 'auctions');
		}
		
		return $tab_options;
	}
	
	public function wcmp_pts_endpoint_options_save( $new_input, $input ) {
		global $WCMp, $WCMp_Frontend_Product_Manager;
		
		if( wcmp_pts_is_booking() ) {
			// Bookings
			if( isset( $input['wcb_wcmp_pts_bookings_endpoint'] ) ) {
				$new_input['wcb_wcmp_pts_bookings_endpoint'] = $input['wcb_wcmp_pts_bookings_endpoint'];
			}
			
			// Booking Details
			if( isset( $input['wcb_wcmp_pts_booking_details_endpoint'] ) ) {
				$new_input['wcb_wcmp_pts_booking_details_endpoint'] = $input['wcb_wcmp_pts_booking_details_endpoint'];
			}
		}
		
		if( wcmp_pts_is_wcsauction() || wcmp_pts_is_yithauction() ) {
			$new_input['wcmp_pts_auctions_endpoint'] = $input['wcmp_pts_auctions_endpoint'];
		}
		
		return $new_input;
	}
}