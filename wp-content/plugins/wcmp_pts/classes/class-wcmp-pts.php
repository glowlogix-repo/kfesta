<?php

class WCMp_Product_Types {

    public $plugin_url;
    public $plugin_path;
    public $version;
    public $token;
    public $text_domain;
    public $library;
    public $shortcode;
    public $admin;
    public $frontend;
    public $template;
    public $ajax;
    private $file;
    public $settings;
    public $license;
    public $wcmp_wp_fields;
    public $wcmp_pts_wcbooking;
    public $wcmp_pts_wcsubscriptions;
    public $wcmp_pts_wcrental;
    public $wcmp_pts_yithauction;
    public $wcmp_pts_wcsauction;
    public $wcmp_pts_wcaddons;

    public function __construct($file) {

        $this->file = $file;
        $this->plugin_url = trailingslashit(plugins_url('', $plugin = $file));
        $this->plugin_path = trailingslashit(dirname($file));
        $this->token = WCMP_PTS_PLUGIN_TOKEN;
        $this->text_domain = WCMP_PTS_TEXT_DOMAIN;
        $this->version = WCMP_PTS_PLUGIN_VERSION;

        add_action('init', array(&$this, 'init'));
        add_filter('wcmp_endpoints_query_vars', array(&$this,'add_wcmp_endpoints_query_vars'));
    }

    /**
     * initilize plugin on WP init
     */
    function init() {
        global $WCMp, $WCMp_Frontend_Product_Manager;

        // Init Text Domain
        $this->load_plugin_textdomain();
        
        // Check if WC Bookings
        if( wcmp_pts_is_booking() ) {
        	$this->load_class('wcbookings');
        	$this->wcmp_pts_wcbooking = new WCMp_PTS_WCBookings();
        }
        
        // Check if WC Subscriptions
        if( wcmp_pts_is_subscription() ) {
        	$this->load_class('wcsubscriptions');
        	$this->wcmp_pts_wcsubscriptions = new WCMp_PTS_WCSubscriptions();
        }
        
        // Check if WC Rental
        if( wcmp_pts_is_rental() ) {
        	$this->load_class('wcrental');
        	$this->wcmp_pts_wcrental = new WCMp_PTS_WCRental();
        }
        
        // Check if Yith Auction
        if( wcmp_pts_is_yithauction() ) {
        	$this->load_class('yithauction');
        	$this->wcmp_pts_yithauction = new WCMp_PTS_YTH_Auction();
        }
        
        // Check WC Simple Auction
        if( wcmp_pts_is_wcsauction() ) {
        	$this->load_class('wcsauction');
        	$this->wcmp_pts_wcsauction = new WCMp_PTS_WCS_Auction();
        }
        
        // Check WC Product Addons
        if( wcmp_pts_has_wcaddons() ) {
        	$this->load_class('wcaddons');
        	$this->wcmp_pts_wcaddons = new WCMp_PTS_WC_Addons();
        }

        // Init library
        /*$this->load_class('library');
        $this->library = new WCMp_Product_Types_Library();

        // Init ajax
        if (defined('DOING_AJAX')) {
            $this->load_class('ajax');
            $this->ajax = new WCMp_Product_Types_Ajax();
        }*/

        if (is_admin()) {
            $this->load_class('admin');
            $this->admin = new WCMp_Product_Types_Admin();
        }

        if (!is_admin() || defined('DOING_AJAX')) {
            $this->load_class('frontend');
            $this->frontend = new WCMp_Product_Types_Frontend();
        }

        // DC License Activation
        if (is_admin()) {
            $this->load_class('license');
            $this->license = WCMp_Product_Types_LICENSE();
        }
        
        // DC Wp Fields
        $this->wcmp_wp_fields = $WCMp_Frontend_Product_Manager->library->load_fpm_fields();
    }
    
    public function add_wcmp_endpoints_query_vars($endpoints) {
    	global $WCMp;
    	$endpoints['bookings'] = array(
    			'label' => __('Bookings', $WCMp->text_domain),
    			'endpoint' => get_wcmp_vendor_settings( 'wcmp_vendor_report_endpoint', 'vendor', 'general', 'bookings' )
    	);
    	$endpoints['booking-details'] = array(
    			'label' => __('Booking Details', $WCMp->text_domain),
    			'endpoint' => get_wcmp_vendor_settings( 'wcmp_vendor_report_endpoint', 'vendor', 'general', 'booking-details' )
    	);
    	if( !get_option( 'wcb_wcmp_pts_bookings_endpoint_added' ) ) {
    		flush_rewrite_rules();
    		update_option( 'wcb_wcmp_pts_bookings_endpoint_added', true );
    	}
    	
    	$endpoints['auctions'] = array(
    			'label' => __('Auctions', $WCMp->text_domain),
    			'endpoint' => get_wcmp_vendor_settings( 'wcmp_vendor_report_endpoint', 'vendor', 'general', 'auctions' )
    	);
    	if( !get_option( 'wcb_wcmp_pts_auctions_endpoint_added' ) ) {
    		flush_rewrite_rules();
    		update_option( 'wcb_wcmp_pts_auctions_endpoint_added', true );
    	}
    	return $endpoints;
    }

    /**
     * Load Localisation files.
     *
     * Note: the first-loaded translation file overrides any following ones if the same translation is present
     *
     * @access public
     * @return void
     */
    public function load_plugin_textdomain() {
        $locale = apply_filters('plugin_locale', get_locale(), $this->token);

        load_textdomain($this->text_domain, WP_LANG_DIR . "/wcmp-pts/wcmp-pts-$locale.mo");
        load_textdomain($this->text_domain, $this->plugin_path . "/languages/wcmp-pts-$locale.mo");
    }

    public function load_class($class_name = '') {
        if ('' != $class_name && '' != $this->token) {
            require_once ('class-' . esc_attr($this->token) . '-' . esc_attr($class_name) . '.php');
        } // End If Statement
    }

// End load_class()

    /**
     * Install upon activation.
     *
     * @access public
     * @return void
     */
    static function activate_wcmp_product_types() {
        global $WCMp, $WCMp_Product_Types;

        // License Activation
        $WCMp_Product_Types->load_class('license');
        WCMp_Product_Types_LICENSE()->activation();

        update_option('wcmp_pts_installed', 1);
    }

    /**
     * UnInstall upon deactivation.
     *
     * @access public
     * @return void
     */
    static function deactivate_wcmp_product_types() {
        global $WCMp_Product_Types;
        delete_option('wcmp_pts_installed');

        // License Deactivation
        $WCMp_Product_Types->load_class('license');
        WCMp_Product_Types_LICENSE()->uninstall();
    }
    
    /** Cache Helpers ******************************************************** */

    /**
     * Sets a constant preventing some caching plugins from caching a page. Used on dynamic pages
     *
     * @access public
     * @return void
     */
    function nocache() {
        if (!defined('DONOTCACHEPAGE'))
            define("DONOTCACHEPAGE", "true");
        // WP Super Cache constant
    }

}
