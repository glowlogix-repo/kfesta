<?php

/**
 * WCMp Product Types plugin core
 *
 * Booking WC Booking Support
 *
 * @author 		WC Marketplace
 * @package 	wcmp-pts/classes
 * @version   1.0.0
 */
 
class WCMp_PTS_WCBookings {
	
	public function __construct() {
    global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
    
    if( wcmp_pts_is_booking() ) {
    	if ( current_user_can( 'manage_bookings' ) ) {
				// Bookable Product Type
				add_filter( 'wcmp_product_types', array( &$this, 'wcb_product_types' ), 20 );
				
				// Bookable Product options
				add_filter( 'wcmp_fpm_fields_general', array( &$this, 'wcb_product_manage_fields_general' ), 10, 2 );
				
				// Booking General Block
				add_action( 'after_wcmp_fpm_general', array( &$this, 'wcb_product_manage_general' ) );
				
				// Booking Product Manage View
				add_action( 'end_wcmp_fpm_products_manage', array( &$this, 'wcb_wcmp_pts_form_load_views' ), 20 );
				
				// Booking Product Meta Data Save
				add_action( 'after_wcmp_fpm_meta_save', array( &$this, 'wcb_wcmp_pts_meta_save' ), 20, 2 );
				
				// Booking Dashboard
				add_filter( 'wcmp_vendor_dashboard_nav', array(&$this, 'wcb_wcmp_pts_bookings_nav'));
				add_action( 'wcmp_vendor_dashboard_header', array(&$this,'wcb_wcmp_pts_bookings_header'));
				add_action( 'wcmp_vendor_dashboard_bookings_endpoint', array(&$this, 'wcb_wcmp_pts_bookings_endpoint'));
				
				// Booking Details
				add_action( 'wcmp_vendor_dashboard_header', array(&$this,'wcb_wcmp_pts_booking_details_header'));
				add_action( 'wcmp_vendor_dashboard_booking-details_endpoint', array(&$this, 'wcb_wcmp_pts_booking_details_endpoint'));
			}
    }
    
    // Add vendor email for confirm booking email
		add_filter( 'woocommerce_email_recipient_new_booking', array( $this, 'wcb_wcmp_pts_filter_booking_emails' ), 20, 2 );

		// Add vendor email for cancelled booking email
		add_filter( 'woocommerce_email_recipient_booking_cancelled', array( $this, 'wcb_wcmp_pts_filter_booking_emails' ), 20, 2 );
  }
  
  /**
   * WC Booking Product Type
   */
  function wcb_product_types( $pro_types ) {
  	global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
  	if ( current_user_can( 'manage_bookings' ) ) {
  		$pro_types['booking'] = __( 'Bookable product', 'wcmp_pts' );
  	}
  	
  	return $pro_types;
  }
  
  /**
	 * WC Booking Product General options
	 */
	function wcb_product_manage_fields_general( $general_fields, $product_id ) {
		global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
		
		// Has Resource
		$has_resources = ( get_post_meta( $product_id, '_wc_booking_has_resources', true) ) ? 'yes' : '';
		
		// Has Persons
		$has_persons = ( get_post_meta( $product_id, '_wc_booking_has_persons', true) ) ? 'yes' : '';
		
		$general_fields = array_slice($general_fields, 0, 1, true) +
																	array("_wc_booking_has_resources" => array('label' => __('Has Resouces', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele booking', 'label_class' => 'pro_title pro_ele checkbox_title booking', 'value' => 'yes', 'dfvalue' => $has_resources),
																				"_wc_booking_has_persons" => array('label' => __('Has Persons', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele booking', 'label_class' => 'pro_title pro_ele checkbox_title booking', 'value' => 'yes', 'dfvalue' => $has_persons),
																				) +
																	array_slice($general_fields, 1, count($general_fields) - 1, true) ;
		return $general_fields;
	}
  
  /**
   * WC Booking Product General Options
   */
  function wcb_product_manage_general( $product_id ) {
  	global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
  	
  	$bookable_product = new WC_Product_Booking( $product_id );
  	
  	$duration_type = $bookable_product->get_duration_type( 'edit' );
		$duration      = $bookable_product->get_duration( 'edit' );
		$duration_unit = $bookable_product->get_duration_unit( 'edit' );
		
		$min_duration = $bookable_product->get_min_duration( 'edit' );
		$max_duration = $bookable_product->get_max_duration( 'edit' );
		$enable_range_picker = $bookable_product->get_enable_range_picker( 'edit' ) ? 'yes' : 'no';
		
		$calendar_display_mode = $bookable_product->get_calendar_display_mode( 'edit' );
		$requires_confirmation = $bookable_product->get_requires_confirmation( 'edit' ) ? 'yes' : 'no';
		
		$user_can_cancel = $bookable_product->get_user_can_cancel( 'edit' ) ? 'yes' : 'no';
		$cancel_limit = $bookable_product->get_cancel_limit( 'edit' );
		$cancel_limit_unit = $bookable_product->get_cancel_limit_unit( 'edit' );
  	?>
  	<!-- collapsible Booking 1 -->
	  <h3 class="pro_ele_head booking"><?php _e('Booking Options', $WCMp_Frontend_Product_Manager->text_domain); ?></h3>
		<div class="pro_ele_block booking">
			<p>
			  <?php
					$WCMp_Product_Types->wcmp_wp_fields->dc_generate_form_field( array(  
						
						"_wc_booking_duration_type" => array('label' => __('Booking Duration', 'wcmp_pts') , 'type' => 'select', 'options' => array( 'fixed' => __( 'Fixed blocks of', 'wcmp_pts'), 'customer' => __( 'Customer defined blocks of', 'wcmp_pts' ) ), 'class' => 'regular-select pro_ele booking', 'label_class' => 'pro_title booking', 'value' => $duration_type ),
						"_wc_booking_duration" => array('type' => 'number', 'class' => 'regular-text pro_ele booking', 'label_class' => 'pro_title booking', 'value' => $duration ),
						"_wc_booking_duration_unit" => array('type' => 'select', 'options' => array( 'month' => __( 'Month(s)', 'wcmp_pts'), 'day' => __( 'Day(s)', 'wcmp_pts' ), 'hour' => __( 'Hour(s)', 'wcmp_pts' ), 'minute' => __( 'Minute(s)', 'wcmp_pts' ) ), 'class' => 'regular-select pro_ele booking', 'label_class' => 'pro_title booking', 'value' => $duration_unit ),
						"_wc_booking_min_duration" => array('label' => __('Minimum duration', 'wcmp_pts') , 'type' => 'number', 'class' => 'regular-text pro_ele duration_type_customer_ele booking', 'label_class' => 'pro_title duration_type_customer_ele booking', 'value' => $min_duration, 'hints' => __( 'The minimum allowed duration the user can input.', 'wcmp_pts' ), 'attributes' => array( 'min' => '', 'step' => '1' ) ),
						"_wc_booking_max_duration" => array('label' => __('Maximum duration', 'wcmp_pts') , 'type' => 'number', 'class' => 'regular-text pro_ele duration_type_customer_ele booking', 'label_class' => 'pro_title duration_type_customer_ele booking', 'value' => $max_duration, 'hints' => __( 'The maximum allowed duration the user can input.', 'wcmp_pts' ), 'attributes' => array( 'min' => '', 'step' => '1' ) ),
						"_wc_booking_enable_range_picker" => array('label' => __('Enable Calendar Range Picker?', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele duration_type_customer_ele booking', 'label_class' => 'pro_title duration_type_customer_ele booking', 'value' => 'yes', 'dfvalue' => $enable_range_picker, 'hints' => __( 'Lets the user select a start and end date on the calendar - duration will be calculated automatically.', 'wcmp_pts' ) ),
						"_wc_booking_calendar_display_mode" => array('label' => __('Calendar display mode', 'wcmp_pts') , 'type' => 'select', 'options' => array( '' => __( 'Display calendar on click', 'wcmp_pts'), 'always_visible' => __( 'Calendar always visible', 'wcmp_pts' ) ), 'class' => 'regular-select pro_ele booking', 'label_class' => 'pro_title booking', 'value' => $calendar_display_mode ),
						"_wc_booking_requires_confirmation" => array('label' => __('Requires confirmation?', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele booking', 'label_class' => 'pro_title booking', 'value' => 'yes', 'dfvalue' => $requires_confirmation, 'hints' => __( 'Check this box if the booking requires admin approval/confirmation. Payment will not be taken during checkout.', 'wcmp_pts' ) ),
						"_wc_booking_user_can_cancel" => array('label' => __('Can be cancelled?', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele booking', 'label_class' => 'pro_title booking', 'value' => 'yes', 'dfvalue' => $user_can_cancel, 'hints' => __( 'Check this box if the booking can be cancelled by the customer after it has been purchased. A refund will not be sent automatically.', 'wcmp_pts' ) ),
						"_wc_booking_cancel_limit" => array('label' => __('Booking can be cancelled until', 'wcmp_pts') , 'type' => 'number', 'class' => 'regular-text pro_ele can_cancel_ele booking', 'label_class' => 'pro_title can_cancel_ele booking', 'value' => $cancel_limit ),
						"_wc_booking_cancel_limit_unit" => array('type' => 'select', 'options' => array( 'month' => __( 'Month(s)', 'wcmp_pts'), 'day' => __( 'Day(s)', 'wcmp_pts' ), 'hour' => __( 'Hour(s)', 'wcmp_pts' ), 'minute' => __( 'Minute(s)', 'wcmp_pts' ) ), 'class' => 'regular-select pro_ele can_cancel_ele booking', 'label_class' => 'pro_title can_cancel_ele booking', 'desc_class' => 'can_cancel_ele booking', 'value' => $cancel_limit_unit, 'desc' => __( 'before the start date.', 'wcmp_pts' ) )
						
																															) );
			  
			  ?>
		  </p>
		</div>
  	<?php
  }
  
  /**
   * WC Booking load views
   */
  function wcb_wcmp_pts_form_load_views( $product_id ) {
		global $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types;
	  
	 require_once( $WCMp_Product_Types->plugin_path . 'views/wcmp-pts-view-wcbookings.php' );
	}
	
	/**
	 * WC Booking Product Meta data save
	 */
	function wcb_wcmp_pts_meta_save( $new_product_id, $product_manager_form_data ) {
		global $wpdb, $WCMp, $WCMp_Frontend_Product_Manager, $WCMp_Product_Types, $_POST;
		
		$product_type = empty( $product_manager_form_data['product_type'] ) ? WC_Product_Factory::get_product_type( $new_product_id ) : sanitize_title( stripslashes( $product_manager_form_data['product_type'] ) );
		$classname    = WC_Product_Factory::get_product_classname( $new_product_id, $product_type ? $product_type : 'simple' );
		$product      = new $classname( $new_product_id );
		
		// Only set props if the product is a bookable product.
		if ( ! is_a( $product, 'WC_Product_Booking' ) ) {
			return;
		}
		
		// Availability Rules
		$availability_rule_index = 0;
		$availability_rules = array();
		$availability_default_rules = array(  "type"       => 'custom',
																				  "from"       => '',
																				  "to"         => '',
																					"bookable"   => '',
																					"priority"   => 10
																				);
		if( isset($product_manager_form_data['_wc_booking_availability_rules']) && !empty($product_manager_form_data['_wc_booking_availability_rules']) ) {
			foreach( $product_manager_form_data['_wc_booking_availability_rules'] as $availability_rule ) {
				$availability_rules[$availability_rule_index] = $availability_default_rules;
				$availability_rules[$availability_rule_index]['type'] = $availability_rule['type'];
				if( $availability_rule['type'] == 'custom' ) {
					$availability_rules[$availability_rule_index]['from'] = $availability_rule['from_custom'];
					$availability_rules[$availability_rule_index]['to']   = $availability_rule['to_custom'];
				} elseif( $availability_rule['type'] == 'months' ) {
					$availability_rules[$availability_rule_index]['from'] = $availability_rule['from_months'];
					$availability_rules[$availability_rule_index]['to']   = $availability_rule['to_months'];
				} elseif($availability_rule['type'] == 'weeks' ) {
					$availability_rules[$availability_rule_index]['from'] = $availability_rule['from_weeks'];
					$availability_rules[$availability_rule_index]['to']   = $availability_rule['to_weeks'];
				} elseif($availability_rule['type'] == 'days' ) {
					$availability_rules[$availability_rule_index]['from'] = $availability_rule['from_days'];
					$availability_rules[$availability_rule_index]['to']   = $availability_rule['to_days'];
				} elseif($availability_rule['type'] == 'time:range' ) {
					$availability_rules[$availability_rule_index]['from_date'] = $availability_rule['from_custom'];
					$availability_rules[$availability_rule_index]['to_date']   = $availability_rule['to_custom'];
					$availability_rules[$availability_rule_index]['from'] = $availability_rule['from_time'];
					$availability_rules[$availability_rule_index]['to']   = $availability_rule['to_time'];
				} else {
					$availability_rules[$availability_rule_index]['from'] = $availability_rule['from_time'];
					$availability_rules[$availability_rule_index]['to']   = $availability_rule['to_time'];
				}
				$availability_rules[$availability_rule_index]['bookable'] = $availability_rule['bookable'];
				$availability_rules[$availability_rule_index]['priority'] = $availability_rule['priority'];
				$availability_rule_index++;
			}
		}
		
		// Person Types
		$person_type_index = 0;
		$person_types = array();
		if( isset( $product_manager_form_data['_wc_booking_has_persons'] ) && isset($product_manager_form_data['_wc_booking_person_types']) && !empty($product_manager_form_data['_wc_booking_person_types']) ) {
			foreach( $product_manager_form_data['_wc_booking_person_types'] as $booking_person_types ) {
				$loop    = intval( $person_type_index );
		
				$person_type_id = ( isset( $booking_person_types['person_id'] ) ) ? $booking_person_types['person_id'] : 0;
				if( !$person_type_id ) {
					$person_type = new WC_Product_Booking_Person_Type();
					$person_type->set_parent_id( $product->get_id() );
					$person_type->set_sort_order( $loop );
					$person_type_id = $person_type->save();
				} else {
					$person_type = new WC_Product_Booking_Person_Type( $person_type_id );
				}
				
				$person_type->set_props( array(
					'name'        => wc_clean( stripslashes( $booking_person_types['person_name'] ) ),
					'description' => wc_clean( stripslashes( $booking_person_types['person_description'] ) ),
					'sort_order'  => absint( $person_type_index ),
					'cost'        => wc_clean( $booking_person_types['person_cost'] ),
					'block_cost'  => wc_clean( $booking_person_types['person_block_cost'] ),
					'min'         => wc_clean( $booking_person_types['person_min'] ),
					'max'         => wc_clean( $booking_person_types['person_max'] ),
					'parent_id'   => $product->get_id(),
				) );
				$person_types[] = $person_type;
				$person_type_index++;
			}
		}
		
		// Resources
		$resource_index = 0;
		$resources = array();
		if( isset( $product_manager_form_data['_wc_booking_has_resources'] ) && isset($product_manager_form_data['_wc_booking_resources']) && !empty($product_manager_form_data['_wc_booking_resources']) ) {
			foreach( $product_manager_form_data['_wc_booking_resources'] as $booking_resources ) {
				if( $booking_resources[ 'resource_title' ] ) {
					$loop    = intval( $resource_index );
					$resource_id = ( isset( $booking_resources['resource_id'] ) ) ? absint( $booking_resources['resource_id'] ) : 0;
					// Creating new Resource
					if( !$resource_id ) {
						$nresource = new WC_Product_Booking_Resource();
						$nresource->set_name( $booking_resources[ 'resource_title' ] );
						$resource_id = $nresource->save();
					}
					$resources[ $resource_id ] = array(
						'base_cost'  => wc_clean( $booking_resources[ 'resource_base_cost' ] ),
						'block_cost' => wc_clean( $booking_resources[ 'resource_block_cost' ] ),
					);
					$resource_index++;
				}
			}
		}
		
		// Remove Deleted Person Types
		if(isset($_POST['removed_person_types']) && !empty($_POST['removed_person_types'])) {
			foreach($_POST['removed_person_types'] as $removed_person_type) {
				wp_delete_post($removed_person_type, true);
			}
		}
		
		$errors = $product->set_props( apply_filters( 'wcmp_pts_booking_data_factory', array(
			'apply_adjacent_buffer'      => isset( $product_manager_form_data['_wc_booking_apply_adjacent_buffer'] ),
			'base_cost'                  => wc_clean( $product_manager_form_data['_wc_booking_base_cost'] ),
			'buffer_period'              => wc_clean( $product_manager_form_data['_wc_booking_buffer_period'] ),
			'calendar_display_mode'      => wc_clean( $product_manager_form_data['_wc_booking_calendar_display_mode'] ),
			'cancel_limit_unit'          => wc_clean( $product_manager_form_data['_wc_booking_cancel_limit_unit'] ),
			'cancel_limit'               => wc_clean( $product_manager_form_data['_wc_booking_cancel_limit'] ),
			'cost'                       => wc_clean( $product_manager_form_data['_wc_booking_cost'] ),
			'default_date_availability'  => wc_clean( $product_manager_form_data['_wc_booking_default_date_availability'] ),
			'display_cost'               => wc_clean( $product_manager_form_data['_wc_display_cost'] ),
			'duration_type'              => wc_clean( $product_manager_form_data['_wc_booking_duration_type'] ),
			'duration_unit'              => wc_clean( $product_manager_form_data['_wc_booking_duration_unit'] ),
			'duration'                   => wc_clean( $product_manager_form_data['_wc_booking_duration'] ),
			'enable_range_picker'        => isset( $product_manager_form_data['_wc_booking_enable_range_picker'] ),
			'max_date_unit'              => wc_clean( $product_manager_form_data['_wc_booking_max_date_unit'] ),
			'max_date_value'             => wc_clean( $product_manager_form_data['_wc_booking_max_date'] ),
			'max_duration'               => wc_clean( $product_manager_form_data['_wc_booking_max_duration'] ),
			'min_date_unit'              => wc_clean( $product_manager_form_data['_wc_booking_min_date_unit'] ),
			'min_date_value'             => wc_clean( $product_manager_form_data['_wc_booking_min_date'] ),
			'min_duration'               => wc_clean( $product_manager_form_data['_wc_booking_min_duration'] ),
			'qty'                        => wc_clean( $product_manager_form_data['_wc_booking_qty'] ),
			'requires_confirmation'      => isset( $product_manager_form_data['_wc_booking_requires_confirmation'] ),
			'user_can_cancel'            => isset( $product_manager_form_data['_wc_booking_user_can_cancel'] ),
			'check_start_block_only'     => 'start' === $product_manager_form_data['_wc_booking_check_availability_against'],
			'first_block_time'           => wc_clean( $product_manager_form_data['_wc_booking_first_block_time'] ),
			'availability'               => $availability_rules,
			'has_person_cost_multiplier' => isset( $product_manager_form_data['_wc_booking_person_cost_multiplier'] ),
			'has_person_qty_multiplier'  => isset( $product_manager_form_data['_wc_booking_person_qty_multiplier'] ),
			'has_person_types'           => isset( $product_manager_form_data['_wc_booking_has_person_types'] ),
			'has_persons'                => isset( $product_manager_form_data['_wc_booking_has_persons'] ),
			'has_resources'              => isset( $product_manager_form_data['_wc_booking_has_resources'] ),
			'max_persons'                => wc_clean( $product_manager_form_data['_wc_booking_max_persons_group'] ),
			'min_persons'                => wc_clean( $product_manager_form_data['_wc_booking_min_persons_group'] ),
			'person_types'               => $person_types,
			//'pricing'                    => $this->get_posted_pricing(),
			'resource_label'             => wc_clean( $product_manager_form_data['_wc_booking_resource_label'] ),
			'resource_base_costs'        => wp_list_pluck( $resources, 'base_cost' ),
			'resource_block_costs'       => wp_list_pluck( $resources, 'block_cost' ),
			'resource_ids'               => array_keys( $resources ),
			'resources_assignment'       => wc_clean( $product_manager_form_data['_wc_booking_resources_assignment'] ),
		), $new_product_id, $product, $product_manager_form_data ) );
		
		if ( is_wp_error( $errors ) ) {
			//echo '{"status": false, "message": "' . $errors->get_error_message() . '", "id": "' . $new_product_id . '", "redirect": "' . get_permalink( $new_product_id ) . '"}';
		}
		
		$product->save();
	}
	
	
	public function wcb_wcmp_pts_bookings_nav( $nav ) {
		global $WCMp, $WCMp_Product_Types;
		
		$nav['bookings'] = array(
				'label' => __('Bookings', 'wcmp_pts' )
				, 'url' => wcmp_get_vendor_dashboard_endpoint_url( get_wcmp_vendor_settings( 'wcmp_vendor_report_endpoint', 'vendor', 'general', 'bookings' ) )
				, 'capability' => apply_filters( 'wcmp_vendor_dashboard_menu_bookings_capability', true )
				, 'position' => 21
				, 'submenu' => array()
				, 'link_target' => '_self'
				, 'nav_icon' => 'dashicons-calendar-alt'
		);
		return $nav;
	}
	
	public function wcb_wcmp_pts_bookings_header() {
		global $WCMp, $WCMp_Product_Types;
		
		if( $WCMp->endpoints->get_current_endpoint() == 'bookings' ) {
			echo '<ul>';
			echo '<li>' . __( 'Bookings', 'wcmp_pts' ) . '</li>';
			echo '</ul>';
		}
	}
	
	public function wcb_wcmp_pts_bookings_endpoint(){
		global $WCMp, $WCMp_Product_Types, $wpdb;
		
		$vendor_id = get_current_user_id();
		
		$include_bookings = array(0);
		$products = array();
		if ($vendor_id) {
			$vendor = get_wcmp_vendor($vendor_id);
			if ($vendor)
				$vendor_products = $vendor->get_products();
			if (!empty($vendor_products)) {
				foreach ($vendor_products as $vendor_product) {
					$products[] = $vendor_product->ID;
					if( $vendor_product->post_type == 'product_variation' ) $products[] = $vendor_product->post_parent;
				}
			}
		}
		
		if( empty($products) ) $include_bookings =  array(0);
		
  	$query = "SELECT ID FROM {$wpdb->posts} as posts
							INNER JOIN {$wpdb->postmeta} AS postmeta ON posts.ID = postmeta.post_id
							WHERE 1=1
							AND posts.post_type IN ( 'wc_booking' )
							AND postmeta.meta_key = '_booking_product_id' AND postmeta.meta_value in (" . implode(',', $products) . ")";
		
		$vendor_bookings = $wpdb->get_results($query);
		if( empty($vendor_bookings) ) $include_bookings =  array(0);
		$vendor_bookings_arr = array();
		foreach( $vendor_bookings as $vendor_booking ) {
			$vendor_bookings_arr[] = $vendor_booking->ID;
		}
		if( !empty($vendor_bookings_arr) ) $include_bookings =  $vendor_bookings_arr;
		else $include_bookings =  array(0);
		
		$args = array(
							'posts_per_page'   => -1,
							'offset'           => 0,
							'category'         => '',
							'category_name'    => '',
							'bookingby'          => 'date',
							'booking'            => 'DESC',
							'include'          => $include_bookings,
							'exclude'          => '',
							'meta_key'         => '',
							'meta_value'       => '',
							'post_type'        => 'wc_booking',
							'post_mime_type'   => '',
							'post_parent'      => '',
							//'author'	   => get_current_user_id(),
							'post_status'      => 'any',
							//'suppress_filters' => true 
						);
		
		$wcmp_pts_bookings_array = get_posts( $args );
		
		$booking_details_url = wcmp_get_vendor_dashboard_endpoint_url( get_wcmp_vendor_settings( 'wcmp_vendor_report_endpoint', 'vendor', 'general', 'booking-details' ) );
		
		if(!empty($wcmp_pts_bookings_array)) {
			echo '<div class="wcmp_tab ui-tabs ui-widget ui-widget-content ui-corner-all"><div class="wcmp_table_holder"><table><tbody><tr><td>' . __('Bookings', 'wcmp_pts') . '</td><td>' . __('Status', 'wcmp_pts') . '</td><td>' . __('Start Date', 'wcmp_pts') . '</td><td>' . __('End Date', 'wcmp_pts') . '</td><td style="text-align: right;">' . __('Action', 'wcmp_pts') . '</td></tr>';
			foreach($wcmp_pts_bookings_array as $wcmp_pts_bookings_single) {
				$the_booking = get_wc_booking( $wcmp_pts_bookings_single->ID );
				
				echo '<tr><td>#<a class="" href="' . $booking_details_url . $wcmp_pts_bookings_single->ID . '"><strong style="color: #fc482f; text-decoration: underline;">' . $wcmp_pts_bookings_single->ID . '</strong></a></td>';
				echo '<td>' . ucfirst(sanitize_title( $the_booking->get_status( ) )) . '</td>';
				echo '<td>' . date( 'Y-m-d H:i A', $the_booking->get_start( 'edit' ) ) . '</td>';
				echo '<td>' . date( 'Y-m-d H:i A', $the_booking->get_end( 'edit' ) ) . '</td>';
				echo '<td><a class="wcmp_ass_btn" href="' . $booking_details_url . $wcmp_pts_bookings_single->ID . '">' . __('Details', 'wcmp_pts') . '</a></td></tr>';
			}
			echo '</tbody></table></div></div>';
		}  else {
			?>
			<div><h4>&nbsp;&nbsp;&nbsp;&nbsp;
			<?php
			_e( "You do not have any Bookings yet!!!", 'wcmp_pts' );
			?>
			</h4></div>
			<?php
		}
	}
	
	public function wcb_wcmp_pts_booking_details_header() {
		global $WCMp, $WCMp_Product_Types;
		
		if( $WCMp->endpoints->get_current_endpoint() == 'booking-details' ) {
			echo '<ul>';
			echo '<li>' . __( 'Booking Details', 'wcmp_pts' ) . '</li>';
			echo '</ul>';
		}
	}
	
	public function wcb_wcmp_pts_booking_details_endpoint() {
		global $wp, $WCMp, $WCMp_Product_Types, $thebooking;           
		
		if ( ! is_object( $thebooking ) ) {
			if( isset( $wp->query_vars['booking-details'] ) && !empty( $wp->query_vars['booking-details'] ) ) {
				$thebooking = get_wc_booking( $wp->query_vars['booking-details'] );
			}
		}
		
		$booking_id = $wp->query_vars['booking-details'];
		$post = get_post($booking_id);
		$booking = new WC_Booking( $post->ID );
		$order             = $booking->get_order();
		$product_id        = $booking->get_product_id( 'edit' );
		$resource_id       = $booking->get_resource_id( 'edit' );
		$customer_id       = $booking->get_customer_id( 'edit' );
		$product           = $booking->get_product( $product_id );
		$resource = new WC_Product_Booking_Resource( $resource_id );
		$customer          = $booking->get_customer();
		$statuses          = array_unique( array_merge( get_wc_booking_statuses( null, true ), get_wc_booking_statuses( 'user', true ), get_wc_booking_statuses( 'cancel', true ) ) );
		
		?>
		
		<div class="wcmp_pts-collapse" id="wcmp_pts_booking_details" style="padding: 20px;">
		
			<div class="wcmp_pts-collapse-content">
				<div class="wcmp_pts-container">
					<div id="bookings_details_general_expander" class="wcmp_pts-content">
			
						<p class="form-field form-field-wide">
							<label for="booking_date"><?php _e( 'Booking Created:', 'wcmp_pts' ) ?></label>
							<?php echo date_i18n( 'Y-m-d', $booking->get_date_created() ); ?> @<?php echo date_i18n( 'H', $booking->get_date_created() ); ?>:<?php echo date_i18n( 'i A', $booking->get_date_created() ); ?>
						</p>
						
						<p class="form-field form-field-wide">
							<label for="booking_date"><?php _e( 'Order Number:', 'wcmp_pts' ) ?></label>
							<?php
							if ( $order ) {
								echo '#' . $order->get_order_number() . ' - ' . esc_html( wc_get_order_status_name( $order->get_status() ) );
							} else {
								echo '-';
							}
							?>
						</p>
						
						<p class="form-field form-field-wide">
							<label for="wcmp_pts_booking_status"><?php _e( 'Booking Status:', 'woocommerce-bookings' ); ?></label>
							<?php echo ucfirst($post->post_status); ?>
						</p>
					</div>
				</div>
				<div class="wcmp_pts_clearfix"></div>
				<br />
				<!-- collapsible End -->
				
				<!-- collapsible -->
				<div class="page_collapsible bookings_details_booking" id="wcmp_pts_booking_options">
					<strong style="color: #fc482f; text-decoration: underline;"><?php _e('Booking', 'wcmp_pts'); ?></strong>
				</div>
				<div class="wcmp_pts-container">
					<div id="bookings_details_booking_expander" class="wcmp_pts-content">
						
						<p class="form-field form-field-wide">
							<label for="booked_product"><?php _e( 'Booked Product:', 'woocommerce-bookings' ) ?></label>
							<?php
							
							if ( $product ) {
								$product_post = get_post($product->get_ID());
								echo $product_post->post_title;
							} else {
								echo '-';
							}
							?>
						</p>
						
						<?php if( $resource_id ) { ?>
							<p class="form-field form-field-wide">
								<label for="booked_product"><?php _e( 'Resource:', 'woocommerce-bookings' ) ?></label>
								<?php
									echo esc_html( $resource->post_title );
								?>
							</p>
						<?php } ?>
						
						<?php
						if ( $product && is_callable( array( $product, 'get_person_types' ) ) ) {
							$person_types  = $product->get_person_types();
							$person_counts = $booking->get_person_counts();
							if ( ! empty( $person_types ) && is_array( $person_types ) ) {
						?>
								<p class="form-field form-field-wide">
									<label for="booked_product"><?php _e( 'Person(s):', 'woocommerce-bookings' ) ?></label>
									<?php 
									$pfirst = true;
									foreach ( $person_types as $person_type ) {
										if( !$pfirst ) echo ', ';
										echo $pfirst = false;
										echo $person_type->get_name() . ' (';
										if( isset( $person_counts[ $person_type->get_id() ] ) ) { echo $person_counts[ $person_type->get_id() ]; } else { echo '0'; }
										echo ')';
									} 
									?>
								</p>
						<?php }
						}
						?>
						
						<p class="form-field form-field-wide">
							<label for="booking_date"><?php _e( 'Booking Start Date:', 'woocommerce-bookings' ) ?></label>
							<?php echo date( 'Y-m-d H:i A', $booking->get_start( 'edit' ) ); ?>
						</p>
						
						<p class="form-field form-field-wide">
							<label for="booking_date"><?php _e( 'Booking End Date:', 'woocommerce-bookings' ) ?></label>
							<?php echo date( 'Y-m-d H:i A', $booking->get_end( 'edit' ) ); ?>
						</p>
						<p class="form-field form-field-wide">
							<label for="booking_date"><?php _e( 'All day booking:', 'woocommerce-bookings' ) ?></label>
							<?php echo $booking->get_all_day( 'edit' ) ? 'YES' : 'NO'; ?>
						</p>
				 </div>
				</div>
				<div class="wcmp_pts_clearfix"></div>
				<br />
				
				<div class="page_collapsible bookings_details_customer" id="wcmp_pts_customer_options">
					<strong style="color: #fc482f; text-decoration: underline;"><?php _e('Customer', 'woocommerce-bookings'); ?></strong>
				</div>
				<div class="wcmp_pts-container">
					<div id="bookings_details_customer_expander" class="wcmp_pts-content">
						<?php
						$customer_id = get_post_meta( $post->ID, '_booking_customer_id', true );
						$order_id    = $post->post_parent;
						$has_data    = false;
				
						echo '<table class="booking-customer-details">';
				
						if ( $customer_id && ( $user = get_user_by( 'id', $customer_id ) ) ) {
							echo '<tr>';
								echo '<th>' . __( 'Name:', 'woocommerce-bookings' ) . '</th>';
								echo '<td>';
								if ( $user->last_name && $user->first_name ) {
									echo $user->first_name . ' ' . $user->last_name;
								} else {
									echo '-';
								}
								echo '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<th>' . __( 'User Email:', 'woocommerce-bookings' ) . '</th>';
								echo '<td>';
								echo '<a href="mailto:' . esc_attr( $user->user_email ) . '">' . esc_html( $user->user_email ) . '</a>';
								echo '</td>';
							echo '</tr>';
				
							$has_data = true;
						}
				
						if ( $order_id && ( $order = wc_get_order( $order_id ) ) ) {
							echo '<tr>';
								echo '<th>' . __( 'Address:', 'woocommerce-bookings' ) . '</th>';
								echo '<td>';
								if ( $order->get_formatted_billing_address() ) {
									echo wp_kses( $order->get_formatted_billing_address(), array( 'br' => array() ) );
								} else {
									echo __( 'No billing address set.', 'woocommerce-bookings' );
								}
								echo '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<th>' . __( 'Email:', 'wcmp_pts' ) . '</th>';
								echo '<td>';
								echo '<a href="mailto:' . esc_attr( $order->get_billing_email() ) . '">' . esc_html( $order->get_billing_email() ) . '</a>';
								echo '</td>';
							echo '</tr>';
							echo '<tr>';                                    
								echo '<th>' . __( 'Phone:', 'wcmp_pts' ) . '</th>';
								echo '<td>';
								echo esc_html( $order->get_billing_phone() );
								echo '</td>';
							echo '</tr>';
				
							$has_data = true;
						}
				
						if ( ! $has_data ) {
							echo '<tr>';
								echo '<td colspan="2">' . __( 'N/A', 'woocommerce-bookings' ) . '</td>';
							echo '</tr>';
						}
				
						echo '</table>';
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	
	/**
	 * Add vendor email to booking admin emails
	 */
	public function wcb_wcmp_pts_filter_booking_emails( $recipients, $this_email ) {
		global $WCMp, $WCMp_Product_Types;
		
		if ( ! empty( $this_email ) ) {
			$product = get_post( $this_email->product_id );
			$vendor_id = $product->post_author;
			if( is_user_wcmp_vendor( $vendor_id ) ) {
				$vendor_data = get_userdata( $vendor_id );
				if ( ! empty( $vendor_data ) ) {
					if ( isset( $recipients ) ) {
						$recipients .= ',' . $vendor_data->user_email;
					} else {
						$recipients = $vendor_data->user_email;
					}
				}
			}
		}

		return $recipients;
	}
}