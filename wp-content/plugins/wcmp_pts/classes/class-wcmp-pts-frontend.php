<?php

/**
 * WCMp Product Types plugin core
 *
 * WCMp Product Types Frontend
 *
 * @author 		WC Marketplace
 * @package 	wcmp-pts/classes
 * @version   1.0.0
 */
 
class WCMp_Product_Types_Frontend {
	
	public function __construct() {
		
		//enqueue scripts
		add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts'));
		//enqueue styles
		add_action('wp_enqueue_scripts', array(&$this, 'frontend_styles'));
	}
	
		function frontend_scripts() {
		global $WCMp, $WCMp_Product_Types;
		$frontend_script_path = $WCMp_Product_Types->plugin_url . 'assets/frontend/js/';
		$frontend_script_path = str_replace( array( 'http:', 'https:' ), '', $frontend_script_path );
		$pluginURL = str_replace( array( 'http:', 'https:' ), '', $WCMp_Product_Types->plugin_url );
		$suffix 				= defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		
		// Product Manager Script 
		if(is_forntend_product_manager_page()) {  
			
			if( wcmp_pts_is_booking() ) {
				wp_enqueue_script('wcmp_pts_booking_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/wcmp_pts_wcbookings.js', array('jquery', 'product_manager_js'), $WCMp_Product_Types->version, true);
			}
			
			if( wcmp_pts_is_subscription() ) {
				wp_enqueue_script('wcmp_pts_subscription_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/wcmp_pts_wcsubscriptions.js', array('jquery', 'product_manager_js'), $WCMp_Product_Types->version, true);
			}
			
			if( wcmp_pts_is_rental() ) {
				wp_enqueue_script('wcmp_pts_rental_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/wcmp_pts_wcrental.js', array('jquery', 'product_manager_js'), $WCMp_Product_Types->version, true);
			}
			
			if( wcmp_pts_is_yithauction() || wcmp_pts_is_wcsauction() ) {
				wp_enqueue_script('jquery-ui-datepicker');
				wp_enqueue_script('wcmp_pts_timepicker_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/timepicker.js', array('jquery', 'jquery-ui-datepicker'), $WCMp_Product_Types->version, true);
			}
			
			if( wcmp_pts_is_yithauction() ) {
				wp_enqueue_script('wcmp_pts_yithauction_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/wcmp_pts_yithauction.js', array('jquery', 'product_manager_js', 'wcmp_pts_timepicker_js', 'jquery-ui-datepicker'), $WCMp_Product_Types->version, true);
			}
			
			if( wcmp_pts_is_wcsauction() ) {
				wp_enqueue_script('wcmp_pts_wcsauction_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/wcmp_pts_wcsauction.js', array('jquery', 'product_manager_js', 'wcmp_pts_timepicker_js', 'jquery-ui-datepicker'), $WCMp_Product_Types->version, true);
			}
			
			if( wcmp_pts_has_wcaddons() ) {
				wp_enqueue_script('wcmp_pts_wcaddons_js', $WCMp_Product_Types->plugin_url.'assets/frontend/js/wcmp_pts_wcaddons.js', array('jquery', 'product_manager_js'), $WCMp_Product_Types->version, true);
			}
		}
	}

	function frontend_styles() {
		global $WCMp_Product_Types;
		$frontend_style_path = $WCMp_Product_Types->plugin_url . 'assets/frontend/css/';
		$frontend_style_path = str_replace( array( 'http:', 'https:' ), '', $frontend_style_path );
		$suffix 				= defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		// Product Manager Style
		if(is_forntend_product_manager_page()) {
			if( wcmp_pts_is_booking() ) {
				wp_enqueue_style( 'wcmp_pts_wcbooking_css',  $WCMp_Product_Types->plugin_url . 'assets/frontend/css/wcmp_pts_wcbookings.css', array( 'product_manager_css' ), $WCMp_Product_Types->version);
			}
			
			if( wcmp_pts_is_subscription() ) {
				wp_enqueue_style( 'wcmp_pts_wcsubscriptions_css',  $WCMp_Product_Types->plugin_url . 'assets/frontend/css/wcmp_pts_wcsubscriptions.css', array( 'product_manager_css' ), $WCMp_Product_Types->version);
			}
			
			if( wcmp_pts_is_rental() ) {
				//wp_enqueue_style( 'wcmp_pts_wcrental_css',  $WCMp_Product_Types->plugin_url . 'assets/frontend/css/wcmp_pts_wcrental.css', array( 'product_manager_css' ), $WCMp_Product_Types->version);
			}
			
			if( wcmp_pts_is_yithauction() || wcmp_pts_is_wcsauction() ) {
				wp_enqueue_style( 'wcmp_pts_timepicker_css',  $WCMp_Product_Types->plugin_url . 'assets/frontend/css/timepicker.css', array( 'product_manager_css' ), $WCMp_Product_Types->version);
			}
		}
	}
}