<?php
/**
 * WCMp Product Types plugin views
 *
 * Plugin YITH Auction Products Manage Views
 *
 * @author 		WC Marketplace
 * @package 	wcmp-pts/views
 * @version   1.0.3
 */
global $wp, $WCMp_Product_Types;

$_yith_auction_start_price = '';
$_yith_auction_bid_increment = '';
$_yith_auction_minimum_increment_amount = '';
$_yith_auction_reserve_price = '';
$_yith_auction_buy_now = '';
$_yith_auction_for = '';
$_yith_auction_to = '';
$_yith_check_time_for_overtime_option = '';
$_yith_overtime_option = '';
$_yith_wcact_auction_automatic_reschedule = '';
$_yith_wcact_automatic_reschedule_auction_unit = '';
$_yith_wcact_upbid_checkbox = '';
$_yith_wcact_overtime_checkbox = '';

if( $product_id ) {
	$_yith_auction_start_price = get_post_meta( $product_id, '_yith_auction_start_price', true );
	$_yith_auction_bid_increment = get_post_meta( $product_id, '_yith_auction_bid_increment', true );
	$_yith_auction_minimum_increment_amount = get_post_meta( $product_id, '_yith_auction_minimum_increment_amount', true );
	$_yith_auction_reserve_price = get_post_meta( $product_id, '_yith_auction_reserve_price', true );
	$_yith_auction_buy_now = get_post_meta( $product_id, '_yith_auction_buy_now', true );
	$_yith_auction_for = get_post_meta( $product_id, '_yith_auction_for', true );
	$_yith_auction_to = get_post_meta( $product_id, '_yith_auction_to', true );
	$_yith_check_time_for_overtime_option = get_post_meta( $product_id, '_yith_check_time_for_overtime_option', true );
	$_yith_overtime_option = get_post_meta( $product_id, '_yith_overtime_option', true );
	$_yith_wcact_auction_automatic_reschedule = get_post_meta( $product_id, '_yith_wcact_auction_automatic_reschedule', true );
	$_yith_wcact_automatic_reschedule_auction_unit = get_post_meta( $product_id, '_yith_wcact_automatic_reschedule_auction_unit', true );
	$_yith_wcact_upbid_checkbox = get_post_meta( $product_id, '_yith_wcact_upbid_checkbox', true );
	$_yith_wcact_overtime_checkbox = get_post_meta( $product_id, '_yith_wcact_overtime_checkbox', true );
	
	if( $_yith_auction_for ) $_yith_auction_for = date( 'Y-m-d h:i:s', $_yith_auction_for);
	if( $_yith_auction_to ) $_yith_auction_to = date( 'Y-m-d h:i:s', $_yith_auction_to);
	
}


?>

<h3 class="pro_ele_head products_manage_yith_auction auction"><?php _e('Auction', 'wcmp_pts'); ?></h3>
<div class="pro_ele_block auction">
	<?php
	$WCMp_Product_Types->wcmp_wp_fields->dc_generate_form_field( array( 
		"_yith_auction_start_price" => array( 'label' => __('Starting Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_start_price ),
		"_yith_auction_bid_increment" => array( 'label' => __('Bid up', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_bid_increment ),
		"_yith_auction_minimum_increment_amount" => array( 'label' => __('Minimum increment amount', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_minimum_increment_amount ),
		"_yith_auction_reserve_price" => array( 'label' => __('Reserve price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_reserve_price ),
		"_yith_auction_buy_now" => array( 'label' => __('Buy it now price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_buy_now ),
		"_yith_auction_for" => array( 'label' => __('Auction Date From', 'wcmp_pts') , 'type' => 'text', 'placeholder' => 'YYYY-MM-DD hh:mm:ss', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_for ),
		"_yith_auction_to" => array( 'label' => __('Auction Date To', 'wcmp_pts') , 'type' => 'text', 'placeholder' => 'YYYY-MM-DD hh:mm:ss', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_auction_to ),
		"_yith_check_time_for_overtime_option" => array( 'label' => __('Time to add overtime', 'wcmp_pts') , 'type' => 'number', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_check_time_for_overtime_option, 'hints' => __( 'Number of minutes before auction ends to check if overtime added. (Override the settings option)', 'wcmp_pts' ) ),
		"_yith_overtime_option" => array( 'label' => __('Overtime', 'wcmp_pts') , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_overtime_option, 'hints' => __( 'Number of minutes by which the auction will be extended. (Overrride the settings option)', 'wcmp_pts' ) ),
		"_yith_wcact_auction_automatic_reschedule" => array( 'label' => __('Time value for automatic rescheduling', 'wcmp_pts') , 'type' => 'text', 'class' => 'regular-text pro_ele auction', 'label_class' => 'pro_title auction', 'value' => $_yith_wcact_auction_automatic_reschedule ),
		"_yith_wcact_automatic_reschedule_auction_unit" => array( 'label' => __('Select unit for automatic rescheduling', 'wcmp_pts') , 'type' => 'select', 'class' => 'regular-select pro_ele auction', 'label_class' => 'pro_title auction', 'options' => array( 'days' => __( 'days', 'wcmp_pts' ), 'hours' => __( 'hours', 'wcmp_pts' ), 'minutes' => __( 'minutes', 'wcmp_pts' ) ), 'value' => $_yith_wcact_automatic_reschedule_auction_unit ),
		"_yith_wcact_upbid_checkbox" => array( 'label' => __('Show bid up', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele auction', 'label_class' => 'pro_title auction', 'value' => 'yes', 'dfvalue' => $_yith_wcact_upbid_checkbox ),
		"_yith_wcact_overtime_checkbox" => array( 'label' => __('Show overtime', 'wcmp_pts') , 'type' => 'checkbox', 'class' => 'regular-checkbox pro_ele auction', 'label_class' => 'pro_title auction', 'value' => 'yes', 'dfvalue' => $_yith_wcact_overtime_checkbox ),
		
		) );
	?>
</div>