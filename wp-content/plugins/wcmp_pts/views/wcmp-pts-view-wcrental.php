<?php
/**
 * WCMp Product Types plugin views
 *
 * Plugin WC Rental & Booking Products Manage Views
 *
 * @author 		WC Marketplace
 * @package 	wcmp-pts/views
 * @version   1.0.3
 */
global $wp, $WCMp_Product_Types;

$pricing_type = '';
$hourly_price = '';
$general_price = '';

$friday_price = '';
$saterday_price = '';
$sunday_price = '';
$monday_price = '';
$tuesday_price = '';
$wednesday_price = '';
$thrusday_price = '';

$january_price = '';
$february_price = '';
$march_price = '';
$april_price = '';
$may_price = '';
$june_price = '';
$july_price = '';
$august_price = '';
$september_price = '';
$october_price = '';
$november_price = '';
$december_price = '';


$redq_rental_availability = array();

if( $product_id ) {
	$pricing_type = get_post_meta( $product_id, 'pricing_type', true );
	$hourly_price = get_post_meta( $product_id, 'hourly_price', true );
	$general_price = get_post_meta( $product_id, 'general_price', true );
	
	$friday_price = get_post_meta( $product_id, 'friday_price', true );
	$saterday_price = get_post_meta( $product_id, 'saterday_price', true );
	$sunday_price = get_post_meta( $product_id, 'sunday_price', true );
	$monday_price = get_post_meta( $product_id, 'monday_price', true );
	$tuesday_price = get_post_meta( $product_id, 'tuesday_price', true );
	$wednesday_price = get_post_meta( $product_id, 'wednesday_price', true );
	$thrusday_price = get_post_meta( $product_id, 'thrusday_price', true );
	
	$january_price = get_post_meta( $product_id, 'january_price', true );
	$february_price = get_post_meta( $product_id, 'february_price', true );
	$march_price = get_post_meta( $product_id, 'march_price', true );
	$april_price = get_post_meta( $product_id, 'april_price', true );
	$may_price = get_post_meta( $product_id, 'may_price', true );
	$june_price = get_post_meta( $product_id, 'june_price', true );
	$july_price = get_post_meta( $product_id, 'july_price', true );
	$august_price = get_post_meta( $product_id, 'august_price', true );
	$september_price = get_post_meta( $product_id, 'september_price', true );
	$october_price = get_post_meta( $product_id, 'october_price', true );
	$november_price = get_post_meta( $product_id, 'november_price', true );
	$december_price = get_post_meta( $product_id, 'december_price', true );
	
	$redq_rental_availability = (array) get_post_meta( $product_id, 'redq_rental_availability', true );
}

?>


<h3 class="pro_ele_head products_manage_rental_pricing redq_rental"><?php _e('Price Calculation', 'wcmp_pts'); ?></h3>
<div class="pro_ele_block redq_rental">
	<?php
	$WCMp_Product_Types->wcmp_wp_fields->dc_generate_form_field( array( 
		"pricing_type" => array( 'label' => __('Set Price Type', 'wcmp_pts') , 'type' => 'select', 'options' => array( 'general_pricing' => __( 'General Pricing', 'wcmp_pts' ), 'daily_pricing' => __( 'Daily Pricing', 'wcmp_pts' ), 'monthly_pricing' => __( 'Monthly Pricing', 'wcmp_pts' ) ), 'class' => 'regular-select pro_ele redq_rental', 'label_class' => 'pro_title redq_rental', 'value' => $pricing_type, 'hints' => __( 'Choose a price type - this controls the schema.', 'wcmp_pts' ) ),
		"hourly_price" => array( 'label' => __('Hourly Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele redq_rental', 'label_class' => 'pro_title redq_rental', 'value' => $hourly_price, 'hints' => __( 'Hourly price will be applicabe if booking or rental days min 1day', 'wcmp_pts' ), 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"general_price" => array( 'label' => __('General Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_general_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_general_pricing redq_rental', 'value' => $general_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		
		"friday_price" => array( 'label' => __('Friday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $friday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"saterday_price" => array( 'label' => __('Saterday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $saterday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"sunday_price" => array( 'label' => __('Sunday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $sunday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"monday_price" => array( 'label' => __('Monday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $monday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"tuesday_price" => array( 'label' => __('Tuesday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $tuesday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"wednesday_price" => array( 'label' => __('Wednesday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $wednesday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"thrusday_price" => array( 'label' => __('Thrusday Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_daily_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_daily_pricing redq_rental', 'value' => $thrusday_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		
		"january_price" => array( 'label' => __('January Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $january_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"february_price" => array( 'label' => __('February Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $february_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"march_price" => array( 'label' => __('March Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $march_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"april_price" => array( 'label' => __('April Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $april_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"may_price" => array( 'label' => __('May Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $may_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"june_price" => array( 'label' => __('June Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $june_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"july_price" => array( 'label' => __('July Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $july_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"august_price" => array( 'label' => __('August Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $august_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"september_price" => array( 'label' => __('September Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $september_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"october_price" => array( 'label' => __('October Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $october_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"november_price" => array( 'label' => __('November Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $november_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		"december_price" => array( 'label' => __('December Price', 'wcmp_pts') . '(' . get_woocommerce_currency_symbol() . ')' , 'type' => 'number', 'class' => 'regular-text pro_ele rentel_pricing rental_monthly_pricing redq_rental', 'label_class' => 'pro_title rentel_pricing rental_monthly_pricing redq_rental', 'value' => $december_price, 'placeholder' => __( 'Enter price here', 'wcmp_pts' ) ),
		) );
	?>
</div>

<h3 class="pro_ele_head products_manage_rental_availability redq_rental"><?php _e('Availability', 'wcmp_pts'); ?></h3>
<div class="pro_ele_block redq_rental">
  <?php
	$WCMp_Product_Types->wcmp_wp_fields->dc_generate_form_field( array( 
		"redq_rental_availability" =>   array('label' => __('Product Availabilities', 'wcmp_pts') , 'type' => 'multiinput', 'class' => 'regular-text pro_ele redq_rental', 'label_class' => 'pro_title redq_rental', 'desc' => __( 'Please select the date range to be disabled for the product.', 'wcmp_pts' ), 'desc_class' => 'avail_rules_desc', 'value' => $redq_rental_availability, 'options' => array(
									"type" => array('label' => __('Type', 'wcmp_pts'), 'type' => 'select', 'options' => array( 'custom_date' => __( 'Custom Date', 'wcmp_pts' )), 'class' => 'regular-select pro_ele avail_range_type redq_rental', 'label_class' => 'pro_title avail_rules_ele avail_rules_label redq_rental' ),
									"from" => array('label' => __('From', 'wcmp_pts'), 'type' => 'text', 'class' => 'regular-text dc_datepicker avail_rule_field avail_rule_custom avail_rules_ele avail_rules_text', 'label_class' => 'pro_title avail_rule_field avail_rule_custom avail_rules_ele avail_rules_label' ),
									"to" => array('label' => __('To', 'wcmp_pts'), 'type' => 'text', 'class' => 'regular-text dc_datepicker avail_rule_field avail_rule_custom avail_rules_ele avail_rules_text', 'label_class' => 'pro_title avail_rule_field avail_rule_custom avail_rules_ele avail_rules_label' ),
									"rentable" => array('label' => __('Bookable', 'wcmp_pts'), 'type' => 'select', 'options' => array( 'no' => 'NO' ), 'class' => 'regular-select pro_ele avail_rules_ele avail_rules_text redq_rental', 'label_class' => 'pro_title avail_rules_ele avail_rules_label' ),
									)	)
		) );
  ?>
</div>