<?php
/**
 * WCMp Product Types plugin views
 *
 * Plugin WC Product Addons Products Manage Views
 *
 * @author 		WC Marketplace
 * @package 	wcmp-pts/views
 * @version   1.1.1
 */
global $wp, $WCMp_Product_Types;

$_product_addons = array();

if( $product_id ) {
	$_product_addons = (array) get_post_meta( $product_id, '_product_addons', true );
}

$group_types = array( 'custom_price'               => __( 'Additional custom price input', 'woocommerce-product-addons' ),
											'input_multiplier'           => __( 'Additional price multiplier', 'woocommerce-product-addons' ),
											'checkbox'                   => __( 'Checkboxes', 'woocommerce-product-addons' ),
											'custom_textarea'            => __( 'Custom input (textarea)', 'woocommerce-product-addons' ),
											'custom'                     => __( 'Any text', 'woocommerce-product-addons' ),
											'custom_email'               => __( 'Email address', 'woocommerce-product-addons' ),
											'custom_letters_only'        => __( 'Only letters', 'woocommerce-product-addons' ),
											'custom_letters_or_digits'   => __( 'Only letters and numbers', 'woocommerce-product-addons' ),
											'custom_digits_only'         => __( 'Only numbers', 'woocommerce-product-addons' ),
											'file_upload'                => __( 'File upload', 'woocommerce-product-addons' ),
											'radiobutton'                => __( 'Radio buttons', 'woocommerce-product-addons' ),
											'select'                     => __( 'Select box', 'woocommerce-product-addons' )
										);

?>

<h3 class="pro_ele_head products_manage_wcs_addons wcaddons"><?php _e('Add-ons', 'woocommerce-product-addons'); ?></h3>
<div class="pro_ele_block wcaddons">
	<?php
	$WCMp_Product_Types->wcmp_wp_fields->dc_generate_form_field( array( 
		"_product_addons" =>     array('label' => __('Add-ons', 'woocommerce-product-addons') , 'type' => 'multiinput', 'class' => 'pro_ele wcaddons', 'label_class' => 'pro_title wcaddons', 'value' => $_product_addons, 'options' => array(
									"type" => array('label' => __('Gruop', 'woocommerce-product-addons'), 'type' => 'select', 'options' => $group_types, 'class' => 'regular-select addon_fields_option wcaddons', 'label_class' => 'pro_title wcaddons' ),
									"name" => array('label' => __('Name', 'woocommerce-product-addons'), 'type' => 'text', 'class' => 'regular-text', 'label_class' => 'pro_title' ),
									"position" => array( 'type' => 'hidden' ),
									"description" => array('label' => __('Description', 'woocommerce-product-addons'), 'type' => 'textarea', 'class' => 'regular-text', 'label_class' => 'pro_title' ),
									"required" => array('label' => __('Required fields?', 'woocommerce-product-addons'), 'type' => 'checkbox', 'class' => 'regular-checkbox', 'label_class' => 'pro_title checkbox_title', 'value' => 1 ),
									"options" =>     array('label' => __('Options', 'wcmp_pts') , 'type' => 'multiinput', 'class' => 'pro_ele wcaddons', 'label_class' => 'pro_title wcaddons', 'options' => array(
										  "label" => array('label' => __('Label', 'woocommerce-product-addons'), 'type' => 'text', 'class' => 'regular-text', 'label_class' => 'pro_title' ),
										  "price" => array('label' => __('Price', 'woocommerce-product-addons'), 'type' => 'text', 'class' => 'regular-text addon_fields addon_price', 'label_class' => 'pro_title addon_fields addon_price' ),
										  "min" => array('label' => __('Min', 'woocommerce-product-addons'), 'type' => 'number', 'class' => 'regular-text addon_fields addon_minmax', 'label_class' => 'pro_title addon_fields addon_minmax' ),
										  "max" => array('label' => __('Max', 'woocommerce-product-addons'), 'type' => 'number', 'class' => 'regular-text addon_fields addon_minmax', 'label_class' => 'pro_title addon_fields addon_minmax' ),
										) )
									)	)
		) );
	?>
</div>