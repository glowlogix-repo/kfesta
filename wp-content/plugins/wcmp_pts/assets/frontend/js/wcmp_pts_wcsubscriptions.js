jQuery(document).ready(function($) {
  $('#_subscription_period').change(function() {
  	$('.subscription_length_ele').addClass('pro_ele_hide pro_title_hide');
  	if( $('#product_type').val() == 'subscription' ) {
  		$('.subscription_length_' + $(this).val()).removeClass('pro_ele_hide pro_title_hide');
  	}
  }).change();
  
  $('#product_type').change(function() {
  	$('#_subscription_period').change();	
  });
  
  $('.variable-subscription_period').change(function() {
  	$(this).parent().find('.variable-subscription_length_ele').addClass('pro_ele_hide pro_title_hide');
  	if( $('#product_type').val() == 'variable-subscription' ) {
  		$(this).parent().find('.variable-subscription_length_' + $(this).val()).removeClass('pro_ele_hide pro_title_hide');
  	}
  }).change();
  
  $('#product_type').change(function() {
  	$('.variable-subscription_period').change();	
  });
  
  $('#variations').find('.add_multi_input_block').click(function() {
  	$('.variable-subscription_period:last').change(function() {
			$(this).parent().find('.variable-subscription_length_ele').addClass('pro_ele_hide pro_title_hide');
			if( $('#product_type').val() == 'variable-subscription' ) {
				$(this).parent().find('.variable-subscription_length_' + $(this).val()).removeClass('pro_ele_hide pro_title_hide');
			}
		});
  });
});