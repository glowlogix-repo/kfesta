<?php
if(!function_exists('wcmp_pts_woocommerce_inactive_notice')) {
	function wcmp_pts_woocommerce_inactive_notice() {
		?>
		<div id="message" class="error">
		<p><?php printf( __( '%sWCMp Product Types is inactive.%s The %sWooCommerce plugin%s must be active for the WCMp Product Types to work. Please %sinstall & activate WooCommerce%s', WCMP_PTS_TEXT_DOMAIN ), '<strong>', '</strong>', '<a target="_blank" href="http://wordpress.org/extend/plugins/woocommerce/">', '</a>', '<a href="' . admin_url( 'plugin-install.php?tab=search&s=woocommerce' ) . '">', '&nbsp;&raquo;</a>' ); ?></p>
		</div>
		<?php
	}
}

if(!function_exists('wcmp_pts_wcmp_inactive_notice')) {
	function wcmp_pts_wcmp_inactive_notice() {
		?>
		<div id="message" class="error">
		<p><?php printf( __( '%sWCMp Product Types is inactive.%s The %sWC Marketplace%s must be active for the WCMp Product Types to work. Please %sinstall & activate WC Marketplace%s', WCMP_PTS_TEXT_DOMAIN ), '<strong>', '</strong>', '<a target="_blank" href="https://wordpress.org/plugins/dc-woocommerce-multi-vendor/">', '</a>', '<a href="' . admin_url( 'plugin-install.php?tab=search&s=wc+marketplace' ) . '">', '&nbsp;&raquo;</a>' ); ?></p>
		</div>
		<?php
	}
}

if(!function_exists('wcmp_pts_fpm_inactive_notice')) {
	function wcmp_pts_fpm_inactive_notice() {
		?>
		<div id="message" class="error">
		<p><?php printf( __( '%sWCMp Product Types is inactive.%s The %sWCMp Frontend Manager%s must be active for the WCMp Product Types to work. Please %sinstall & activate WCMp Frontend Manager%s', WCMP_PTS_TEXT_DOMAIN ), '<strong>', '</strong>', '<a target="_blank" href="https://wc-marketplace.com/product/wcmp-frontend-product-manager/">', '</a>', '<a href="https://wc-marketplace.com/product/wcmp-frontend-product-manager/">', '&nbsp;&raquo;</a>' ); ?></p>
		</div>
		<?php
	}
}

if( !function_exists( 'wcmp_pts_is_booking' ) ) {
	function wcmp_pts_is_booking() {
		$active_plugins = (array) get_option( 'active_plugins', array() );
		if (is_multisite())
			$active_plugins = array_merge( $active_plugins, get_site_option('active_sitewide_plugins', array() ) );
		
		// WC Booking Check
		$is_booking = ( in_array( 'woocommerce-bookings/woocommerce-bookings.php', $active_plugins ) || array_key_exists( 'woocommerce-bookings/woocommerce-bookings.php', $active_plugins ) ) ? 'wcbooking' : false;
		
		return $is_booking;
	}
}

if( !function_exists( 'wcmp_pts_is_subscription' ) ) {
	function wcmp_pts_is_subscription() {
		$active_plugins = (array) get_option( 'active_plugins', array() );
		if (is_multisite())
			$active_plugins = array_merge( $active_plugins, get_site_option('active_sitewide_plugins', array() ) );
		
		// WC Subscriptions Check
		$is_subscriptions = ( in_array( 'woocommerce-subscriptions/woocommerce-subscriptions.php', $active_plugins ) || array_key_exists( 'woocommerce-subscriptions/woocommerce-subscriptions.php', $active_plugins ) ) ? 'wcsubscriptions' : false;
		
		return $is_subscriptions;
	}
}

if( !function_exists( 'wcmp_pts_is_rental' ) ) {
	function wcmp_pts_is_rental() {
		$active_plugins = (array) get_option( 'active_plugins', array() );
		if (is_multisite())
			$active_plugins = array_merge( $active_plugins, get_site_option('active_sitewide_plugins', array() ) );
		
		// WC Rental & Booking Check
		$is_rental = ( in_array( 'booking-and-rental-system-woocommerce/redq-rental-and-bookings.php', $active_plugins ) || array_key_exists( 'booking-and-rental-system-woocommerce/redq-rental-and-bookings.php', $active_plugins ) ) ? 'wcrental' : false;
		
		return $is_rental;
	}
}

if( !function_exists( 'wcmp_pts_is_yithauction' ) ) {
	function wcmp_pts_is_yithauction() {
		$active_plugins = (array) get_option( 'active_plugins', array() );
		if (is_multisite())
			$active_plugins = array_merge( $active_plugins, get_site_option('active_sitewide_plugins', array() ) );
		
		// YITH Auctions Check
		$is_yithauction = ( in_array( 'yith-woocommerce-auctions-premium/init.php', $active_plugins ) || array_key_exists( 'yith-woocommerce-auctions-premium/init.php', $active_plugins ) ) ? 'yithauction' : false;
		
		return $is_yithauction;
	}
}

if( !function_exists( 'wcmp_pts_is_wcsauction' ) ) {
	function wcmp_pts_is_wcsauction() {
		$active_plugins = (array) get_option( 'active_plugins', array() );
		if (is_multisite())
			$active_plugins = array_merge( $active_plugins, get_site_option('active_sitewide_plugins', array() ) );
		
		// YITH Auctions Check
		$is_yithauction = ( in_array( 'woocommerce-simple-auctions/woocommerce-simple-auctions.php', $active_plugins ) || array_key_exists( 'woocommerce-simple-auctions/woocommerce-simple-auctions.php', $active_plugins ) ) ? 'yithauction' : false;
		
		return $is_yithauction;
	}
}

if( !function_exists( 'wcmp_pts_has_wcaddons' ) ) {
	function wcmp_pts_has_wcaddons() {
		$active_plugins = (array) get_option( 'active_plugins', array() );
		if (is_multisite())
			$active_plugins = array_merge( $active_plugins, get_site_option('active_sitewide_plugins', array() ) );
		
		// YITH Auctions Check
		$is_yithauction = ( in_array( 'woocommerce-product-addons/woocommerce-product-addons.php', $active_plugins ) || array_key_exists( 'woocommerce-product-addons/woocommerce-product-addons.php', $active_plugins ) ) ? 'yithauction' : false;
		
		return $is_yithauction;
	}
}
?>