<?php
/**
 * Plugin Name: WCMp Advanced Product Types
 * Plugin URI: https://wc-marketplace.com/product/wcmp-advanced-product-types/
 * Description: Allow your vendors to create and manage any kind of WC products from their individual shops front end using the WCMp Frontend Manager.
 * Author: WC Marketplace, Arim Ghosh
 * Version: 1.1.2
 * Author URI: https://wc-marketplace.com
 *
 * Text Domain: wcmp_pts
 * Domain Path: /languages/
 */

if(!defined('ABSPATH')) exit; // Exit if accessed directly


if ( ! class_exists( 'WCMp_Product_Types_Dependencies' ) )
	require_once 'includes/class-wcmp-pts-dependencies.php';
require_once 'includes/wcmp-pts-core-functions.php';
require_once 'wcmp_pts_config.php';

if(!defined('WCMP_PTS_PLUGIN_TOKEN')) exit;
if(!defined('WCMP_PTS_TEXT_DOMAIN')) exit;


if(WCMp_Product_Types_Dependencies::woocommerce_plugin_active_check()) {
	if(WCMp_Product_Types_Dependencies::wc_marketplace_plugin_active_check()) {
		if(WCMp_Product_Types_Dependencies::wcmp_fpm_plugin_active_check()) {
			if(!class_exists('WCMp_Product_Types')) {
				require_once( 'classes/class-wcmp-pts.php' );
				global $WCMp_Product_Types;
				$WCMp_Product_Types = new WCMp_Product_Types( __FILE__ );
				$GLOBALS['WCMp_Product_Types'] = $WCMp_Product_Types;
				
				// Activation Hooks
				register_activation_hook( __FILE__, array('WCMp_Product_Types', 'activate_wcmp_product_types') );
				register_activation_hook( __FILE__, 'flush_rewrite_rules' );
				
				// Deactivation Hooks
				register_deactivation_hook( __FILE__, array('WCMp_Product_Types', 'deactivate_wcmp_product_types') );
			}
		} else {
			add_action( 'admin_notices', 'wcmp_pts_fpm_inactive_notice' );
		}
	} else {
		add_action( 'admin_notices', 'wcmp_pts_wcmp_inactive_notice' );
	}
} else {
	add_action( 'admin_notices', 'wcmp_pts_woocommerce_inactive_notice' );
}
?>