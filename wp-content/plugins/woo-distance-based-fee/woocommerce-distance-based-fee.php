<?php
/*
 * Plugin Name: WooCommerce Distance Based Fee
 * Version: 1.0.3
 * Plugin URI: https://www.webbisivut.org/
 * Description: This plugin adds fee based on distance
 * Author: Webbisivut.org
 * Author URI: https://www.webbisivut.org
 * Requires at least: 4.0
 * Tested up to: 4.9.1
 *
 * Text Domain: woocommerce-distance-based-fee
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Webbisivut.org
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load plugin class files
require_once( 'includes/class-woocommerce-distance-based-fee.php' );
require_once( 'includes/class-woocommerce-distance-based-fee-settings.php' );
require_once( 'includes/class-woocommerce-distance-based-fee-functions.php' );

// Load plugin libraries
require_once( 'includes/lib/class-woocommerce-distance-based-fee-admin-api.php' );

/**
 * Returns the main instance of WooCommerce_Distance_Based_Fee to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object WooCommerce_Distance_Based_Fee
 */
function WooCommerce_Distance_Based_Fee () {
	$instance = WooCommerce_Distance_Based_Fee::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = WooCommerce_Distance_Based_Fee_Settings::instance( $instance );
	}

	return $instance;
}

WooCommerce_Distance_Based_Fee();
