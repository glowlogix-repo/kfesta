<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Check selected shipping method
 * Return string
 */
function checkChosenMethod() {
	$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
	$chosen_method = $chosen_methods[0];

	$chosen_method = preg_replace('/[0-9:]+/', '', $chosen_method);

	return $chosen_method;
}

/**
 * Send data to Google API
 * Return array
 */
function sendDataToAPI($setUnits, $origin, $destination, $apiKey) {
	$header = array();
	$header[] = 'Content-length: 0';
	$header[] = 'Content-type: application/json';
																												
	$service_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?mode=car&language=en-GB' . $setUnits . '&origins=' . $origin . '&destinations=' . $destination . '&key=' . $apiKey;

	$response = wp_remote_get($service_url);
	$body = wp_remote_retrieve_body( $response );
	$bodyDecoded = json_decode($body, true);

	return $bodyDecoded;
}

/**
 * Add fee on article specifics
 * @param WC_Cart $cart
 */
function add_woocommerce_distance_fee_fees(){
	
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) 
	return;

	global $woocommerce;

	$chosen_method = checkChosenMethod();

	$apiKey = esc_attr( get_option('wc_distance_fee_google_api_key') );
	$feeName = esc_attr( get_option('wc_distance_fee_fee_name') );
	$divider = floatval(esc_attr( get_option('wc_distance_fee_divider') ));
	$price = floatval(esc_attr( get_option('wc_distance_fee_price') ));
	$units = esc_attr( get_option('wc_distance_fee_units') );
	$methods = get_option('wc_distance_fee_methods');
	$tax = esc_attr( get_option('wc_distance_fee_taxable') );

	if($units == 'ml') {
		$setUnits = '&units=imperial';
	} else {
		$setUnits = '&units=metric';
	}

	if( $tax == 'yes' ) {
		$addTaxes = true;
	} else {
		$addTaxes = false;
	}

	$originCity = esc_attr(get_option('woocommerce_store_city'));
	$originAddress = esc_attr(get_option('woocommerce_store_address'));
	$originPostcode = esc_attr(get_option('woocommerce_store_postcode'));
	
	$destinationCity = WC()->customer->get_shipping_city();
	$destinationAddress = WC()->customer->get_shipping_address();
	$destinationPostcode = WC()->customer->get_shipping_postcode();

	$origin = urlencode($originAddress . ',' . $originPostcode . ' ' .$originCity);
	$destination = urlencode($destinationAddress . ',' . $destinationPostcode . ' ' . $destinationCity);

	$response = sendDataToAPI($setUnits, $origin, $destination, $apiKey);

	$calculatedFee = 0;

	if($response['rows'][0]['elements'][0]['status'] !== 'NOT_FOUND') {
		$meters = $response['rows'][0]['elements'][0]['distance']['value'];
		$kiloMeters = ($meters / 1000);

		$calculatedFee = ($kiloMeters / $divider) * $price;
	}

	if ( $calculatedFee > 0 && in_array( $chosen_method, $methods ) ) {
		// Annetaan kassalla lisäkululle nimi
		WC()->cart->add_fee( $feeName, $calculatedFee, $addTaxes, '' );
	} else {
		unset($chosen_method);
	}
}

add_action( 'woocommerce_cart_calculate_fees' , 'add_woocommerce_distance_fee_fees' );
add_action( 'woocommerce_after_cart_item_quantity_update', 'add_woocommerce_distance_fee_fees' );

/**
 * Hide shipping methods if destination cannot be found
 * @param WC_Cart $cart
 */
function hide_show_fee_based_shipping( $rates, $package ) {
	$originCity = esc_attr(get_option('woocommerce_store_city'));
	$originAddress = esc_attr(get_option('woocommerce_store_address'));
	$originPostcode = esc_attr(get_option('woocommerce_store_postcode'));
	
	$destinationCity = WC()->customer->get_shipping_city();
	$destinationAddress = WC()->customer->get_shipping_address();
	$destinationPostcode = WC()->customer->get_shipping_postcode();

	$origin = urlencode($originAddress . ',' . $originPostcode . ' ' .$originCity);
	$destination = urlencode($destinationAddress . ',' . $destinationPostcode . ' ' . $destinationCity);

	$apiKey = esc_attr( get_option('wc_distance_fee_google_api_key') );
	$units = esc_attr( get_option('wc_distance_fee_units') );

	if($units == 'ml') {
		$setUnits = '&units=imperial';
	} else {
		$setUnits = '&units=metric';
	}

	$methods = get_option('wc_distance_fee_methods');
	$chosen_method = checkChosenMethod();

	$response = sendDataToAPI($setUnits, $origin, $destination, $apiKey);

	$calculatedFee = 0;

	if($response['rows'][0]['elements'][0]['status'] == 'NOT_FOUND') {
		$new_rates = array();

		foreach ( $rates as $rate_id => $rate ) {

			if ( !in_array($rate->method_id, $methods) ) {
				$new_rates[ $rate_id ] = $rate;
			}
		}
		
		return $new_rates;

	} else {
		return $rates;
	}
	
}

add_filter( 'woocommerce_package_rates', 'hide_show_fee_based_shipping' , 100, 2 );