=== WooCommerce Distance Based Fee ===
Contributors: w-labs
Tags: woocommerce, plugin, custom fee, distance, google, matrix, api
Requires at least: 4.0
Tested up to: 4.9.1
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin uses Google Matrix API to add custom fee based on distance between customers destination city and online stores location. Fee will be calculated by dividing the distance with user entered divider, and then multiplicated by the price. For example, if distance is 100km and the divider is 2 and price is 5 euros. The calculation formula will be (100 / 2) * 5 = 250 euros. If destination cannot be found, shipping method will be hidden from user.

Plugin settings can be found at Settings - Distance based fee settings.

This plugin requires WooCommerce version 3.2.0 or later.

== Installation ==

Installing "WooCommerce distance based fee" can be done either by searching for "WooCommerce distance based fee" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
1. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==
= 1.0.3 =
* WordPress 4.9.1 support

= 1.0.2 =
* Curl changed to wp_remote_get() function

= 1.0.1 =
* Hide shipping methods if destination cannot be found

= 1.0 =
* Initial release