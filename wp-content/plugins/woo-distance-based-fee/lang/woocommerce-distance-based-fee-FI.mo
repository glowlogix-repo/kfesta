��            )   �      �     �  4   �     �     �  :   �  �   9  w   �  -   d     �     �     �     �     �     �     �     �     �     �       &   	     0     @  	   Q     [     j     �     �     �  �  �  
   �  3   �  '   �     	  .    	  �   O	  ^   

  $   i
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
       =        W     d     v     �  '   �     �     �     �                
                                                            	                                                              Cheatin&#8217; huh? Choose which shipping methods this fee is added for. Distance based fee settings Divider Enter name for the fee that customer will see on checkout. Enter your Google API key in this field. You can get your API key <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">from here.</a> Enter your divider number here. Price will be calculated by dividing the distance, and then multiplicated by the price. Enter your price for the calculated distance. Fee name Google API key Is fee taxable? Methods No Price Remove image Save Settings Standard Standard settings Taxable This plugin adds fee based on distance Upload an image Upload new image Use image Webbisivut.org WooCommerce Distance Based Fee Yes https://www.webbisivut.org https://www.webbisivut.org/ Project-Id-Version: WooCommerce Distance Based Fee
POT-Creation-Date: 2017-11-19 15:37+0700
PO-Revision-Date: 2017-11-19 15:37+0700
Last-Translator: 
Language-Team: 
Language: fi_FI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woocommerce-distance-based-fee.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Huijaatko? Valitse mille toimitustavoille kulu on käytössä. Etäisyyteen perustuvan kulun asetukset Jakaja Anna kulun nimi jonka asiakas näkee kassalla. Anna Google API avain tähän kenttään. API avaimen saat haettua<a target=”_blank” href=”https://developers.google.com/maps/documentation/javascript/get-api-key”>täältä.</a> Anna jakaja. Hinta lasketaan jakamalla etäisyys jakajalla, ja kertomalla saatu luku hinnalla. Anna hinta lasketulle etäisyydelle. Kulun nimi Google API avain Onko kulu verotettava? Toimitustavat Ei Hinta Poista kuva Tallenna asetukset Yleiset Yleiset asetukset Verotus Lisäosa joka asettaa kulun perustuen laskettuun etäisyyteen Lisää kuva Lisää uusi kuva Käytä kuvaa Webbisivut.org WooCommerce etäisyyteen perustuva kulu Kyllä https://www.webbisivut.org https://www.webbisivut.org/ 