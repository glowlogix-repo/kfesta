<?php
/*
Plugin Name: Novaro Testimonial
Plugin URI: http://www.novarostudio.com/
Description: Novaro Testimonial is a wordpress plugin for display testimonial.
Version: 1.0
Author: novarostudio
Author URI: http://www.novarostudio.com
License: GPL
*/

/*  Copyright 2015  Novaro Studio

    Novaro Testimonial is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);

class Novaro_Testimonial{
	
	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;
	
	function __construct(){
		// Register the shortcode to the function ep_shortcode()
		add_shortcode( 'testimonial', array($this, "novaro_testimonialshortcode") );
		add_shortcode( 'testimonial360', array($this, 'novaro_rotatingtestimonial') );
		
		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');
		
		//Register the Portfolio Menu
		add_action('init', array($this, 'novaro_pf_post_type'));
		add_action('init', array($this, 'novaro_pf_action_init'));
		add_action('after_setup_theme', array($this, 'novaro_pf_setup'));
		
		//Customize the Portfolio List in the wp-admin
		add_filter('manage_edit-testimonialpost_columns', array($this, 'novaro_pf_add_list_columns'));
		add_action('manage_testimonialpost_posts_custom_column', array($this, 'novaro_pf_manage_column'));
		add_action( 'restrict_manage_posts', array($this, 'novaro_pf_add_taxonomy_filter') );
		
		$this->version		= $this->novaro_plugin_version();
		$this->postslug		= $this->novaro_postslug();
		$this->taxonomslug	= $this->novaro_taxonomslug();
		$this->posttype		= $this->novaro_posttype();
		$this->posttaxonomy	= $this->novaro_taxonomy();
	}
	
	//Get the version of portfolio
	function novaro_plugin_version(){
		$this->version = "1.0";
		
		return $this->version;
	}
	
	function novaro_lang(){
		$thelang = 'novaro';
		return $thelang;
	}
	
	function novaro_shortname(){
		$theshortname = 'novaro';
		return $theshortname;
	}
	
	function novaro_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}
	
	function novaro_posttype(){
		$this->posttype = 'testimonialpost';
		return $this->posttype;
	}
	
	function novaro_taxonomy(){
		$this->posttaxonomy = 'testimonialcat';
		return $this->posttaxonomy;
	}
	
	function novaro_postslug(){
		$this->postslug = 'testimonial';
		return $this->postslug;
	}
	
	function novaro_taxonomslug(){
		$this->taxonomslug = 'testimonialcat';
		return $this->taxonomslug;
	}
	
	function novaro_pf_md5hash($str = ''){
		return md5($str);
	}

	//Get the image size for every column
	function novaro_pf_setsize(){
	
		//set image size for every column in here.
		$this->imagesizes = array(
			array(
				"num"		=> 'default',
				"namesize"	=> 'testimonial-thumb',
				"width" 	=> 100,
				"height" 	=> 100
			)
			
		);
		return $this->imagesizes;
	}
	
	function novaro_pf_setup(){
		add_theme_support( 'post-thumbnails' );
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			add_image_size( $imgsize["namesize"], $imgsize["width"], $imgsize["height"], true ); // Portfolio Thumbnail
		}
	}
	
	function novaro_pf_getthumbinfo($col){
		$imagesizes = $this->novaro_pf_setsize();
		foreach($imagesizes as $imgsize){
			if($col==$imgsize["num"]){
				return $imgsize;
			}
		}
		return false;
	}
	
	function novaro_testimonialshortcode($atts, $content = null) {
		
		$nvr_initial = $this->novaro_initial();
		
		extract(shortcode_atts(array(
			'id' 	=> '',
			'class'	=> '',
			'col' => '1',
			'cat' => '',
			'showposts' => 5,
			'showtitle' => 'yes',
			'showinfo' => 'yes',
			'showthumb' => 'yes'
		), $atts));
		
		$catname = get_term_by('slug', $cat, $this->novaro_taxonomy() );
		$showtitle = ($showtitle=='yes')? true : false;
		$showinfo = ($showinfo=='yes')? true : false;
		$showthumb = ($showthumb=='yes')? true : false;
		$showposts = (is_numeric($showposts))? $showposts : 5;
		
		if($col!='1' && $col!='2' && $col!='3'){
			$col = '1';
		}
		
		if($col=='3'){
			$col = 3;
		}elseif($col=='2'){
			$col = 2;
		}else{
			$col = 1;
		}
		
		$qryargs = array(
			'post_type' => $this->novaro_posttype(),
			'showposts' => $showposts
		);
		if($catname!=false){
			$qryargs['tax_query'] = array(
				array(
					'taxonomy' => $this->novaro_taxonomy(),
					'field' => 'slug',
					'terms' => $catname->slug
				)
			);
		}
		
		$nvr_testiqry = new WP_Query( $qryargs );
		
		$nvr_output = "";
		if( $nvr_testiqry->have_posts() ){
			$nvr_output .= '<div class="nvr-testimonial '.esc_attr( $class ).'">';
			$nvr_output .= '<ul class="row">';
			$i = 1;
			while ( $nvr_testiqry->have_posts() ) : $nvr_testiqry->the_post(); 
				
				$nvr_testiid = get_the_ID();
				if($col==3){
					$liclass = 'four columns';
				}elseif($col==2){
					$liclass = 'six columns';
				}else{
					$liclass = '';
				}
				
				$custom = get_post_custom( $nvr_testiid );
				$testiinfo 	= (isset($custom["_".$nvr_initial."_testi_info"][0]))? $custom["_".$nvr_initial."_testi_info"][0] : "";
				$testithumb = (isset($custom["testi_thumb"][0]))? $custom["testi_thumb"][0] : "";
				
				if(($i%$col) == 1){
					$liclass .= " alpha";
				}elseif($i%$col==0 && $col>1){
					$liclass .= ' last';
				}
				
				$nvr_output .= '<li class="'.esc_attr( $liclass ).'">';
				
				$bqclass = ($showthumb)? '' : 'nomargin';
				
				$nvr_output .= '<div class="testiwrapper">';
				
				if($showthumb){
					$nvr_output .='<div class="testiimg">';
					if($testithumb){
						$nvr_output .='<img src="'.esc_url( $testithumb ).'" width="50" height="50" alt="'.esc_attr( get_the_title( $nvr_testiid ) ).'" title="'.esc_attr( get_the_title( $nvr_testiid ) ).'" class="scale-with-grid" />';
					}elseif( has_post_thumbnail( $nvr_testiid ) ){
						$nvr_output .= get_the_post_thumbnail( $nvr_testiid, 'testimonial-thumb', array('class' => 'scale-with-grid'));
					}else{
						$nvr_output .='<img src="'. esc_url( plugin_dir_url( __FILE__ ).'images/testi-user.png') .'" width="50" height="50" alt="'.esc_attr( get_the_title( $nvr_testiid ) ).'" title="'. esc_attr( get_the_title( $nvr_testiid ) ) .'" class="scale-with-grid" />';
					}
					$nvr_output .='<span class="insetshadow"></span>';
					$nvr_output .='</div>';
				}
				
				if($showtitle || $showinfo){
					$nvr_output .= '<div class="testiinfo">';
					if($showtitle){
						$nvr_output .= '<h4 class="testititle">'.get_the_title( $nvr_testiid ).'</h4>';
					}
					if($testiinfo){
						$nvr_output .= $testiinfo;
					}
					$nvr_output .= '</div>';
				}
				
				$nvr_output .= '<div class="clearfix"></div>';
				
				$nvr_output .= '<blockquote class="'.esc_attr( $bqclass ).'">'.get_the_content().'<span class="arrowbubble"></span></blockquote>';
				
				$nvr_output .= '<div class="clearfix"></div>';
				
				$nvr_output .= '</div>';
				
				$nvr_output .= '</li>';
				
				$i++;
			endwhile;
			$nvr_output .= '<li class="clearfix"></li></ul>';
			$nvr_output .= '<div class="clearfix"></div>';
			$nvr_output .= "</div>";
		}else{
			$nvr_output .= '<!-- no testimonial post -->';
		}
		wp_reset_postdata();
		
		return do_shortcode($nvr_output);
	}

	function novaro_rotatingtestimonial($atts, $content = null) {
		
		$nvr_initial = $this->novaro_initial();
		
		extract(shortcode_atts(array(
			'id' 	=> '',
			'class'	=> '',
			'cat' => '',
			'showposts' => 5,
			'showinfo' => 'yes',
			'showthumb' => 'yes'
		), $atts));
		
		$catname = get_term_by('slug', $cat, $this->novaro_taxonomy());
		$showinfo = ($showinfo=='yes')? true : false;
		$showthumb = ($showthumb=='yes')? true : false;
		$showposts = (is_numeric($showposts))? $showposts : 5;
		
		$qryargs = array(
			'post_type' => $this->novaro_posttype(),
			'showposts' => $showposts
		);
		if($catname!=false){
			$qryargs['tax_query'] = array(
				array(
					'taxonomy' => $this->novaro_taxonomy(),
					'field' => 'slug',
					'terms' => $catname->slug
				)
			);
		}
		
		$nvr_testiqry = new WP_Query( $qryargs );
		
		$nvr_output = '';
		if( $nvr_testiqry->have_posts() ){
			$nvr_output .= '<div class="nvr-trotating flexslider '.esc_attr( $class ).'">';
				$nvr_output .= '<ul class="slides">';
					while ( $nvr_testiqry->have_posts() ) : $nvr_testiqry->the_post();
						
						$nvr_testiid = get_the_ID(); 
						$custom = get_post_custom( $nvr_testiid );
						$testiinfo 	= (isset($custom["_".$nvr_initial."_testi_info"][0]))? $custom["_".$nvr_initial."_testi_info"][0] : "";
						$testithumb = (isset($custom["testi_thumb"][0]))? $custom["testi_thumb"][0] : "";
						
						$nvr_output .= '<li>';
							
							$nvr_output .= '<blockquote>'.get_the_content().'<span class="arrowbubble"></span></blockquote>';
							$nvr_output .= '<div class="clearfix"></div>';
							
							$nvr_output .= '<div class="testiinfo">';
								$nvr_output .= '<span class="testititle">'.get_the_title( $nvr_testiid ).'</span>';
								if($testiinfo){
									$nvr_output .= ' - '.$testiinfo;
								}
							$nvr_output .= '</div>';
							
							if($showthumb){
								$nvr_output .='<div class="testiimg">';
								if($testithumb){
									$nvr_output .='<img src="'.esc_url( $testithumb ).'" width="50" height="50" alt="'. esc_attr( get_the_title( $nvr_testiid ) ).'" title="'. esc_attr( get_the_title( $nvr_testiid ) ) .'" class="scale-with-grid" />';
								}elseif( has_post_thumbnail( $nvr_testiid ) ){
									$nvr_output .= get_the_post_thumbnail($nvr_testiid, 'testimonial-thumb', array('class' => 'scale-with-grid'));
								}else{
									$nvr_output .='<img src="'. esc_url( plugin_dir_url( __FILE__ ).'images/testi-user.png' ) .'" width="50" height="50" alt="'. esc_attr( get_the_title( $nvr_testiid ) ).'" title="'. esc_attr( get_the_title( $nvr_testiid ) ) .'" class="scale-with-grid" />';
								}
								$nvr_output .='<span class="insetshadow"></span>';
								$nvr_output .='</div>';
							}
							$nvr_output .= '<div class="clearfix"></div>';
						$nvr_output .= '</li>';
						
					endwhile;
				$nvr_output .= '</ul>';
				$nvr_output .= '<div class="clearfix"></div>';
			$nvr_output .= "</div>";
		}else{
			$nvr_output .= '<!-- no testimonial post -->';
		}
		wp_reset_postdata();
		
		return do_shortcode($nvr_output);
	}
	
	/* Make a Portfolio Post Type */
	function novaro_pf_post_type() {
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		$postslug = $this->novaro_postslug();
		$taxonomslug = $this->novaro_taxonomslug();
		
		register_post_type( $posttype,
					array( 
					'label' => __('Testimonial', $this->novaro_lang() ), 
					'public' => true, 
					'show_ui' => true,
					'show_in_nav_menus' => true,
					'rewrite' => array( 'slug' => $postslug, 'with_front' => false ),
					'hierarchical' => true,
					'menu_position' => 5,
					'has_archive' => true,
					'supports' => array(
										 'title',
										 'editor',
										 'thumbnail',
										 'excerpt',
										 'custom-fields',
										 'revisions')
						) 
					);
		register_taxonomy($taxonom, $posttype, array(
			'hierarchical' => true,
			'label' =>  __('Testimonial Categories', $this->novaro_lang()),
			'query_var' => true,
			'rewrite' => array( 'slug' => $taxonomslug, 'with_front' => false ),
			'show_ui' => true,
			'singular_name' => 'Category'
			));
	}
	
	function novaro_pf_add_list_columns($portfolio_columns){
		
		$thetaxonomy = $this->novaro_taxonomy();
		$new_columns = array();
		$new_columns['cb'] = '<input type="checkbox" />';
		
		$new_columns['title'] = __('Testimonial Title', $this->novaro_lang());
		$new_columns['images'] = __('Images', $this->novaro_lang());
		$new_columns['author'] = __('Author', $this->novaro_lang());
		
		$new_columns[$thetaxonomy] = __('Categories', $this->novaro_lang());
		
		$new_columns['date'] = __('Date', $this->novaro_lang());
		
		return $new_columns;
	}
	
	function novaro_pf_manage_column($column_name){
		global $post;
		$posttype = $this->novaro_posttype();
		$taxonom = $this->novaro_taxonomy();
		
		$id = $post->ID;
		$title = $post->post_title;
		switch($column_name){
			case 'images':
				$thumbnailid = get_post_thumbnail_id($id);
				$imagesrc = wp_get_attachment_image_src($thumbnailid, 'thumbnail');
				if($imagesrc){
					echo '<img src="'.$imagesrc[0].'" width="50" alt="'.$title.'" />';
				}else{
					_e('No Featured Image', $this->novaro_lang());
				}
				break;
			
			case $taxonom:
				$postterms = get_the_terms($id, $taxonom);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonom.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$termtext = implode(", ",$termlists);
						echo $termtext;
					}
				}
				
				break;
		}
	}
	
	/* Filter Custom Post Type Categories */
	function novaro_pf_add_taxonomy_filter() {
		global $typenow;
		$posttype = $this->novaro_posttype();
		$taxonomy = $this->novaro_taxonomy();
		if( $typenow==$posttype){
			$filters = array($taxonomy);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo '<select name="'. esc_attr( $tax_slug ).'" id="'. esc_attr( $tax_slug ) .'" class="postform">';
				echo "<option value=''>".__('View All',$this->novaro_lang())." "."$tax_name</option>";
				foreach ($terms as $term) { 
					$selectedstr = '';
					if(isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug){
						$selectedstr = ' selected="selected"';
					}
					echo '<option value='. $term->slug. $selectedstr . '>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
	
	function novaro_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages
		
		$version = $this->novaro_plugin_version();
		
		wp_register_script('nvr_flexslider', plugin_dir_url( __FILE__ ).'js/jquery.flexslider-min.js', array('jquery'), '1.8', true);
		wp_enqueue_script('nvr_flexslider');
		
		wp_register_script('nvr_customTestimonial', plugin_dir_url( __FILE__ ).'js/nvrtestimonial.js', array('jquery'), '1.8', true);
		wp_enqueue_script('nvr_customTestimonial');
		
		$nvr_localvar = array( 
			'pluginurl'					=> plugin_dir_url( __FILE__ )
		);
		wp_localize_script( 'nvr_customTestimonial', 'nvrtestilocal_var', $nvr_localvar );
		
		//Register and use this plugin main CSS
		wp_register_style('nvr_skeleton-css', plugin_dir_url( __FILE__ ).'css/1140.css', 'nvr_normalize-css', '', 'screen, all');
		wp_enqueue_style('nvr_skeleton-css');
		
		wp_register_style('nvr_flexslider-css', plugin_dir_url( __FILE__ ).'css/flexslider.css', '', '', 'screen, all');
		wp_enqueue_style('nvr_flexslider-css');
		
		wp_register_style('nvr_custom-testimonial-css', plugin_dir_url( __FILE__ ).'css/nvrtestimonial.css', '', '', 'screen, all');
		wp_enqueue_style('nvr_custom-testimonial-css');
	}
	
	// The excerpt based on character
	function novaro_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}
	
}

$thetestimonial = new Novaro_Testimonial();
?>