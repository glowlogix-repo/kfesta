<?php
// =============================== Novaro Woocommerce Price Range widget ======================================
class Constance_WooPriceRangeWidget extends WP_Widget {
    /** constructor */

	function __construct() {
		$widget_ops = array('classname' => 'widget_constance_woo_price_range', 'description' => esc_html__('Constance - Price Range', "constance") );
		parent::__construct('constance-price-range', esc_html__('Constance - Price Range', "constance"), $widget_ops);

	}


  /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
		if(!is_woocommerce()){return;}
		global $_chosen_attributes, $wpdb, $wp;

		if ( ! is_post_type_archive( 'product' ) && ! function_exists('wc_price') && ! class_exists("WC_Query") && ! is_tax( get_object_taxonomies( 'product' ) ) ) {
			return;
		}

		$current_term = is_tax() ? get_queried_object()->term_id : '';
		$current_tax  = is_tax() ? get_queried_object()->taxonomy : '';
        $title = apply_filters('widget_title', empty($instance['title']) ? esc_html__('Price Range', "constance") : $instance['title']);
		$rangeinterval = apply_filters('widget_rangeinterval', empty($instance['rangeinterval']) ? "100" : $instance['rangeinterval']);

		if(!is_numeric($rangeinterval)){
			return;
		}

    $prices = $this->get_filtered_price();
    $min = 0;
    $max    = ceil( $prices->max_price );

		if ( $min == $max ) {
			return;
		}

		$price_range = array();

		$a = 0;
		do{
			$z = $a+$rangeinterval;

			if($z>$max){
				$price_range[$max] = wc_price($max);
			}else{
				$price_range[$z] = wc_price($z);
			}

			$a = $z;

		}while($a<$max);

        ?>
              <?php echo wp_kses_post( $before_widget ); ?>
                  <?php if ( $title )
                        echo wp_kses_post( $before_title . esc_html( $title ) . $after_title ); ?>

                                <ul class="nvr-price-range-widget">
                                    <?php
									$minprice = 0;
									foreach ( $price_range as $orderbyid => $orderbyname ){


									/*******/
									// Base Link decided by current page
									if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
										$link = home_url();
									} elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id('shop') ) ) {
										$link = get_post_type_archive_link( 'product' );
									} else {
										$link = get_term_link( get_query_var('term'), get_query_var('taxonomy') );
									}

									// All current filters
									if ( $_chosen_attributes ) {
										foreach ( $_chosen_attributes as $name => $data ) {

												// Exclude query arg for current term archive term
												while ( in_array( $current_term, $data['terms'] ) ) {
													$key = array_search( $current_term, $data );
													unset( $data['terms'][$key] );
												}

												// Remove pa_ and sanitize
												$filter_name = sanitize_title( str_replace( 'pa_', '', $name ) );

												if ( ! empty( $data['terms'] ) ) {
													$link = add_query_arg( 'filter_' . $filter_name, implode( ',', $data['terms'] ), $link );
												}

												if ( 'or' == $data['query_type'] ) {
													$link = add_query_arg( 'query_type_' . $filter_name, 'or', $link );
												}
										}
									}

									// Orderby
									if ( isset( $_GET['orderby'] ) ) {
										$link = add_query_arg( 'orderby', $_GET['orderby'], $link );
									}

									// Search Arg
									if ( get_search_query() ) {
										$link = add_query_arg( 's', get_search_query(), $link );
									}

									// Post Type Arg
									if ( isset( $_GET['post_type'] ) ) {
										$link = add_query_arg( 'post_type', $_GET['post_type'], $link );
									}

									// Min & Max Price Arg
									$link = add_query_arg( 'min_price', $minprice, $link );
									$link = add_query_arg( 'max_price', $orderbyid, $link );
									/*******/


									?>
                                    <li class="product-sorting-list">
                                        <a class="product-sorting-link" href="<?php echo esc_url( $link ); ?>" rel="bookmark" title="<?php esc_attr_e('Permanent Link to', "constance");?> <?php the_title_attribute(); ?>">
                                        <?php
										if($orderbyid>$max){
											echo '&gt; ' . wc_price($orderbyid);
										}else{
											echo wc_price($minprice). ' &mdash; ' . wc_price($orderbyid);
										}

										?>
                                        </a>
                                    </li>
                                    <?php
										$minprice = $orderbyid;
									}//end for each
									?>
                                </ul>

								<?php wp_reset_postdata(); ?>



              <?php echo wp_kses_post( $after_widget ); ?>

        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
		$instance['title'] = (isset($instance['title']))? $instance['title'] : "";
		$instance['rangeinterval'] = (isset($instance['rangeinterval']))? $instance['rangeinterval'] : "";

        $title = esc_attr($instance['title']);
		$rangeinterval = esc_attr($instance['rangeinterval']);
        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', "constance"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('rangeinterval') ); ?>"><?php esc_html_e('Range Interval:', "constance"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('rangeinterval') ); ?>" name="<?php echo esc_attr( $this->get_field_name('rangeinterval') ); ?>" type="text" value="<?php echo esc_attr( $rangeinterval ); ?>" /></label></p>
        <?php
    }

    /**
  	 * Get filtered min price for current products.
  	 * @return int
  	 */
    protected function get_filtered_price() {
  		global $wpdb, $wp_the_query;

  		$args       = $wp_the_query->query_vars;
  		$tax_query  = isset( $args['tax_query'] ) ? $args['tax_query'] : array();
  		$meta_query = isset( $args['meta_query'] ) ? $args['meta_query'] : array();

  		if ( ! empty( $args['taxonomy'] ) && ! empty( $args['term'] ) ) {
  			$tax_query[] = array(
  				'taxonomy' => $args['taxonomy'],
  				'terms'    => array( $args['term'] ),
  				'field'    => 'slug',
  			);
  		}

  		foreach ( $meta_query as $key => $query ) {
  			if ( ! empty( $query['price_filter'] ) || ! empty( $query['rating_filter'] ) ) {
  				unset( $meta_query[ $key ] );
  			}
  		}

  		$meta_query = new WP_Meta_Query( $meta_query );
  		$tax_query  = new WP_Tax_Query( $tax_query );

  		$meta_query_sql = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
  		$tax_query_sql  = $tax_query->get_sql( $wpdb->posts, 'ID' );

  		$sql  = "SELECT min( CAST( price_meta.meta_value AS UNSIGNED ) ) as min_price, max( CAST( price_meta.meta_value AS UNSIGNED ) ) as max_price FROM {$wpdb->posts} ";
  		$sql .= " LEFT JOIN {$wpdb->postmeta} as price_meta ON {$wpdb->posts}.ID = price_meta.post_id " . $tax_query_sql['join'] . $meta_query_sql['join'];
  		$sql .= " 	WHERE {$wpdb->posts}.post_type = 'product'
  					AND {$wpdb->posts}.post_status = 'publish'
  					AND price_meta.meta_key IN ('" . implode( "','", array_map( 'esc_sql', apply_filters( 'woocommerce_price_filter_meta_keys', array( '_price' ) ) ) ) . "')
  					AND price_meta.meta_value > '' ";
  		$sql .= $tax_query_sql['where'] . $meta_query_sql['where'];

  		return $wpdb->get_row( $sql );
  	}

} // class  Widget
?>
