<?php
add_action('admin_init', 'sampleoptions_init_fn');
add_action('admin_menu', 'sampleoptions_add_page_fn');
$text_sid = "";
$text_token = "";
$text_phone = "";
// Register our settings. Add the settings section, and settings fields
function sampleoptions_init_fn()
{
    register_setting('plugin_options', 'plugin_options', 'plugin_options_validate');
    add_settings_section('main_section', 'Main Settings', 'section_text_fn', __FILE__);
    add_settings_field('plugin_text_sid', 'Enter SID', 'setting_string_fn', __FILE__, 'main_section');
    add_settings_field('plugin_text_token', 'Input Token', 'setting_token_fn', __FILE__, 'main_section');
    add_settings_field('plugin_text_phone', 'Enter Twilio Phone', 'setting_phone_fn', __FILE__, 'main_section');
}

/*
 * Add sub page to the Settings Menu
 */
function sampleoptions_add_page_fn()
{
    // add optiont to main settings panel
    add_options_page('Twilio Settings', 'Twilio Settings', 'administrator', __FILE__, 'options_page_fn');
}

// ************************************************************************************************************
// Callback functions
// Init plugin options to white list our options
// Section HTML, displayed before the first option
function section_text_fn()
{
    echo '<p>Enter Twilio Credentials</p>';
}

function setting_string_fn()
{
    $options = get_option('plugin_options');
    echo "<input id='plugin_text_sid' name='plugin_options[text_sid]' size='40' type='text' value='{$options['text_sid']}' />";
}

// PASSWORD-TEXTBOX - Name: plugin_options[pass_string]
function setting_token_fn()
{
    $options = get_option('plugin_options');
    echo "<input id='plugin_text_token' name='plugin_options[text_token]' size='40' type='text' value='{$options['text_token']}' />";
}

function setting_phone_fn()
{
    $options = get_option('plugin_options');
    echo "<input id='plugin_text_phone' name='plugin_options[text_phone]' size='40' type='text' value='{$options['text_phone']}' />";
}

// Display the admin options page
function options_page_fn()
{?>
    <div class="wrap">
        <form action="options.php" method="post">
            <?php
            if (function_exists('wp_nonce_field'))
                wp_nonce_field('plugin-name-action_' . "yep");
            ?>
            <?php settings_fields('plugin_options'); ?>
            <?php do_settings_sections(__FILE__); ?>
            <p class="submit">
                <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>"/>
            </p>
        </form>
    </div>
    <?php
}

// Validate user data for some/all of your input fields
function plugin_options_validate($input)
{
    // Check our textbox option field contains no HTML tags - if so strip them out
    $input['text_sid']  = wp_filter_nohtml_kses($input['text_sid']);
    $input['text_token'] = wp_filter_nohtml_kses($input['text_token']);
    $input['text_phone'] = wp_filter_nohtml_kses($input['text_phone']);

    return $input; // return validated input
}