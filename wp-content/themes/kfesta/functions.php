<?php

require __DIR__ . '/twilio-php-master/Twilio/autoload.php';
use Twilio\Rest\Client;
include ('option.php');

function product_manager_form(){
	if (isset($_POST["vendor_product_type"])) { 
    
    	$user_id 		= (int) $_POST['vendor_product_id'];
		$product_ids 	= (int) $_POST['vendor_product_type'];

    	$vendor_products = get_user_meta($user_id, '_vendor_product_ids', true);

		if ( ! is_array($vendor_products) ) {
		    $vendor_products = array();

		     // Push a new value onto the array
		    $vendor_products[] = $product_ids;
		    // Write the user meta record with the new value in it
		    update_user_meta($user_id, '_vendor_product_ids', $vendor_products);

		} elseif (! in_array($product_ids, $vendor_products)) {
    		
    		array_push($vendor_products, $product_ids);
    		/*
    		Added product id in meta.
    		*/
    		update_user_meta( $user_id, '_vendor_product_ids', $vendor_products);
    	} 
    }
}
add_action('init', 'product_manager_form');


function custom_processing($order_id) {

    /*
start_working_on_cake, cake_is_finished, cake’s_photo, cake_is_delivered
    */
    $order = wc_get_order( $order_id );
    $items = $order->get_items(); 

    $product_order_config = array();
    foreach ( $order->get_items() as $item ) {

        // Compatibility for woocommerce 3+
        $product_id = version_compare( WC_VERSION, '3.0', '<' ) ? $item['product_id'] : $item->get_product_id(); 

        if( have_rows('configurable_product', $product_id) ): 
            while( have_rows('configurable_product', $product_id) ): the_row();
                $step_name = get_sub_field('step_name', $product_id);
                $step_type = get_sub_field('step_type', $product_id);
                $product_order_config[$product_id][$step_name] = 0;
                kfesta_checkbox_timestamp($order_id,$product_id,$step_name, 0);
            endwhile;
        endif;
    }
	update_post_meta($order_id, 'configurable_order_product', $product_order_config);

/*a:1:{i:2356;a:3:{s:21:"start_working_on_cake";i:0;...*/ 
}
//add_action('admin_init', 'custom_processing');
add_action('woocommerce_order_status_processing', 'custom_processing');

//For shipping upon checkbox
function shipping_check($configurable_order_product)
{
    foreach ($configurable_order_product as $key => $fields) {
        foreach ($fields as $field => $value) {
            if ($value == 0) {
                return false;
            }
        }
    }
    return true;
}

/**
 * Calculate shipping, tax and insert into wcmp_vendor_orders table whenever an order is created.
 * 
 * @since 2.7.6
 * @global onject $wpdb
 * @param int $order_id
 * @param WC_Order object $order
 */
function kfesta_process_order($order_id, $order) {
    global $wpdb;
    error_log("Order data: ",print_r($order, true));
    return;
    if (get_post_meta($order_id, '_wcmp_order_processed', true)) {
        return;
    }
    error_log("Order data: ",print_r($order, true));

    $vendor_shipping_array = get_post_meta($order_id, 'dc_pv_shipped', true);
    $mark_ship = 0;
    $items = $order->get_items('line_item');
    $shipping_items = $order->get_items('shipping');
    $vendor_shipping = array();
    foreach ($shipping_items as $shipping_item_id => $shipping_item) {
        $order_item_shipping = new WC_Order_Item_Shipping($shipping_item_id);
        $shipping_vendor_id = $order_item_shipping->get_meta('vendor_id', true);
        $vendor_shipping[$shipping_vendor_id] = array(
            'shipping' => $order_item_shipping->get_total()
            , 'shipping_tax' => $order_item_shipping->get_total_tax()
            , 'package_qty' => $order_item_shipping->get_meta('package_qty', true)
        );
    }
    foreach ($items as $order_item_id => $item) {
        $line_item = new WC_Order_Item_Product($item);
        $product_id = $item['product_id'];
        $variation_id = isset($item['variation_id']) ? $item['variation_id'] : 0;
        if ($product_id) {
            $product_vendors = get_wcmp_product_vendors($product_id);
            if ($product_vendors) {
                if (isset($product_vendors->id) && is_array($vendor_shipping_array)) {
                    if (in_array($product_vendors->id, $vendor_shipping_array)) {
                        $mark_ship = 1;
                    }
                }
                $shipping_amount = $shipping_tax_amount = 0;
                if (!empty($vendor_shipping) && isset($vendor_shipping[$product_vendors->id])) {
                    $shipping_amount = (float) round(($vendor_shipping[$product_vendors->id]['shipping'] / $vendor_shipping[$product_vendors->id]['package_qty']) * $line_item->get_quantity(), 2);
                    $shipping_tax_amount = (float) round(($vendor_shipping[$product_vendors->id]['shipping_tax'] / $vendor_shipping[$product_vendors->id]['package_qty']) * $line_item->get_quantity(), 2);
                }
                $wpdb->query(
                        $wpdb->prepare(
                                "INSERT INTO `{$wpdb->prefix}wcmp_vendor_orders` 
                                    ( order_id
                                    , commission_id
                                    , vendor_id
                                    , shipping_status
                                    , order_item_id
                                    , product_id
                                    , variation_id
                                    , tax
                                    , line_item_type
                                    , quantity
                                    , commission_status
                                    , shipping
                                    , shipping_tax_amount
                                    ) VALUES ( %d
                                    , %d
                                    , %d
                                    , %s
                                    , %d
                                    , %d 
                                    , %d
                                    , %s
                                    , %s
                                    , %d
                                    , %s
                                    , %s
                                    , %s
                                    ) ON DUPLICATE KEY UPDATE `created` = now()"
                                , $order_id
                                , 0
                                , $product_vendors->id
                                , $mark_ship
                                , $order_item_id
                                , $product_id
                                , $variation_id
                                , $line_item->get_total_tax()
                                , 'product'
                                , $line_item->get_quantity()
                                , 'unpaid'
                                , $shipping_amount
                                , $shipping_tax_amount
                        )
                );
            }
        }
    }
    update_post_meta($order_id, '_wcmp_order_processed', true);
    do_action('wcmp_order_processed', $order);
}

/**
 * Process order after checkout for shipping, Tax calculation.
 *
 * @param int $order_id
 * @param array $order_posted
 * @param WC_Order $order WooCommerce order object
 * @return void
 */
function kfesta_checkout_order_processed($order_id, $order_posted, $order) {
    if (!get_post_meta($order_id, '_wcmp_order_processed', true)) {
        kfesta_process_order($order_id, $order);
    }
}
//add_action('woocommerce_checkout_order_processed', 'kfesta_checkout_order_processed', 30, 3);
add_action( 'woocommerce_order_status_pending', 'kfesta_find_nearest_vendor', 10, 1);
//Find nearest Vendor
function kfesta_find_nearest_vendor($order_id, $product_id){
    $product_id=2802;
    $order = wc_get_order( $order_id );
    $items = $order->get_items();
    $product_id_num = array();
    $vendors_ids = array();
    $vendors = get_users( 'role=dc_vendor' ); //get all vendors
    foreach ($vendors as $vendor) {
        $vendors_ids[] = $vendor->id;
    }
    foreach ($vendors_ids as $vendors_id) {
        $products = get_user_meta($vendors_id, "_vendor_product_ids");
        foreach ($products[0] as $product) {
            if($product == $product_id){
                $today_date             = date('N');
                $vendor_working_days    = get_user_meta($vendors_id, 'vendor_working_days', true);
                if ( in_array($today_date, $vendor_working_days) ) {
                    $vendors_id_products[$product_id][] = $vendors_id;
                    $vendor_address_1[$vendors_id] = get_user_meta($vendors_id, 'shipping_address_1', true);
                    $vendor_address_2[$vendors_id] = get_user_meta($vendors_id, 'shipping_address_2', true);
                    $vendor_radius[]=get_user_meta($vendors_id,'vendor_radius',true);
                }
            }
        }
    }
    $vendor_address = array(); // initialize array for vendors address

    //Concatinate both addresses(address_1 and address_2)
    foreach ($vendor_address_1 as $key => $value) {
        $vendor_address[$key] = $vendor_address_1[$key].' '.$vendor_address_2[$key];
    }
    $order_address = $order->data['billing']['address_1']. ' ' . $$order->data['billing']['address_2']; //concatinate customer address
    // URL of API
    $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='. str_replace(' ', '%20', $order_address).'&destinations='.str_replace(' ', '%20', implode('|',$vendor_address)).'&key=AIzaSyC1SIwpV3xrTyGioVo7WHZl3RY3ZXTjzWE';
    $locations = file_get_contents($url);
    print_r($locations);
    $locations = json_decode($locations);
    $addresses_with_id = array_combine($locations->destination_addresses, $vendors_id_products[$product_id]); //combine vendor array and destination addresses array
    $nearest_vendor_address = -1;
    // Finding nearest vendor address
    foreach ($locations->rows[0]->elements as $index => $location){
        foreach ($vendor_radius as $key_value => $vendor_rad) {
            if($vendor_rad*1000 >= $location->distance->value){
                if ($nearest_vendor_address == -1) {
                    $nearest_vendor_address = $location->distance->value;
                    $nearest_vendor = $locations->destination_addresses[$key_value];
                }
                elseif ($nearest_vendor_address > $location->distance->value && $location->distance->value != NULL) {
                    $nearest_vendor_address = $location->distance->value;
                    $nearest_vendor = $locations->destination_addresses[$key_value];
                    }
                }
            }
        }
        if($addresses_with_id[$nearest_vendor]!=null){
            $id=$addresses_with_id[$nearest_vendor];            
 $vendor_phone= get_user_meta($id,'billing_phone',true);
 $account_sid    = get_option('plugin_text_sid');  
 $auth_token     = get_option('plugin_text_token');
    $twilio_number  = get_option('plugin_text_phone');
          $receiver_number= $vendor_phone;
          $client = new Client($account_sid, $auth_token);
          $client->messages->create(    
          $receiver_number,
         array(
        'from' => $twilio_number,
        'body' => 'You have new Order' .$order  ) ); 

        }
        var_dump($addresses_with_id[$nearest_vendor]);
        exit;
    return $addresses_with_id[$nearest_vendor];
}

// timestamp for checkbox on order
function kfesta_checkbox_timestamp($order_id, $product_id, $field, $value){
    global $wpdb;
    $wpdb->insert('wp_wcmp_order_retail', array(
        'order_id' => $order_id,
        'product_id' => $product_id,
        'key_name' => $field,
        'key_value' => $value
    ));
}
/*add_action("wp_ajax_vendor_available_days", "vendor_available_days_test");*/
add_action("registered_post_type","vendor_available_days");
function vendor_available_days(){
    $id = get_current_user_id();
    if (isset($_POST['days'])) {
        update_user_meta($id, 'vendor_working_days', $_POST['days']);
    }
    if( isset($_POST['radius']) ){
        update_user_meta($id, 'vendor_radius', $_POST['radius']);
    }
}



// ajax call for order items checkbox
add_action("wp_ajax_order_item_check_box", "kfesta_order_checkbox");
function kfesta_order_checkbox() {
    $data = $_POST['data'];
    $configurable_order_product = get_post_meta($data[2], 'configurable_order_product');
    var_dump($data[0]);
    foreach ($configurable_order_product[0] as $key => $products_checklist) {
        if ($key == $data[1]) {
            var_dump($key);
            var_dump($products_checklist);
            foreach ($products_checklist as $field => $value) {
                var_dump($field);
                if ($field == $data[0]) {
                    $configurable_order_product[0][$key][$field] = 1;
                    var_dump($field);
                }
            }
        }
    }
    var_dump($configurable_order_product[0]);
    update_post_meta($data[2], 'configurable_order_product', $configurable_order_product[0]);
    kfesta_checkbox_timestamp($data[2], $data[1], $data[0], 1);
}
add_action("wp_ajax_delete_product_id", "kfesta_delete_product_id");
function kfesta_delete_product_id(){
    $data = $_POST['data'];
    $product_id = $data[0];
    $vendor_id  = $data[1];
    $products_vendors= get_user_meta($vendor_id,'_vendor_product_ids',true );
    $prev_value= get_user_meta($vendor_id,'_vendor_product_ids',true );
    //print_r($products_vendors);
    $key = array_search($product_id, $products_vendors);
    unset($products_vendors[$key]);
    update_user_meta( $vendor_id,'_vendor_product_ids' , $products_vendors, $prev_value );
    $products= get_user_meta($vendor_id,'_vendor_product_ids',true );
    print_r($products);
    die();
}

add_action("wp_ajax_order_item_image", "kfesta_order_item_image");
function kfesta_order_item_image() {
    echo 'here worked.';
    $target_dir = $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/';
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $meta_file = get_site_url().'/wp-content/uploads/'. basename($_FILES["image"]["name"]);
    if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)){
        echo "File uploaded!";
        update_post_meta($order, $img_field, $meta_file);
    }
 }

 add_action('init', 'kfesta_order_checkbox_script_enqueuer');
 function kfesta_order_checkbox_script_enqueuer() {
    wp_register_script( "kfesta_order_checkbox_script", get_template_directory_uri().'/assets/js/kfesta_order_checkbox_script.js', array('jquery') );
    wp_localize_script( 'kfesta_order_checkbox_script', 'myAjax', array( 'ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('jquery');
    wp_enqueue_script('kfesta_order_checkbox_script');
    wp_enqueue_script('Google_Api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCNOswWPmxhYSeKyPn6Pm5w4g3wP16VbRw&libraries=places&callback=initAutocomplete', array(), null, true);
    wp_enqueue_style('bootstrap_css','https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
    wp_enqueue_script('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
    // wp_enqueue_script('jquery_popup','http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js');
}

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
<h3><?php _e("Vendor information", "blank"); ?></h3>

<table class="form-table">
<tr>
<th><label for="address"><?php _e("Radius"); ?></label></th>
<td>
<input type="text" name="radius" id="radius" value="<?php echo esc_attr( get_user_meta( $user->ID, 'vendor_radius', true ) ); ?>" class="regular-text" /><br />

</td>
</tr>
<br/>
<br/>

<label><b>Select Working Days</b></label>    
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

 <?php $days   =get_user_meta($user->ID, 'vendor_working_days', true); ?>
   
  <input type="checkbox" id="weekday-mon"  class="weekday required" value="1" <?php if(in_array(1, $days)) echo( 'checked'); ?> name="days[]"  />
  <label for="weekday-mon"  >Mon</label>&nbsp;&nbsp;

  <input type="checkbox" id="weekday-tue" class="weekday required" value="2" <?php if(in_array(2, $days)) echo( 'checked'); ?> name="days[]"/>
  <label for="weekday-tue">Tue</label>&nbsp;&nbsp;
  <input type="checkbox" id="weekday-wed" class="weekday required" value="3" <?php if(in_array(3, $days)) echo( 'checked'); ?> name="days[]"/>
  <label for="weekday-wed">Wed</label>&nbsp;&nbsp;
  <input type="checkbox" id="weekday-thu" class="weekday required" value="4" <?php if(in_array(4, $days)) echo( 'checked'); ?> name="days[]"/>
  <label for="weekday-thu">Thu</label>&nbsp;&nbsp;
  <input type="checkbox" id="weekday-fri" class="weekday required" value="5" <?php if(in_array(5, $days)) echo( 'checked'); ?> name="days[]"/>
  <label for="weekday-fri">Fri</label>&nbsp;&nbsp;
  <input type="checkbox" id="weekday-sat" class="weekday required" value="6" <?php if(in_array(6, $days)) echo( 'checked'); ?> name="days[]"/>
  <label for="weekday-sat">Sat</label>&nbsp;&nbsp;
  <input type="checkbox" id="weekday-sun" class="weekday required" value="7" <?php if(in_array(7, $days)) echo( 'checked'); ?> name="days[]"/>
  <label for="weekday-sun">Sun</label>  
    
 </table>
<?php 
 }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }
    update_user_meta( $user_id, 'radius', $_POST['vendor_radius'] );
}




